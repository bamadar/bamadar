!(function() {
  function t() {
    var t = document.createElement("script");
    (t.type = "text/javascript"),
      (t.async = !0),
      localStorage.getItem("rayToken")
        ? (t.src =
            "https://app.raychat.io/scripts/js/" +
            o +
            "?rid=" +
            localStorage.getItem("rayToken") +
            "&href=" +
            window.location.href)
        : (t.src = "https://app.raychat.io/scripts/js/" + o);
    var e = document.getElementsByTagName("script")[0];
    e.parentNode.insertBefore(t, e);
  }
  var e = document,
    a = window,
    o = "cb1a7b4b-b4ce-4a65-b56b-50067f411635";
  "complete" == e.readyState
    ? t()
    : a.attachEvent
    ? a.attachEvent("onload", t)
    : a.addEventListener("load", t, !1);
})();
