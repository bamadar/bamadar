(function () {
  var now = new Date();
  var version =
    now.getFullYear().toString() +
    "0" +
    now.getMonth() +
    "0" +
    now.getDate() +
    "0" +
    now.getHours();
  var head = document.getElementsByTagName("head")[0];
  var link = document.createElement("link");
  link.rel = "stylesheet";
  link.href =
    "https://van.najva.com/static/cdn/css/local-messaging.css" +
    "?v=" +
    version;
  head.appendChild(link);
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.async = true;
  script.src =
    "https://van.najva.com/static/js/scripts/bamadar-website-23736-ebf8a016-cb7e-4138-b828-2ae97c851bdb.js" +
    "?v=" +
    version;
  head.appendChild(script);
})();
