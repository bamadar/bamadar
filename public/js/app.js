if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("/sw.js")
    .then((registration) => {
      console.log("service worker registration successed:", registration);
    })
    .catch((err) => {
      console.log("service worker registration failed:", err);
    });
} else {
  console.log("Service workers are not supported.");
}

// let installPromptEvent;

// window.addEventListener('beforeinstallprompt', (e) => {
//   // Prevent Chrome 67 and earlier from automatically showing the prompt
//   e.preventDefault();
//   // Stash the event so it can be triggered later.
//   installPromptEvent = e;
// });

// document.querySelector('.ssss').addEventListener('click', (e) => {
//   e.preventDefault();
//   console.log(installPromptEvent);
//   if (installPromptEvent) {
//     installPromptEvent.prompt();

//     installPromptEvent.userChoice.then((choiceResult) => {
//       if (choiceResult.outcome === 'accepted') {
//         console.log('User Accepted');
//       } else {
//         console.log('User Dismissed');
//       }

//       installPromptEvent = null;
//     });
//   }
// });
