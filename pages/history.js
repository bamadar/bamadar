import React, { useContext, useEffect } from "react";
import { loginCheck } from "../utils/main";
import GlobalContainer from "../containers/GlobalContainer/GlobalContainer";
import { History } from "../containers/History";
import Router from "next/router";
import { getUserOrders } from "../context/server";

export default () => {
  useEffect(() => {
    if (!loginCheck()) Router.push("/order?target=history");
  }, []);

  return (
    <GlobalContainer currentNav={2} deleteHeader={true}>
      {loginCheck() && <History getUserOrders={getUserOrders} />}
    </GlobalContainer>
  );
};
