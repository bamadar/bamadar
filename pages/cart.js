import React, { useEffect, useState, useContext } from "react";
import Cart from "./../containers/Cart";
import GlobalContainer from "./../containers/GlobalContainer/GlobalContainer";
import CardContextConsumer, { CardContext } from "../context/cardContext";
import styled from "styled-components";
export default () => {
  const [cartDetails, setCartDetails] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const { items, increaseCardBalance, decreaseCardBalance } = useContext(
    CardContext
  );

  useEffect(() => {
    let sum = 0;
    items.forEach((item) => {
      if (item.balance > 0 && item.sell_price && item.number)
        return (sum += item.sell_price * item.balance);
      else decreaseCardBalance(item);
    });
    setCartDetails(sum);
  }, [items]);

  return (
    <GlobalContainer currentNav={3} deleteHeader={true}>
      <Wrapper>
        <Cart
          cartDetails={cartDetails}
          showModal={showModal}
          setShowModal={setShowModal}
          items={items}
          increaseCardBalance={increaseCardBalance}
          decreaseCardBalance={decreaseCardBalance}
        />
      </Wrapper>
    </GlobalContainer>
  );
};

const Wrapper = styled.div``;
