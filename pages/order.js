import React, { useEffect, useState } from "react";
import { loginCheck } from "../utils/main";
import { Login } from "../containers/Login";
import GlobalContainer from "./../containers/GlobalContainer/GlobalContainer";
import OrderForm from "../containers/Order";

export default () => {
  const [logged, setLogged] = useState("");
  const [maInfo, setMaInfo] = useState(null);

  useEffect(() => {
    loginCheck() != 1 ? setLogged("0") : setLogged("1");

    const madarPack = window.localStorage.getItem("MadarPack");
    const userInfo = JSON.parse(madarPack);
    let info = userInfo && userInfo.pack && userInfo.pack.user;

    setMaInfo(info);
  }, []);

  return logged == 0 ? (
    <GlobalContainer currentNav={4} deleteHeader={true}>
      <Login setLogged={setLogged} />
    </GlobalContainer>
  ) : (
    <GlobalContainer currentNav={4} deleteHeader={true}>
      <OrderForm userInfo={maInfo} />
    </GlobalContainer>
  );
};
