import React from "react";
import Profile from "./../containers/Profile";
import GlobalContainer from "./../containers/GlobalContainer/GlobalContainer";
import styled from "styled-components";

export default () => {
  return (
    <GlobalContainer currentNav={4} deleteHeader={true}>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          width: "100%",
          minHeight: "calc(100vh - 120px)",
          height: "100%",
          maxWidth: "100vw",
        }}
      >
        <img src="./images/555.jpg" style={{ maxWidth: "100vw" }} />
      </div>
    </GlobalContainer>
  );
};
