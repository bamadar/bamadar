import React from "react";
import styled from "styled-components";
import GlobalContainer from "./../containers/GlobalContainer/GlobalContainer";
import CloudDownloadIcon from "@material-ui/icons/CloudDownload";

import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Container } from "@material-ui/core";

import AndroidIcon from "@material-ui/icons/Android";
import AppleIcon from "@material-ui/icons/Apple";

const addr = "images/app/";
const chromeImages = ["1.jpg", "2.jpg", "3.jpg", "4.jpg", "5.jpg"];
const samsungImages = ["s_0.jpg", "s_1.jpg", "s_2.jpg", "s_3.jpg"];
const firefoxImages = ["f_1.jpg", "f_2.jpg", "f_3.jpg", "f_4.jpg"];
const edgeImages = ["e_1.jpg", "e_2.jpg", "e_3.jpg", "e_4.jpg"];
const safariImages = ["i_1.jpg", "i_2.jpg", "i_3.jpg", "i_4.jpg"];
const operaImages = ["o_1.jpg", "o_2.jpg", "o_3.jpg", "o_4.jpg"];

const content = [
  {
    title: "افزودن PWA در کروم",
    logo: "/images/vectors/browsers/chrome.svg",
    images: chromeImages,
  },
  {
    title: "افزودن PWA در سافاری",
    logo: "/images/vectors/browsers/safari.svg",
    images: safariImages,
  },
  {
    title: "افزودن PWA در مرورگر سامسونگ",
    logo: "/images/vectors/browsers/samsung.svg",
    images: samsungImages,
  },
  {
    title: "افزودن PWA در فایرفاکس",
    logo: "/images/vectors/browsers/firefox.svg",
    images: firefoxImages,
  },
  {
    title: "افزودن PWA در مروگر اج",
    logo: "/images/vectors/browsers/edge.svg",
    images: edgeImages,
  },
  {
    title: "افزودن PWA در اپرا",
    logo: "/images/vectors/browsers/opera.svg",
    images: operaImages,
  },
];

export default () => {
  return (
    <GlobalContainer currentNav={4} deleteHeader={true}>
      <Container style={{ paddingTop: 15 }}>
        <Accordion dir="rtl">
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>دریافت اپلیکیشن</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Wrapper>
              <Title
                href="https://cdn.bamadar.com/app/bamadar.apk"
                target="_blank"
              >
                <AndroidIcon />
                دریافت اپلیکیشن اندروید
              </Title>
              <Title
                href="https://cdn.bamadar.com/app/bamadar.ipa"
                target="_blank"
              >
                <AppleIcon />
                دریافت اپلیکیشن ios
              </Title>
            </Wrapper>
          </AccordionDetails>
        </Accordion>

        {content.map(({ title, images, logo }) => (
          <Accordion dir="rtl">
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Logo src={logo} />
              <Typography> {title}</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <ImagesCon>
                {images.map((image) => (
                  <PwaImg src={addr + image} />
                ))}
              </ImagesCon>
            </AccordionDetails>
          </Accordion>
        ))}
      </Container>
    </GlobalContainer>
  );
};

const ImagesCon = styled.div`
  display: grid;
  margin: auto;
`;

const PwaImg = styled.img`
  width: 250px;
  height: 450px;
  margin-bottom: 15px;
`;

const Title = styled.a`
  font-size: 0.9rem;
  margin-top: 10px;
  padding: 10px 20px;
  border: 1px solid ${(props) => props.theme.palette.primary};
  border-radius: 10px;
  color: ${(props) => props.theme.palette.primary};
  text-decoration: none;
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 75%;

  svg {
    font-size: 1.5rem;
    margin-left: 5px;
    color: ${(props) => props.theme.palette.primary};
  }
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  max-width: 300px;
  margin: auto;
`;

const Logo = styled.img`
  height: 30px;
  width: 30px;
  margin-left: 5px;
`;
