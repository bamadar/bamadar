import React from "react";
import SubCategories from "../../containers/SubCategories";
import GlobalContainer from "../../containers/GlobalContainer/GlobalContainer";
import { useRouter } from "next/router";
import useSWR from "swr";
import { DeviceContext } from "../../context/DeviceContext";

const Page = ({ main }) => {
  const { query } = useRouter();
  const catId = query.id ? query.id : undefined;
  return (
    <GlobalContainer currentNav={1}>
      <SubCategories mainCats={main} catId={catId} />
    </GlobalContainer>
  );
};

const CatPage = () => {
  const { data } = useSWR("all_categories");
  let main = data || [];
  return <Page main={main} />;
};

export default CatPage;
