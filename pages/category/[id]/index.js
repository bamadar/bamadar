import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import InfiniteScroll from "react-infinite-scroll-component";
import styled from "styled-components";
import GlobalContainer from "./../../../containers/GlobalContainer/GlobalContainer";
import { searchProducts } from "../../../context/server";
import ProductsCon from "../../../components/productsCon";

const Products = () => {
  const { query } = useRouter();

  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);

  const [page, setPage] = useState({
    current: query.page || 1,
    searchedItem: query.q || "",
    products: [],
  });

  const action = query.id;
  const q = query.q || ""; // search name

  useEffect(() => {
    const type = action === "search" ? "byName" : "byCat";
    const name = action === "search" ? q : action;

    page.products = products;
    if (page.searchedItem !== q) {
      page.searchedItem = q;
      page.current = 1;
      page.products = [];
      setLoading(true);
    }

    const isCancelled = { current: false };

    searchProducts(name, type, page.current).then(({ products }) => {
      if (isCancelled.current) return;

      if (products) {
        setProducts([...page.products, ...products]);
        setLoading(false);
      }
    });

    return () => {
      isCancelled.current = true;
    };
  }, [action, page]);

  return (
    <GlobalContainer currentNav={1}>
      <Content
        dataLength={products.length}
        next={() => {
          products.length > 0 &&
            setPage({ ...page, current: page.current + 1 });
        }}
        hasMore={true}
      >
        <ProductsCon products={products} loading={loading} />
      </Content>
    </GlobalContainer>
  );
};

export default Products;

const Content = styled(InfiniteScroll)`
  max-width: 1200px;
  margin: auto;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  padding-bottom: 20px;
`;
