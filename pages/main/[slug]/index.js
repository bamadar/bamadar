import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import InfiniteScroll from "react-infinite-scroll-component";
import styled from "styled-components";
import GlobalContainer from "../../../containers/GlobalContainer/GlobalContainer";
import { searchProducts } from "../../../context/server";
import ProductsCon from "../../../components/productsCon";
import Router from "next/router";
import { Carousel as CaroselCon } from "../../../components/Carousel";

export default () => {
  return <GlobalContainer currentNav={1}>hello</GlobalContainer>;
};

const CategoryTitle = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: 0.8rem;
  padding: 0 16px;
`;

const Title = styled.p`
  font-weight: bold;
`;

const SeeMore = styled.p`
  color: ${(props) => props.theme.palette.primary};
`;

const Category = styled.div`
  background: #ecedef;
  width: 100px;
  padding: 16px 8px;
  display: grid;
  justify-content: center;
  align-items: center;
  text-align: center;
  border-radius: 8px;
  margin-right: 8px;

  span {
    margin-top: 8px;
    color: #818389;
    font-size: 0.7rem;
    font-weight: bold;
  }

  img {
    width: 100px;
    height: 90px;
  }
`;

const Carousel = styled(CaroselCon)`
  margin-top: 16px;

  & > div {
    transform: translateX(0);
    margin-right: 8px;
  }
`;

const Empty = styled.div`
  width: 20px;
  height: 50px;
  display: block;
  color: transparent;
`;
