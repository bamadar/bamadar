import React from "react";
import Profile from "./../containers/Profile";
import GlobalContainer from "./../containers/GlobalContainer/GlobalContainer";

export default () => {
  return (
    <GlobalContainer currentNav={4} deleteHeader={true}>
      <Profile />
    </GlobalContainer>
  );
};
