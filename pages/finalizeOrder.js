import React, { useEffect, useState, useCallback, useContext } from "react";
import FinalizeOrder from "./../containers/FinalizeOrder";
import GlobalContainer from "./../containers/GlobalContainer/GlobalContainer";
import axios from "axios";
import Router from "next/router";
import { toId, cityToPersian } from "./../utils/finalize-order";
import { CardContext } from "../context/cardContext";
import { config } from "../context/server";

export const Finalize = () => {
  const [userOrder, setUserOrder] = useState([]);
  const [cartDetails, setCartDetails] = useState({
    sum: 0,
    delivery: 0,
    finalSum: 0,
  });
  const [loader, setLoader] = useState(false);
  const [snackBar, setsnackBar] = useState(false);

  const { items, increaseCardBalance, decreaseCardBalance } = useContext(
    CardContext
  );

  const inMadarPack = useCallback(async () => {
    const mad = await window.localStorage.getItem("MadarPack");
    if (mad) {
      const packLocalStorage = JSON.parse(mad);
      setUserOrder(packLocalStorage.pack);
    }
  }, [setUserOrder]);

  useEffect(() => {
    inMadarPack();
  }, []);

  const FinalizeMyOrder = async () => {
    const city = userOrder.user.city,
      addr = userOrder.user.address;

    userOrder.user.address = addr.replace(`&${city}`, cityToPersian(city));
    userOrder.user.city = toId(city);
    userOrder.user.mobile = window.localStorage.getItem("MadarCell");
    userOrder.card = JSON.parse(window.localStorage.getItem("card"));
    userOrder.description = window.localStorage.getItem("orderDescription");

    if (items.length < 1) {
      setsnackBar(true);
      return;
    }

    const res = await axios.post(config + "/api/order-submit", {
      package: JSON.stringify(userOrder),
      headers: {
        Authorization: `bearer${window.localStorage.getItem("access_token")}`,
      },
    });

    if (res.data.message == "ok") {
      if (userOrder.user.payment == "cash") {
        window.localStorage.setItem("orderDescription", "");
        Router.push(`/cashSuccess?trackingCode=${res.data.trackingCode}`);
      } else {
        window.location = res.data.url;
      }
    } else {
      if (userOrder.user.payment == "cash") {
        Router.push("/cashUnsuccess");
      }
    }
  };

  useEffect(() => {
    let sum = 0;

    items.forEach((item) => {
      sum += item.sell_price * item.balance;
    });

    if (userOrder && userOrder.user && userOrder.user.city == "dezful") {
      if (sum >= 1800000) {
        setCartDetails({
          sum,
          delivery: 0,
          finalSum: sum + 0,
        });
      } else if (sum < 1800000) {
        setCartDetails({
          sum,
          delivery: 50000,
          finalSum: sum + 50000,
        });
      }
    } else {
      if (sum >= 3000000) {
        setCartDetails({
          sum,
          delivery: 0,
          finalSum: sum + 0,
        });
      } else if (sum < 3000000) {
        setCartDetails({
          sum,
          delivery: 100000,
          finalSum: sum + 100000,
        });
      }
    }
  }, [items, userOrder]);

  return (
    <GlobalContainer currentNav={3} deleteHeader={true}>
      <FinalizeOrder
        cart={items}
        cartDetails={cartDetails}
        userOrder={userOrder}
        snackBar={snackBar}
        loader={loader}
        setLoader={setLoader}
        FinalizeMyOrder={FinalizeMyOrder}
        add={increaseCardBalance}
        remove={decreaseCardBalance}
      />
    </GlobalContainer>
  );
};

export default () => <Finalize />;
