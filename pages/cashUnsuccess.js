import React, { useState, useEffect, useCallback } from "react";
import styled, { css } from "styled-components";
import Router from "next/router";
import { MultiplyIcon } from "../components/icons/Icons";

export default function PayStatus() {
  return (
    <Wrapper>
      <LogoCon>
        <MultiplyIcon color={"#e74c3c"} />
      </LogoCon>
      <Title>پرداخت ناموفق بود</Title>
      <Button onClick={() => Router.push("/order")}>بازگشت به ثبت سفارش</Button>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  direction: rtl;
`;
const LogoCon = styled.div`
  width: 100px;
  height: 100px;
  border-radius: 50px;
  outline: 1px dashed ${(props) => props.theme.palette.secondary};
  outline-offset: -5px;
  -moz-outline-radius: 15px;
  overflow: hidden;
  margin: 40px auto;
  margin-bottom: 50px;
  svg {
    width: 50px;
    height: 50px;
    margin: 25px auto;
    display: block;
  }
`;
const Title = styled.h2`
  font-size: 16px;
  font-weight: 400;
  color: ${(props) => props.theme.colors.darkGray};
  text-align: center;
  margin-bottom: 50px;
`;

const Button = styled.div`
  width: 150px;
  text-align: center;
  display: block;
  padding: 8px 10px;
  margin: 10px auto;
  border-radius: 5px;
  background: ${(props) => props.theme.palette.primary};
  color: ${(props) => props.theme.colors.lightGray};
  cursor: pointer;
  font-size: 13px;
`;
