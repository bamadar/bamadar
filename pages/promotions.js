import { useState } from "react";
import ProductsCon from "../components/productsCon";
import useSWR from "swr";
import GlobalContainer from "../containers/GlobalContainer/GlobalContainer";

const Products = () => {
  const [products, setProducts] = useState([]);
  const { data } = useSWR("get_promotions");

  if (data && products.length == 0) setProducts(data?.data);

  return (
    <GlobalContainer currentNav={0}>
      <ProductsCon
        products={products}
        loading={products.length > 0 ? false : true}
      />
    </GlobalContainer>
  );
};

export default Products;
