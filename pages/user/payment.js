import React, { useState, useEffect } from "react";
import styled, { css } from "styled-components";
import { Header } from "../../components/Header/Header";
//Importing Pages

const Payment = () => {
  return (
    <MainWrapper>
      <Header display="" />
      <ContentWrapper>
        <Boxed>
          <Title>
            <img src="/images/Soon.png" width="200px" alt="بزودی" />
          </Title>
          <Title> خرید شما با موفقیت انجام شد</Title>
        </Boxed>
      </ContentWrapper>
    </MainWrapper>
  );
};

export default Payment;
const MainWrapper = styled.div`
  margin: 0 auto;
  width: 100%;
  max-width: ${(p) => p.theme.width.maxWidth};
  height: 100vh;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  align-items: center;
  font-family: IRANSans;
`;

const ContentWrapper = styled.div`
  flex-grow: 1;
  width: 100%;
  background: ${(props) => props.theme.colors.white};
  border-radius: 16px 16px 0px 0px;
  overflow: auto;
  margin-top: -20px;
  color: ${(props) => props.theme.colors.black};

  ${(p) =>
    p.boxed &&
    css`
      padding: 20px;
    `}
`;

const Boxed = styled.div`
  padding: 20px;
`;
const MsgBox = styled.div`
  position: fixed;
  width: 250px;
  height: 40px;
  line-height: 40px;
  background: ${(props) => props.theme.colors.gren};
  bottom: ${(d) => (d.msgstatus ? d.msgstatus : "-50px")};
  margin: 0px auto;
  text-align: center;
  float: right;
  color: ${(props) => props.theme.colors.white};
  border-radius: 5px;
  transition: all 500ms;
`;

const Title = styled.h3`
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 30px;
  text-align: center;
  margin: 0px auto;
  color: ${(props) => props.theme.colors.black};
`;

// color pallet https://colorhunt.co/palette/175777
