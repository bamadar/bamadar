import React, { useContext } from "react";
import styled from "styled-components";
import Router, { useRouter } from "next/router";
import { TickIcon } from "../components/icons/Icons";
import { CardContext } from "../context/cardContext";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import { DescAlert } from "../components/Alert";

export default function PayStatus() {
  const { query } = useRouter();
  const { clearCart } = useContext(CardContext);
  clearCart();

  return (
    <Wrapper>
      <Container>
        <Success />

        <Title>سفارش شما با موفقیت ثبت شد</Title>
        <Title style={{ fontWeight: "bold" }}>
          شماره سفارش: {query.trackingCode}
        </Title>
        <DescAlert
          type="info"
          variant="standard"
          title="مدت زمان  ثبت سفارش تا دریافت سفارش 2ساعت خواهد بود"
          subTitle=" "
        />
        <Button
          onClick={() => {
            Router.replace("/history");
            return;
          }}
        >
          مشاهده لیست سفارشات
        </Button>
      </Container>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  height: 100vh;
  align-items: center;
  justify-content: center;
`;

const Container = styled.div`
  display: grid;
  justify-content: center;
  justify-items: center;
  align-items: center;
  grid-gap: 15px;
`;

const Title = styled.h2`
  font-size: 16px;
  font-weight: 400;
  color: ${(props) => props.theme.colors.black};
  text-align: center;
`;

const Button = styled.div`
  text-align: center;
  display: inline;
  padding: 10px 15px;
  border-radius: 5px;
  background: ${(props) => props.theme.palette.secondary};
  color: ${(props) => props.theme.colors.lightGray};
  cursor: pointer;
  font-size: 1rem;
`;

const Success = styled(CheckCircleIcon)`
  width: 150px !important;
  height: 150px !important;
  color: green;

  svg {
    width: 150px !important;
    height: 150px !important;
    color: green;
  }
`;
