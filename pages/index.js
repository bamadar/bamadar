import React, { useEffect, useState } from "react";
import styled, { css } from "styled-components";
import { Home } from "../containers/Home";
import GlobalContainer from "../containers/GlobalContainer/GlobalContainer";
import { config } from "../context/server";
import { store } from "react-notifications-component";

export default () => {
  let guided = undefined;
  if (typeof window != "undefined")
    guided = window.localStorage.getItem("guided");

  return (
    <GlobalContainer withTour={guided == "true" ? false : true}>
      <Home config={config} />
    </GlobalContainer>
  );
};

export const MainWrapper = styled.div`
  margin: 0 auto;
  width: 100%;
  max-width: ${(p) => p.theme.width.maxWidth};
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  align-items: center;
  font-family: IRANSans;
`;

export const ContentWrapper = styled.div`
  flex-grow: 1;
  width: 100%;
  background: ${(props) => props.theme.colors.white};
  border-radius: 16px 16px 0px 0px;
  overflow: auto;
  margin-top: -20px;
  color: ${(props) => props.theme.colors.black};

  ${(p) =>
    p.boxed &&
    css`
      padding: 20px;
    `}
`;
