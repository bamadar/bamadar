import Head from "next/head";
import React from "react";
import { ThemeProvider, createGlobalStyle } from "styled-components";
import "../public/dana.css";
import Router from "next/router";
import withGA from "next-ga";
import theme from "./../theme/theme";
import { SWRConfig } from "swr";
import { fetcher } from "../context/server";
import CardContext from "../context/cardContext";
import "react-notifications-component/dist/theme.css";
import DeviceContext from "../context/DeviceContext";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import NextHead from "next/head";

const BamadarApp = (props) => {
  const { Component, pageProps } = props;

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <CardContext>
        <DeviceContext>
          <SWRConfig value={{ fetcher }}>
            <Head>
              <link rel="manifest" href="/manifest.json" />
              {/* <link rel="shortcut icon" href="/static/favicon.ico" /> */}
              <meta property="og:title" content="فروشگاه اینترنتی بامادر" />
              <meta
                property="og:description"
                content="بررسی، انتخاب و خرید آنلاین"
              />
              <meta property="og:image" content="/images/Logo.png" />

              <meta name="mobile-web-app-capable" content="yes" />
              <meta name="apple-mobile-web-app-capable" content="yes" />
              <meta name="apple-mobile-web-app-title" content="بامادر" />
              <meta name="apple-touch-fullscreen" content="yes" />

              <meta http-equiv="cache-control" content="no-cache" />
              <meta http-equiv="expires" content="0" />
              <meta http-equiv="pragma" content="no-cache" />

              <meta
                name="apple-mobile-web-app-status-bar-style"
                content="default"
              />
              <meta name="theme-color" content="#A13E79" />

              <link
                rel="apple-touch-icon"
                sizes="192x192"
                href="/images/logos/logo192.png"
              />
              <link
                rel="apple-touch-icon"
                sizes="144x144"
                href="/images/logos/logo144.png"
              />
              <link
                rel="apple-touch-icon"
                sizes="96x96"
                href="/images/logos/logo96.png"
              />
              <link
                rel="apple-touch-icon"
                sizes="72x72"
                href="/images/logos/logo72.png"
              />
              <link
                rel="apple-touch-icon"
                sizes="48x48"
                href="/images/logos/logo48.png"
              />

              <link
                rel="app-touch-startup-image"
                href="/images/logos/logo512.png"
              />
              <link
                rel="app-touch-icon"
                href="/images/logos/logo48.png"
                sizes="48x48"
              />
              <link
                rel="app-touch-icon"
                href="/images/logos/logo72.png"
                sizes="72x72"
              />
              <link
                rel="app-touch-icon"
                href="/images/logos/logo96.png"
                sizes="96x96"
              />
              <link
                rel="app-touch-icon"
                href="/images/logos/logo144.png"
                sizes="144x144"
              />
              <link
                rel="app-touch-icon"
                href="/images/logos/logo168.png"
                sizes="168x168"
              />
              <link
                rel="app-touch-icon"
                href="/images/logos/logo192.png"
                sizes="192x192"
              />
              <link
                rel="app-touch-icon"
                href="/images/logos/logo256.png"
                sizes="256x256"
              />
              <link
                rel="app-touch-icon"
                href="/images/logos/logo512.png"
                sizes="512x512"
              />

              <title>فروشگاه اینترنتی بامادر</title>
            </Head>
            <GlobalStyle />

            <Component {...pageProps} />
          </SWRConfig>
        </DeviceContext>
      </CardContext>
    </ThemeProvider>
  );
};

export default withGA("UA-165401046-1", Router)(BamadarApp);

const GlobalStyle = createGlobalStyle`
  *{
    padding:0;
    margin: 0;
    box-sizing: border-box;
    outline:none !important;
    -webkit-tap-highlight-color:  transparent !important ;
  }

  .rec-slider-container{
    margin: 0;
  }

  .notification-content {
    direction: rtl;
  }

  .raychat_main_button {
    width: 55px !important;
    height: 55px !important;
  }

  @media only screen and (max-width: 767px) {
    #raychatBtn{
    /* position: absolute; */
    bottom: 0;
    left: calc(50% - 29px);
    opacity: 0 !important;
    margin-bottom: 0px !important;

  }
  .raychat_main_button {
    width: 70px !important;
    height: 60px !important;
    border-radius: 0 !important;
  }
  }
  .rec-slider-container {
    margin: 0;
  }
  
  body {
    background: ${(props) => props.theme.colors.white};
    font-family: IRANSans  !important;
    legend,span,p,input,div,select,option,textarea{
      font-family: IRANSans  !important;
    }
  }
`;
