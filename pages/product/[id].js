import React, { useContext } from "react";
import { useRouter } from "next/router";
import useSWR from "swr";
import styled from "styled-components";
import { Price } from "../../components/shared/Price";
import { Grid, Paper } from "@material-ui/core";
import ProductOptions from "../../components/ProductOptions";
import { CardContext } from "../../context/cardContext";
import GlobalCon from "../../containers/GlobalContainer/GlobalContainer";
import { config } from "../../context/server";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import FileCopyOutlinedIcon from "@material-ui/icons/FileCopyOutlined";
import Progress from "../../components/Loading";
import { pushNotification } from "../../components/Notification";
import { PromoSlider } from "../../components/PromoSlider";
import ShareIcon from "@material-ui/icons/Share";
import Skeleton from "@material-ui/lab/Skeleton";

import Head from "next/head";

const ImageContainer = ({ image }) => (
  <ImageWrapper>
    <TransformWrapper doubleClick={{ mode: "zoomOut" }}>
      <TransformComponent>
        <img src={image ? config + image : "../images/no image.png"} />
      </TransformComponent>
    </TransformWrapper>
  </ImageWrapper>
);

const PriceContainer = ({
  status,
  sell_price,
  price,
  measurement,
  discount,
  image,
  description,
  number,
  data,
}) => {
  const {
    addToCard,
    increaseCardBalance,
    decreaseCardBalance,
    items,
  } = useContext(CardContext);

  const balance = data
    ? items.filter((thisItem) => thisItem.id === data.data.id)
    : [];

  const Options = () => (
    <OptionCon xs={6} hasDiscount={discount > 0}>
      <Center>
        <ProductOptions
          increase={() => increaseCardBalance(data.data)}
          addToCard={() => addToCard(data.data)}
          remove={() => decreaseCardBalance(data.data)}
          status={status}
          itemBalance={balance}
          measurement={measurement}
          number={number}
          owner="productPage"
          status={data?.data?.status}
        />
      </Center>
    </OptionCon>
  );
  const Prices = () => (
    <Grid container xs={6}>
      {
        <TopPrices xs={12} container hasDiscount={discount > 0}>
          <SellPriceCon xs={8}>
            {sell_price ? (
              <SellPrice>{sell_price}</SellPrice>
            ) : (
              <Skeleton variant="rect" width={150} height={35} />
            )}
          </SellPriceCon>
          {discount > 0 && (
            <Discount xs={4}>
              <span>{discount}%</span>
            </Discount>
          )}
        </TopPrices>
      }
      {discount > 0 && <MainPrice xs={6}>{price}</MainPrice>}
    </Grid>
  );

  const theBalance = balance[0]?.balance;

  return (
    <>
      <PriceCon container xs={12}>
        <Prices />
        {data ? <Options /> : <Skeleton variant="rect" width={150} />}
      </PriceCon>

      <OrdersCon>
        <UserOrder>
          {data ? (
            <>
              <span>مقدار سفارش: </span>
              {theBalance ? theBalance : 0}{" "}
              {measurement == "گرم" ? "کیلوگرم" : measurement}
            </>
          ) : (
            <Skeleton variant="rect" width={180} height={32} />
          )}
        </UserOrder>

        <div>
          {data ? (
            theBalance && (
              <>
                <span>جمع پرداختی: </span>
                <SumOfOrder>{theBalance * sell_price}</SumOfOrder>
              </>
            )
          ) : (
            <Skeleton variant="rect" width={180} height={32} />
          )}
        </div>

        <div>
          {data ? (
            theBalance &&
            price - sell_price > 0 && (
              <>
                <span>تخفیف بامادر:</span>
                <MadarDiscount>
                  {theBalance * (price - sell_price)}
                </MadarDiscount>
              </>
            )
          ) : (
            <Skeleton variant="rect" width={180} height={32} />
          )}
        </div>
      </OrdersCon>
    </>
  );
};

export default () => {
  const { query } = useRouter();
  const productId = query.id;
  const { data } = useSWR(`product/${productId}`);
  const topProducts = useSWR("get_promotions")?.data;

  let {
    name,
    status,
    sell_price,
    price,
    measurement,
    discount,
    images,
    description,
    number,
  } = (data && data.data) || {};
  const image = images && images?.images[768];

  const {
    addToCard,
    increaseCardBalance,
    decreaseCardBalance,
    items,
  } = useContext(CardContext);

  const copyToClipboard = () => {
    var dummy = document.createElement("textarea");
    document.body.appendChild(dummy);
    dummy.value = `${data?.data.name} \n\n${window.location.href}`;
    dummy.select();
    if (document.execCommand("copy"))
      pushNotification("اطلاعات با موفقیت کپی شد", " ", "success");

    document.body.removeChild(dummy);
  };

  const sharePage = async () => {
    const shareData = {
      title: "بامادر",
      text: `${data?.data.name}`,
      url: window.location.href,
    };
    try {
      await navigator.share(shareData);
      pushNotification("اشتراک گذاری با موفقیت انجام شد", " ", "success");
    } catch (err) {
      pushNotification("اشتراک گذاری با خطا مواجه شد", " ", "danger");
    }
  };

  return (
    <GlobalCon>
      {data && (
        <Head>
          <title>{`${data?.data.name}`}</title>
          <meta
            property="og:title"
            content={`${data?.data.name}`}
            data-react-helmet="true"
          />
        </Head>
      )}
      <Con>
        <ImageContainer image={image} />
        <SecCon>
          <ProductTitleCon>
            <ProductTitle>
              {name ? (
                name
              ) : (
                <Skeleton variant="rect" width={210} height={25} />
              )}
            </ProductTitle>
          </ProductTitleCon>

          <PriceContainer
            status={status}
            sell_price={sell_price}
            price={price}
            measurement={measurement}
            discount={discount}
            image={image}
            description={description}
            number={number}
            data={data}
          />

          <Accs>
            {data ? (
              <FileCopyOutlinedIcon onClick={copyToClipboard} />
            ) : (
              <Skeleton variant="rect" width={35} height={35} />
            )}
            {data ? (
              <ShareIcon onClick={sharePage} />
            ) : (
              <Skeleton
                variant="rect"
                width={35}
                height={35}
                style={{ marginRight: 5 }}
              />
            )}
          </Accs>
        </SecCon>
      </Con>
      <PromoCon>
        <PromoSlider
          proPros={data?.data.related || []}
          items={items}
          addToCard={addToCard}
          increaseCardBalance={increaseCardBalance}
          decreaseCardBalance={decreaseCardBalance}
          config={config}
          logo="../images/no image.png"
        />
      </PromoCon>
    </GlobalCon>
  );
};

const OrdersCon = styled.div`
  line-height: 2rem;
  display: grid;
  grid-row-gap: 5px;
  padding: 0 10px;

  span {
    color: #62666d;
  }
`;
const UserOrder = styled.div``;
const SumOfOrder = styled(Price)`
  display: inline-block;
`;
const MadarDiscount = styled(Price)`
  color: ${({ theme }) => theme.palette.secondary};
  display: inline-block;
`;
const OptionCon = styled(Grid)`
  margin-top: ${({ hasDiscount }) => (hasDiscount ? "-25px" : "0")};
`;

const SecCon = styled.div`
  max-width: 600px;
  margin: auto;
`;

const Con = styled(Grid)`
  direction: rtl;
  box-shadow: 0px 2px 9px 0px rgb(0 0 0 / 10%);
  padding-bottom: 16px;
  background: #fff;
`;
const ImageWrapper = styled(Grid)`
  background: #e6e6e6;
  width: 100%;
  height: 35vh;
  min-height: 350px;
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    border-radius: 16px;
    width: 300px;
    height: 300px;
  }
`;

const SellPriceCon = styled(Grid)`
  border-bottom-left-radius: 20px;
  border-top-left-radius: 20px;
  transform: translateX(-10px);
  background: #fff;
`;

const ProductTitleCon = styled.div`
  box-shadow: 0px 2px 9px 0px rgb(0 0 0 / 10%);
  height: auto;
  padding: 10px;
  display: grid;
  align-items: center;
  font-weight: bold;

  background: #fff;
  direction: rtl;
`;
const PriceCon = styled(Grid)`
  text-align: center;
  background: #fff;
  padding: 16px 5px;
  justify-content: center;
  align-items: center;
`;
const SellPrice = styled(Price)`
  width: 100%;
`;
const MainPrice = styled(Grid)`
  text-decoration: line-through;
  color: rgb(0 0 0 / 50%);
`;
const Discount = styled(Grid)`
  color: #fff;

  span {
    background: ${({ theme }) => theme.palette.secondary};
    padding: 2.5px 5px;
    border-bottom-left-radius: 20px;
    border-top-left-radius: 20px;
    display: block;
    font-size: 0.9rem;
  }
`;

const Center = styled.div`
  justify-content: center;
  align-items: center;
  display: flex;
`;

const TopPrices = styled(Grid)`
  border: 1px solid #e6e6e6;
  border-width: ${({ hasDiscount }) => (hasDiscount ? "1px" : "0")};
  border-radius: 20px;
`;

const Accs = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  background: #fff;
  padding: 0 15px;
  padding-top: 8px;

  svg {
    width: 25px;
    height: 25px;
    margin-right: 25px;
    color: rgb(0 0 0 / 80%);
    cursor: pointer;
  }
`;

const ProductTitle = styled.p`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const PromoCon = styled.div`
  padding-top: 16px;
  padding-bottom: 4px;
  margin-right: 8px;
`;
