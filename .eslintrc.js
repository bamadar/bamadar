module.exports = {
  plugins: ["react", "react-hooks"],
  extends: ["plugin:react-hooks/recommended"],
  parserOptions: {
    ecmaVersion: 2019,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    },
  },
};
