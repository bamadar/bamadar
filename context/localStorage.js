const NO_LOCAL_STORAGE = typeof window === "undefined" || !localStorage;

export const getMadarPack = () => {
  if (NO_LOCAL_STORAGE) return;

  const mad = window.localStorage.getItem("MadarPack");
  if (mad) {
    const packLocalStorage = JSON.parse(mad),
      pack = packLocalStorage && packLocalStorage.pack;
    return pack;
  }
};

export const getUserInfo = () => {
  const pack = getMadarPack(),
    userInfo = pack && pack.user;
  const { address, city, family, mobile, name, pelak } = userInfo
    ? userInfo
    : {};
  return { address, city, family, mobile, name, pelak };
};

export const getCard = () => {
  const card = window.localStorage.getItem("card");
  if (card) {
    const cartLocalStorage = JSON.parse(card);
    return cartLocalStorage;
  }
};

export const getMadarCell = () => {
  return window.localStorage.getItem("MadarCell");
};

export const setInfo = (values) => {
  let card = window.localStorage.getItem("card");
  if (card) card = JSON.parse(card);

  let pack = {
    user: values,
    card: card,
  };

  let completeAddr =
    `&${pack.user.city}` +
    (pack.user.address && pack.user.address.replace(`&${pack.user.city}`, ""));
  pack.user.address = completeAddr;

  window.localStorage.setItem("MadarPack", JSON.stringify({ pack }));
};
