import React, { useState, useEffect } from "react";

export const DeviceContext = React.createContext({
  isMobile: true,
});
const NO_LOCAL_STORAGE = typeof window === "undefined" || !localStorage;

export default (props) => {
  const [isMobile, setIsMobile] = useState(true);

  useEffect(() => {
    if (!NO_LOCAL_STORAGE) {
      const w = window.innerWidth;
      w < 768 ? setIsMobile(true) : setIsMobile(false);
    }
  }, []);

  return (
    <DeviceContext.Provider value={{ isMobile }}>
      {props.children}
    </DeviceContext.Provider>
  );
};
