import axios from "axios";

export const config = "https://www.bamadar.com/data";
export const subCatApi = "/category/";
export const productsAction = "/products?action=";

export const fetcher = (url) =>
  fetch(config + "/api/" + url).then((res) => res.json());

export const getPromoSlider = async () => {
  const request = await fetch(config + `/api/get_promotions`);
  const productsJson = await request.json();
  return productsJson.data;
};

export const getCategories = async () => {
  const response = await axios.get(config + "/api/main_categories");
  const arr = response.data.data;
  return arr;
};

export const searchProducts = async (name, type, page) => {
  if (type == "byName") {
    if (name.length >= 2) {
      let url = config + `/api/get_products?name=${name}`;
      if (page) {
        url += "&page=" + page;
      }
      const request = await fetch(url);
      const productsJson = await request.json();
      return { pageData: productsJson.meta, products: productsJson.data };
    } else {
      return { pageData: undefined, products: undefined };
    }
  } else if (type == "byCat") {
    let url = config + `/api/get_products?category=${name}`;
    if (page) {
      url += "&page=" + page;
    }
    const request = await fetch(url);
    const productsJson = await request.json();

    return { pageData: productsJson.meta, products: productsJson.data };
  }
};

export const submitOrder = (userOrder) =>
  axios.post(config + "/api/order-submit", {
    package: JSON.stringify(userOrder),
    headers: {
      Authorization: `bearer${window.localStorage.getItem("access_token")}`,
    },
  });

export const loginUser = (mobile) =>
  axios.post(config + "/api/user/login", { mobile });

export const verifyUser = (mobile, otp) =>
  axios.post(config + "/api/user/verify", { mobile, otp });

export const getUserInfo = () => {
  const token = window.localStorage.getItem("access_token");
  return axios.post(
    config + "/api/user/me",
    { token },
    {
      headers: {
        Accept: "application/json",
        Authorization: `bearer${token}`,
      },
    }
  );
};

export const postUserInfo = (values) =>
  axios.post(
    config + "/api/user/update_info",
    {
      first_name: values.name,
      last_name: values.family,
      address: values.address,
      no: values.pelak,
      national_code: "",
      phone: values.tel,
      referral: values.referral,
      email: "",
      gender: "",
      location: values.location,
    },
    {
      headers: {
        Accept: "application/json",
        Authorization: `bearer${window.localStorage.getItem("access_token")}`,
      },
    }
  );

export const getUserOrders = (status = undefined, trackingCode = undefined) => {
  const token = window.localStorage.getItem("access_token");

  let Api = trackingCode
    ? config + `/api/user/orders/${trackingCode}`
    : config + "/api/user/orders";

  if (status) Api = config + `/api/user/orders?status=${status}`;

  return axios.post(
    Api,
    { token },
    {
      headers: {
        Accept: "application/json",
        Authorization: `bearer${token}`,
      },
    }
  );
};
