import React, { useEffect, useState } from "react";

export const CardContext = React.createContext({
  addToCard: () => {},
  increaseCardBalance: () => {},
  decreaseCardBalance: () => {},
  items: [],
  clearCart: () => {},
});
const NO_LOCAL_STORAGE = typeof window === "undefined" || !localStorage;

export default (props) => {
  const [didHydrate, setDidHydrate] = useState(false);
  const [items, setItems] = useState([]);

  // effect 1 - hydrate
  useEffect(() => {
    if (NO_LOCAL_STORAGE) return;

    if (didHydrate) {
      return;
    }
    const cart = JSON.parse(window.localStorage.getItem("card"));
    if (cart) {
      setItems(cart);
    } else {
      setItems([]);
    }
    setDidHydrate(true);
  }, [didHydrate]);

  // effect 2 - on change, save to local storage
  // if (!hydrated) return // wait for initial filling to happen
  useEffect(() => {
    if (NO_LOCAL_STORAGE) return;

    if (!didHydrate) {
      return;
    }
    window.localStorage.setItem("card", JSON.stringify(items));
  }, [didHydrate, items]);

  const addToCard = (product) => {
    let card = [];
    let addNew = true;

    if (items) {
      card = items;
      card.forEach((item, i) => {
        if (product.id == item.id) {
          card[i].balance = item.balance + 1;
          addNew = false;
        }
      });
    }

    if (addNew) {
      card.push({
        ...product,
        balance: product.measurement === "گرم" ? 0.1 : 1,
      });
    }
    setItems([...card]);
  };

  const clearCart = () => {
    if (items.length) {
      setItems([]);
      window.localStorage.setItem("card", []);
    }
  };

  const increaseCardBalance = (product) => {
    let tempCard = [...items];

    tempCard.forEach((item, i) => {
      if (product.id == item.id) {
        switch (item.measurement) {
          case "کیلوگرم":
            const curBalance = parseFloat(item.balance);
            const inc = curBalance < 0.5 ? 0.1 : 0.5;
            tempCard[i].balance = (curBalance + inc).toFixed(1);
            break;
          case "گرم":
            tempCard[i].balance = (parseFloat(item.balance) + 0.1).toFixed(1);
            break;

          default:
            tempCard[i].balance = item.balance + 1;
            break;
        }
      }
    });

    setItems(tempCard);
  };

  // add to basket
  const decreaseCardBalance = (product) => {
    let tempCard = [...items];
    let shouldRemoveId = false;

    tempCard.forEach((item, i) => {
      if (product.id == item.id) {
        if (item.balance > 1) {
          switch (item.measurement) {
            case "کیلوگرم":
              const curBalance = parseFloat(item.balance);
              const minus = curBalance <= 0.5 ? 0.1 : 0.5;
              tempCard[i].balance = (curBalance - minus).toFixed(1);
              break;
            case "گرم":
              tempCard[i].balance = (parseFloat(item.balance) - 0.1).toFixed(1);
              break;

            default:
              tempCard[i].balance = item.balance - 1;
              break;
          }
        } else {
          switch (item.measurement) {
            case "کیلوگرم":
              if (tempCard[i].balance <= 0.1) shouldRemoveId = item.id;
              else {
                const curBalance = parseFloat(item.balance);
                const minus = curBalance <= 0.5 ? 0.1 : 0.5;
                tempCard[i].balance = (curBalance - minus).toFixed(1);
              }
              break;
            case "گرم":
              if (tempCard[i].balance <= 0.1) shouldRemoveId = item.id;
              else
                tempCard[i].balance = (parseFloat(item.balance) - 0.1).toFixed(
                  1
                );

              break;
            default:
              shouldRemoveId = item.id;
              break;
          }
        }
      }
    });

    if (shouldRemoveId !== false) {
      tempCard = tempCard.filter((item) => {
        return !(item.id === shouldRemoveId);
      });
    }

    setItems(tempCard);
  };

  return (
    <CardContext.Provider
      value={{
        items,
        addToCard,
        increaseCardBalance,
        decreaseCardBalance,
        clearCart,
      }}
    >
      {props.children}
    </CardContext.Provider>
  );
};
