import React from "react";
import styled from "styled-components";
import Router from "next/router";

import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";

export default ({ display }) => {
  return (
    display && (
      <GoBackCon onClick={() => Router.back()}>
        <GoBack>
          <ArrowBackIosIcon />
        </GoBack>
      </GoBackCon>
    )
  );
};

const GoBackCon = styled.div`
  height: 40px;
  cursor: pointer;
  padding-left: 5px;
`;

const GoBack = styled.div`
  height: 40px;
  overflow: hidden;
  transform: translateY(7px);

  path {
    fill: #949494;
  }
`;
