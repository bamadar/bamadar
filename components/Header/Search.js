import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import { useRouter } from "next/router";
import SearchIcon from "@material-ui/icons/Search";

export default ({ searchInput, setSearchInput }) => {
  const { query, push, pathname } = useRouter();
  const inputRef = useRef();

  useEffect(() => {
    if (pathname == "/products") {
      inputRef.current.focus();
    }
  }, [pathname]);

  return (
    <Search id="searchBar">
      <IconWrapper>
        <SearchIcon />
      </IconWrapper>
      <Input
        ref={inputRef}
        placeholder="جستجو در"
        value={searchInput}
        onChange={(e) => {
          const value = e.currentTarget.value;
          setSearchInput(value);
          value.length > 2 &&
            push({
              pathname: "/products",
              query: { ...query, action: "search", q: value },
            });
        }}
      />
      {searchInput.length == 0 && <Typography>بامـــادر</Typography>}
    </Search>
  );
};

const Typography = styled.span`
  position: absolute;
  right: 50%;
  transform: translateY(0) translateX(50%);
  pointer-events: none;
  font-family: mostanad !important;
  font-size: 18px;
`;

const Search = styled.div`
  flex-grow: 1;
  height: 40px;
  direction: rtl;
  transition: 500ms;

  background: transparent;
  border-radius: 5px;

  display: flex;
  justify-content: space-between;
  align-items: center;
  overflow: hidden;
`;

const Input = styled.input`
  flex-grow: 1;
  display: inline-block;
  height: 100%;
  padding-right: 40px;

  transition: 250ms;
  outline: none;
  border: none;
  background: rgba(239, 239, 239, 0.7);
  border-radius: 5px;

  font-size: 1rem;
  font-family: IRANSans !important;
`;

const IconWrapper = styled.div`
  flex-shrink: 0;
  width: 20px;
  height: 25px;
  opacity: 0.4;
  transform: translateX(-15px) translateY(2px);
  position: absolute;
  top: 12px;
  transition: 250ms;
  path {
    width: 100%;
    fill: ${(p) => p.theme.colors.black};
  }
`;
