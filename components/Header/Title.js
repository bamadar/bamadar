import React from "react";
import styled from "styled-components";
import Router from "next/router";

export default () => {
  return (
    <Link onClick={() => Router.push("/")}>
      <Title> بامادر</Title>
    </Link>
  );
};

const Link = styled.a`
  text-decoration: none;
  cursor: pointer;
`;

const Title = styled.h3`
  width: 40%;
  height: 15px;
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  text-align: center;
  margin: 0px auto;
  color: ${(props) => props.theme.colors.purple};
`;
