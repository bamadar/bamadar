import React, { useEffect, useState, useRef, useContext } from "react";
import PropTypes from "prop-types";
import Toolbar from "@material-ui/core/Toolbar";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Slide from "@material-ui/core/Slide";
import Search from "./Search";
import styled from "styled-components";
import { useRouter } from "next/router";
import BackIcon from "./Back";
import { DeviceContext } from "../../context/DeviceContext";
import { DesktopHeader } from "./desktop";

function HideOnScroll(props) {
  const { children, window, winSize } = props;
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  if (winSize >= 1000) return children;

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  window: PropTypes.func,
};

export const Header = () => {
  const [searchInput, setSearchInput] = useState("");
  const { query, pathname } = useRouter();

  const q = query.q || "";
  const transparentNav =
    pathname == "/shop" || pathname == "/" || pathname == "/category";
  const display =
    transparentNav ||
    pathname == "/category/[id]" ||
    pathname == "/products" ||
    pathname == "/product/[id]" ||
    pathname == "/555" ||
    pathname == "/promotions";

  const navRef = useRef(null);

  useEffect(() => {
    setSearchInput(q);
  }, [pathname, q]);

  useEffect(() => {
    window.addEventListener("scroll", function (e) {
      if (!navRef.current) return;
      if (!transparentNav) return;

      const topPos = document.documentElement.scrollTop;
      const startTop = 125;
      if (topPos > startTop) {
        const opacity = topPos - startTop;
        navRef.current.style.backgroundColor = `rgba(250,250,250,${
          opacity / startTop
        }`;
        topPos > startTop + 20 &&
          (navRef.current.style.boxShadow = `0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)`);
      } else {
        navRef.current.style.backgroundColor = "transparent";
        navRef.current.style.boxShadow = "none";
      }
    });
  }, []);
  const { isMobile } = useContext(DeviceContext);

  return (
    <>
      {isMobile ? (
        <Nav ref={navRef} bgColor={transparentNav} display={display}>
          <BackIcon display={!(pathname === "/shop") && !(pathname === "/")} />
          <Search setSearchInput={setSearchInput} searchInput={searchInput} />
        </Nav>
      ) : (
        <DesktopHeader />
      )}
    </>
  );
};

const Nav = styled.div`
  background-color: ${({ bgColor }) => (bgColor ? "transparent" : "white")};
  display: ${({ display }) => (display ? "flex" : "none")};
  transition: 250ms;
  padding: 5px;
  width: 100%;
  max-width: 1000px;
  position: fixed;
  z-index: 2100;
  position: fixed;
  top: 0;

  @media only screen and (min-width: 768px) {
    display: none;
  }
`;
