import React, { useEffect, useState, useRef } from "react";
import Search from "./Search";
import styled from "styled-components";
import Router, { useRouter } from "next/router";
import RoomRoundedIcon from "@material-ui/icons/RoomRounded";
import ArrowBackIosRoundedIcon from "@material-ui/icons/ArrowBackIosRounded";
import FormatListBulletedOutlinedIcon from "@material-ui/icons/FormatListBulletedOutlined";
import HomeOutlinedIcon from "@material-ui/icons/HomeOutlined";
import { getUserInfo } from "../../../context/localStorage";
import { makeAddr } from "../../../utils/finalize-order";

export const Header = () => {
  const [searchInput, setSearchInput] = useState("");
  const { query, pathname } = useRouter();

  const q = query.q || "";

  useEffect(() => {
    setSearchInput(q);
  }, [pathname, q]);

  return (
    <Nav>
      <Search setSearchInput={setSearchInput} searchInput={searchInput} />
      <Accesibilities />
    </Nav>
  );
};

const Accesibilities = () => {
  const { pathname } = useRouter();
  const { city, pelak, address } = getUserInfo();
  const fullAddr = makeAddr(city, address, pelak);

  return (
    <MainAccs>
      <Address onClick={() => Router.push("/order?userInfo=true")}>
        {address && (
          <>
            <ArrowBackIosRoundedIcon />
            <span>{fullAddr}</span>
            <RoomRoundedIcon />
          </>
          // ) : (
          //   <Register>
          //     <ArrowBackIosRoundedIcon />
          //     <span>ثبت اطلاعات</span>
          //   </Register>
        )}
      </Address>

      <Container>
        <MainAcc
          className={pathname == "/category" && "active"}
          onClick={() => {
            Router.push("/category");
          }}
          id="categories"
        >
          <span>دسته بندی ها</span>
          <FormatListBulletedOutlinedIcon />
        </MainAcc>

        <MainAcc
          className={pathname == "/shop" && "active"}
          onClick={() => {
            Router.push("/");
          }}
          id="home"
        >
          <span>خانه</span>
          <HomeOutlinedIcon />
        </MainAcc>
      </Container>
    </MainAccs>
  );
};

const Container = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Address = styled.div`
  font-size: 0.8rem;
  display: flex;
  align-items: center;
  cursor: pointer;

  svg {
    color: ${(props) => props.theme.palette.primary};
    font-size: 1rem;
  }
`;

const MainAccs = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 20px;
  max-width: 1366px;
  margin: auto;
  justify-content: space-between;

  span {
    display: inline-block;
    max-width: 445px;
    max-height: 45px;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
  }
`;

const MainAcc = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 20px;
  transition: 250ms;
  border-bottom: 3px solid transparent;
  padding-bottom: 10px;
  cursor: pointer;

  svg {
    width: 20px;
    height: 20px;
    margin: 0 5px;
  }

  &.active {
    color: ${(props) => props.theme.palette.primary};
    border-bottom-color: ${(props) => props.theme.palette.primary};
  }
`;

const Nav = styled.div`
  background-color: #fff !important;
  display: grid;
  transition: 250ms;
  width: 100%;
  position: fixed;
  z-index: 2100;
  top: 0;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 10px;

  @media only screen and (max-width: 767px) {
    display: none;
  }
`;

const Register = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
