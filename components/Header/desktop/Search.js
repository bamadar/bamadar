import styled from "styled-components";
import Router, { useRouter } from "next/router";
import SearchIcon from "@material-ui/icons/Search";
import Cart from "../../icons/Cart";
import User from "../../icons/user";
import Badge from "@material-ui/core/Badge";
import { useContext } from "react";
import { CardContext } from "../../../context/cardContext";
import { Static } from "../../icons/Static";
import { useEffect, useRef } from "react";

export default ({ searchInput, setSearchInput }) => {
  const { query, push } = useRouter();

  const onChangeHandler = (e) => {
    const value = e.currentTarget.value;
    setSearchInput(value);
    value.length > 2 &&
      push({
        pathname: "/products",
        query: { ...query, action: "search", q: value },
      });
  };

  return (
    <DesktopInput onChangeHandler={onChangeHandler} searchInput={searchInput} />
  );
};

const DesktopInput = ({ searchInput, onChangeHandler }) => {
  const inputRef = useRef();
  const { pathname } = useRouter();
  useEffect(() => {
    if (pathname == "/products") {
      inputRef.current.focus();
    }
  }, [pathname]);

  return (
    <Wrapper>
      <SubWrapper>
        <Logo src="/images/Logo.png" onClick={() => Router.push("/")} />
        <DesCon id="searchBar">
          <DesSearch>
            <SearchIcon />
          </DesSearch>
          <DesInput
            value={searchInput}
            onChange={(e) => onChangeHandler(e)}
            ref={inputRef}
          />
        </DesCon>
        <QuickIcons />
      </SubWrapper>
    </Wrapper>
  );
};

const QuickIcons = () => {
  const { items } = useContext(CardContext);
  const { pathname } = useRouter();

  return (
    <Accs>
      <IconsCon
        onClick={() => Router.push("/history")}
        className={pathname == "/history" && "active"}
        id="history"
      >
        <Static />
        <p>خط سفارش</p>
      </IconsCon>

      <IconsCon
        onClick={() => Router.push("/cart")}
        className={pathname == "/cart" && "active"}
        id="cart"
      >
        <Badge badgeContent={items && items.length} color="secondary">
          <Cart />
        </Badge>

        <p>سبد خرید</p>
      </IconsCon>

      <IconsCon
        onClick={() => Router.push("/profile")}
        className={pathname == "/profile" && "active"}
        id="panel"
      >
        <User />
        <p>پروفایل</p>
      </IconsCon>
    </Accs>
  );
};

const IconsCon = styled.div`
  display: grid;
  justify-content: center;
  align-items: center;
  font-size: 0.7rem;
  line-height: 2rem;
  text-align: center;
  transition: 250ms;
  cursor: pointer;

  svg {
    width: 100%;
    transition: 150ms;
    fill: #fff;
  }

  &.active {
    svg {
      fill: #262f56;
    }
  }

  :hover {
    svg {
      fill: #262f56;
    }
  }
`;

const Wrapper = styled.div`
  border-bottom: 0.125rem solid rgb(229, 229, 229);
`;

const SubWrapper = styled.div`
  max-width: 1366px;
  padding: 15px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin: auto;
`;

const Logo = styled.img`
  width: 45px;
  height: 45px;
  cursor: pointer;

  @media only screen and (max-width: 767px) {
    display: none;
  }
`;

const Accs = styled.div`
  display: flex;
  padding: 0 15px;
  box-sizing: content-box;
  justify-content: space-between;
  align-items: center;
  width: 190px;
`;

const DesCon = styled.div`
  display: flex;
  align-items: center;
  z-index: 10;
  height: 100%;
  position: relative;
  width: 36vw;
  height: 40px;
`;

const DesSearch = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  background-color: ${(props) => props.theme.palette.primary};
  background-size: 1rem;
  background-repeat: no-repeat;
  background-position: 50%;
  transition: background-color 0.3s ease-in-out;
  border: 0;
  border-radius: 0.25rem;
  cursor: pointer;
  outline: 0;
  text-indent: -9999px;
  overflow: hidden;
  width: 2.3rem;
  z-index: 11;
  height: 40px;

  svg {
    fill: white;
    z-index: 12;
    position: absolute;
    right: 5px;
    top: 8px;
  }
`;

const DesInput = styled.input`
  position: relative;
  display: block;
  width: 100%;
  height: 40px;
  margin: 0;
  padding: 0.5rem 1rem 0.5rem 2.5rem;
  font-size: 0.875rem;
  font-weight: 400;
  color: #575757;
  border: 0.0625rem solid ${(props) => props.theme.palette.primary};
  border-radius: 0.25rem;
  background-color: #fff;
  box-shadow: none;
  line-height: 1.25;
  appearance: none;
  transition: border 0.3s ease-in-out;
  text-align: right;
  direction: rtl;
  font-family: IRANSans;

  :focus {
    border: 1px solid ${(props) => props.theme.palette.primary};
  }
`;
