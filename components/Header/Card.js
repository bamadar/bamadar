import React from "react";
import styled from "styled-components";
import Router from "next/router";
import { CardIcon } from "../icons/CardIcon";

export default ({ length }) => {
  return (
    <Card onClick={() => Router.push("/cart")}>
      <CardIconWrapper orders={length}>
        <CardIcon />
      </CardIconWrapper>
    </Card>
  );
};

const Card = styled.div`
  flex-shrink: 0;
  width: 50px;
  height: 50px;

  display: flex;
  justify-content: center;
  align-items: center;

  position: relative;
  cursor: pointer;

  :hover {
    background: rgba(166, 166, 168, 0.1);
    border-radius: 10px;
  }
`;
const CardIconWrapper = styled.div`
  width: 35px;
  height: 35px;

  path {
    width: 100%;
    transition: 500ms;
    fill: ${(props) =>
      props.orders ? props.theme.palette.secondary : "#a6a6a8"} ;
  }

  &::before {
    content: "${(props) => (props.orders ? props.orders : null)}";
    position: absolute;
    transition: 500ms;
    right: 0;
    top: 0;
    width: 20px;
    height: 20px;
    background-color:${(props) => (props.orders ? "#a6a6a8" : null)} ;
    border-radius: 15px;
    color: white;
    font-size: 0.8rem;
    font-weight: bolder;
    text-align: center
  }
`;
