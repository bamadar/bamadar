import React, { useEffect, useState, useContext } from "react";
import { useRouter } from "next/router";
import styled from "styled-components";
import GlobalContainer from "../../containers/GlobalContainer/GlobalContainer";
import LinearProgress from "../Loading";
import { config } from "../../context/server";
import { ProductCard } from "../ProductCard";
import { CardContext } from "../../context/cardContext";

const ProductsCon = ({ products, loading }) => {
  const logo = "/images/no image.png";

  const {
    items,
    addToCard,
    increaseCardBalance,
    decreaseCardBalance,
  } = useContext(CardContext);

  return (
    <Content>
      {!loading && products && products.length != 0 ? (
        products.map((item, index) => (
          <ProductsContainer key={index}>
            <ProductCard
              items={items}
              addToCard={() => addToCard(item)}
              increaseCardBalance={() => increaseCardBalance(item)}
              decreaseCardBalance={() => decreaseCardBalance(item)}
              config={config}
              index={item.id}
              image={item.images && item.images.thumb}
              data={item}
              logo={logo}
              status={item.status}
              number={item.number}
            />
          </ProductsContainer>
        ))
      ) : loading ? (
        <Center>
          <LinearProgress />
        </Center>
      ) : (
        <NotFound>نتیجه ای یافت نشد</NotFound>
      )}
    </Content>
  );
};

export default ProductsCon;

const NotFound = styled.div`
  color: black;
  text-align: center;
`;

const Content = styled.div`
  max-width: 1200px;
  margin: auto;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  padding-bottom: 20px;
`;

const ProductsContainer = styled.div`
  & > div {
    margin: 5px;
  }
`;

const Center = styled.div`
  height: calc(100vh - 110px);
  display: flex;
  justify-content: center;
  align-items: center;
`;
