import { Map, TileLayer, Marker, Popup } from "react-leaflet-universal";
import "react-leaflet-universal/dist/decorator";
import styled from "styled-components";
import { useState } from "react";

export function LeafletMap() {
  return (
    <MapWrapper>
      <Map
        style={{
          width: "100%",
          height: "100%",
        }}
        className="markercluster-map"
        center={[32.3831, 48.4236]}
        zoom={16}
        zoomControl={true}
        maxZoom={18}
        animate={true}
        easeLinearity={0.35}
      >
        {() => {
          return (
            <>
              <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              />
              <Marker position={[32.3831, 48.4236]}></Marker>
            </>
          );
        }}
      </Map>
    </MapWrapper>
  );
}

const MapWrapper = styled.div`
  width: 100vw;
  height: 150px;
  overflow: hidden;
  margin-bottom: 15px;
  .leaflet-marker-icon {
    background: blue !important;
  }
`;
