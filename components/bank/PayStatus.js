import React, { useState, useEffect, useCallback } from "react";
import styled, { css } from "styled-components";
import { MultiplyIcon, TickIcon } from "../icons/Icons";

export const PayStatus = ({ payStatus, setPage }) => {
  return (
    <Wrapper>
      {payStatus == "unsuccess" && (
        <>
          <LogoCon color={payStatus}>
            <MultiplyIcon color={"#e74c3c"} />
          </LogoCon>
          <Title>پرداخت ناموفق بود</Title>
          <Button onClick={() => setPage("order")}>بازگشت به ثبت سفارش</Button>
        </>
      )}

      {payStatus == "success" && (
        <>
          <LogoCon color={payStatus}>
            <TickIcon color={"#27ae60"} />
          </LogoCon>
          <Title>پرداخت موفقیت آمیز بود</Title>
          <Button onClick={() => setPage("order")}>لیست سفارشات</Button>
        </>
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  direction: rtl;
`;
const LogoCon = styled.div`
  width: 100px;
  height: 100px;
  border-radius: 50px;
  outline: 1px dashed
    ${(p) =>
      p.color == "success" ? p.theme.colors.green : p.theme.palette.secondary};
  outline-offset: -5px;
  -moz-outline-radius: 15px;
  overflow: hidden;
  margin: 0px auto;
  margin-bottom: 50px;
  svg {
    width: 50px;
    height: 50px;
    margin: 25px auto;
    display: block;
  }
`;
const Title = styled.h2`
  font-size: 16px;
  font-weight: 400;
  color: ${(props) => props.theme.colors.gray};
  text-align: center;
  margin-bottom: 50px;
`;

const Button = styled.div`
  width: 150px;
  text-align: center;
  display: block;
  padding: 8px 10px;
  margin: 10px auto;
  border-radius: 5px;
  background: ${(props) => props.theme.palette.primary};
  color: ${(props) => props.theme.colors.lightGray};
  cursor: pointer;
  font-size: 13px;
`;
