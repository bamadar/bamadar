import React from "react";
// import BottomNavigation from "@material-ui/core/BottomNavigation";
// import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import styled from "styled-components";

function LabelBottomNavigation({ inputs, currentState }) {
  const [value, setValue] = React.useState(currentState || 0);

  return (
    <Fixed>
      <BottomNavigation value={value} showLabels>
        {inputs &&
          inputs.map((item, index) => (
            <React.Fragment key={item.id}>
              <BottomNavigationAction id={item.id} onClick={item.onClick}>
                {
                  <UserIcon active={value == index} badge={item.badge}>
                    {item.icon}
                  </UserIcon>
                }
                <React.Fragment>{item.label}</React.Fragment>
              </BottomNavigationAction>

              <React.Fragment>{item.extra}</React.Fragment>
            </React.Fragment>
          ))}
      </BottomNavigation>
    </Fixed>
  );
}
export default React.memo(LabelBottomNavigation);

const BottomNavigation = styled.div`
  display: flex;
  grid-template-columns: repeat(4, 1fr);
  justify-content: space-between;
  flex-direction: row-reverse;
  padding: 0 15px;
  align-items: center;
  color: ${(p) => p.theme.colors.black};
  font-size: 0.8rem;
`;

const BottomNavigationAction = styled.div`
  display: grid;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;
const UserIcon = styled.div`
  width: 22px;
  position: relative;
  justify-self: center;
  svg {
    width: 100%;
    fill: ${(p) => (p.active ? p.theme.colors.darkGray : null)};
  }

  ::before {
    content:"${(p) => (p.badge ? p.badge : null)}";
    display: ${(p) => (p.badge ? "block" : "none")};
    position: absolute;
    background: ${(p) => p.theme.palette.secondary};
    color: ${(p) => p.theme.colors.white};
    padding: 0 2px;
    font-family: IRANSans !important;
    border-radius: 3px;
    font-size: 0.8rem;
    right: 0;
    bottom: 0;
    transform: translateX(50%);
  }
`;
const Fixed = styled.div`
  position: fixed;
  z-index: 2100;
  bottom: 0;
  width: 100%;
  border-top: 1px solid ${(p) => p.theme.colors.lightGray};
  padding-bottom: 15px;
  padding-top: 5px;
  background: white;
  height: 60px;

  @media only screen and (min-width: 768px) {
    display: none;
  }
  @media ${(p) => p.theme.media["above-1000"]} {
    width: ${(p) => p.theme.width.maxWidth};
    border-right: 1px solid ${(p) => p.theme.colors.lightGray};
    border-left: 1px solid ${(p) => p.theme.colors.lightGray};
  }
`;
