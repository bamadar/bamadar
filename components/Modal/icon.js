import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import { HandleNo } from "./handle-no";

const Icon = ({ handleOpen, icon }) => {
  return (
    <>
      {icon ? (
        <div type="button" onClick={handleOpen}>
          {icon}
        </div>
      ) : (
        <button type="button" onClick={handleOpen}>
          Open Modal
        </button>
      )}
    </>
  );
};

export { Icon };
