import Button from "@material-ui/core/Button";

const HandleYes = ({ yesBtn, yesTxt, handleClose, handleYes }) => {
  const handleClose_yes = () => {
    handleYes ? handleYes() : null;
    handleClose();
  };

  return (
    <>
      {yesBtn === undefined && yesBtn !== false && (
        <Button
          variant="outlined"
          color="primary"
          onClick={handleClose_yes}
          style={{ marginRight: "15px" }}
        >
          {yesTxt ? yesTxt : "بله"}
        </Button>
      )}
    </>
  );
};

export { HandleYes };
