import Button from "@material-ui/core/Button";

const HandleNo = ({ handleClose, handleNo, noBtn, noTxt }) => {
  const handleClose_no = () => {
    handleNo ? handleNo() : null;
    handleClose();
  };
  return (
    <>
      {noBtn === undefined && noBtn !== false && (
        <Button color="primary" onClick={handleClose_no}>
          {noTxt ? noTxt : "خیر"}
        </Button>
      )}
    </>
  );
};

export { HandleNo };
