import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import styled from "styled-components";
import { HandleNo } from "./handle-no";
import { Icon } from "./icon";
import { HandleYes } from "./handle-yes";

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    minWidth: 350,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    color: "black",
    top: "50%",
    left: "50%",
    transform: "translateY(-50%) translateX(-50%)",
    textAlign: "center",
    marginTop: "10px",
    fontSize: "0.8rem",
    lineHeight: "3rem",
    borderRadius: "10px",
    outline: "none",
    "& *": {
      fontFamily: "IRANSans !important",
    },
  },
}));

export default function SimpleModal({
  icon,
  handleYes,
  handleNo,
  title,
  initState,
  yesTxt,
  noTxt,
  noBtn,
  yesBtn,
}) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(initState ? initState : false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClose_no = () => {
    handleNo ? handleNo() : null;
    handleClose();
  };
  return (
    <div>
      <Icon handleOpen={handleOpen} icon={icon} />

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        onClick={handleClose_no}
        style={{ zIndex: 2500 }}
      >
        {
          <div className={classes.paper}>
            <Message id="simple-modal-title">
              {title ? title : "Are you Sure?"}
            </Message>
            <HandleYes
              yesBtn={yesBtn}
              yesTxt={yesTxt}
              handleClose={handleClose}
              handleYes={handleYes}
            />
            <HandleNo
              handleClose={handleClose}
              handleNo={handleNo}
              noBtn={noBtn}
              noTxt={noTxt}
            />
          </div>
        }
      </Modal>
    </div>
  );
}

export { SimpleModal };

const Message = styled.p`
  font-size: 1rem;
`;
