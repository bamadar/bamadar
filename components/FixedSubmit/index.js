import React from "react";
import styled from "styled-components";
import CircularProgress from "@material-ui/core/CircularProgress";

const FixedSubmit = ({
  onClick,
  buttonTxt,
  subTitle,
  mainSubject,
  loading,
  disable,
}) => {
  return (
    <FixDown>
      <SubmitCart
        disable={disable}
        onClick={onClick}
        fullWidth={!subTitle && !mainSubject}
      >
        {loading ? (
          <CircularProgress style={{ width: 25, height: 25 }} />
        ) : (
          buttonTxt
        )}
      </SubmitCart>
      <Sum>
        <SumTitle>{subTitle} </SumTitle>
        {mainSubject}
      </Sum>
    </FixDown>
  );
};
export default FixedSubmit;
export { FixedSubmit };
const SumTitle = styled.p`
  font-size: 0.8rem;
`;

const Sum = styled.div`
  line-height: 1.9rem;
  text-align: center;
`;

export const SubmitCart = styled.span`
  background: ${({ theme, disable }) => !disable && theme.palette.secondary};
  color: ${({ theme, disable }) =>
    disable ? theme.colors.gray : theme.colors.white};
  padding: 10px 20px;
  border: 1px solid ${({ theme, disable }) => disable && theme.palette.gray};
  border-radius: 5px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${({ fullWidth }) => (fullWidth ? "100%" : "auto")};
  transition: 150ms;

  svg,
  path {
    color: white;
    fill: white;
  }

  @media only screen and (min-width: 768px) {
    color: ${({ theme }) => theme.palette.secondary};
    background: #fff;
  }
`;

const FixDown = styled.a`
  width: 100%;
  height: 60px;
  text-decoration: none;
  direction: rtl;
  z-index: 15;

  position: fixed;
  bottom: 60px;
  right: 0;
  @media only screen and (min-width: 768px) {
    bottom: 0;
    box-shadow: none;
    border: 1px solid ${(p) => p.theme.colors.lightGray};
    color: white;
    background: ${({ theme }) => theme.palette.secondary};
  }

  padding: 5px 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-shadow: 0 0 5px ${(props) => props.theme.colors.gray};
  transition: all 200ms;
  background: ${(props) => props.theme.colors.white};
`;
