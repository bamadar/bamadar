import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

import styled from "styled-components";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    "aria-controls": `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: 0,
    width: "100%",
    backgroundColor: "white",
    "& > header": {
      boxShadow:
        "0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14) !important",
    },
    MuiBox: {
      root: {
        padding: 0,
      },
    },
  },
}));

export default function ScrollableTabsButtonForce({
  tabs,
  tabPanel,
  iniTab,
  ...props
}) {
  const classes = useStyles();
  const [value, setValue] = React.useState(iniTab ? iniTab : 0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Con className={classes.root}>
      <AppBar position="static" color="white">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          aria-label="scrollable force tabs example"
          {...props}
        >
          {tabs.map((item, index) => (
            <Tab
              label={item}
              {...a11yProps(index)}
              style={{ padding: "5px" }}
            />
          ))}
        </Tabs>
      </AppBar>
      {tabPanel &&
        tabPanel.map((item, index) => (
          <TabPanel index={index} value={value}>
            {item}
          </TabPanel>
        ))}
    </Con>
  );
}

const Con = styled.div`
  div.MuiBox-root {
    padding: 0;
  }
`;

export { ScrollableTabsButtonForce as ScrollableTabs };
