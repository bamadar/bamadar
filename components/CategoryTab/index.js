import React from "react";
import styled, { css } from "styled-components";
import Router, { useRouter } from "next/router";
import { SmallBasketIcon } from "../icons/Icons";

export const CategoryTab = () => ({ data, config, ...props }) => {
  return (
    <Item {...props}>
      {data.image ? (
        <ImgCon>
          <Img src={config + data.image} alt={data.name} />
        </ImgCon>
      ) : (
        <SmallBasketIcon />
      )}
      <Name>{data.name}</Name>
    </Item>
  );
};

const ImgCon = styled.div`
  height: 67px;
  width: 67px;
  background: #fff1ff;
  border-radius: 6px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Img = styled.img`
  width: 50px;
  height: 50px;
`;

const Name = styled.span`
  display: block;
  font-size: 10px;
  text-align: center;
  padding: 5px 0;
`;

const Item = styled.div`
  width: 67px;
  height: 100px;
  svg {
    display: block;
    width: 100%;
    height: 50px;
    margin: 5px auto;
    path {
      text-align: center;
      fill: ${(props) => props.theme.colors.purple};
    }
  }
  :hover {
    background: ${(props) => props.theme.colors.lightCream};
  }
`;
