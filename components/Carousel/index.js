import styled from "styled-components";

export const Carousel = ({ children, ...props }) => {
  return <CarouselCon {...props}>{children}</CarouselCon>;
};

const CarouselCon = styled.div`
  display: flex;
  max-width: 1000px;
  direction: rtl;
  overflow-x: scroll;
  overflow-y: hidden;
  padding: 5px;

  -ms-overflow-style: none;
  scrollbar-width: none;
  ::-webkit-scrollbar {
    display: none;
  }

  & > div {
    margin-right: 15px;
    transform: translateX(20px);
  }
`;
