import styled from "styled-components";
import uniqid from "uniqid";
import Slider from "react-slick";
import Router, { useRouter } from "next/router";
import { pathToSchema } from "../../utils/main";
import Skeleton from "@material-ui/lab/Skeleton";

const settings = {
  showArrows: false,
  pagination: false,
  rtl: true,
  focusOnSelect: false,
  autoplay: true,
};

export const MySlider = ({ images }) => {
  const items =
    images && images.length > 0 ? (
      images.map((item) => {
        const destPath = item?.link ? item.link : Router?.asPath,
          pathSchema = pathToSchema(destPath);
        return (
          <ItemCon
            key={uniqid()}
            onClick={() => Router?.push(pathSchema, destPath)}
          >
            {item?.image ? (
              <Item src={item?.image} />
            ) : (
              <Skeleton variant="rect" width="100vw" height={250} />
            )}
          </ItemCon>
        );
      })
    ) : (
      <Skeleton variant="rect" width="100vw" height={250} />
    );

  return <Slider {...settings}>{items}</Slider>;
};

const responsive = {
  desktop: {
    breakpoint: {
      max: 3000,
      min: 1024,
    },
    items: 1,
  },
  mobile: {
    breakpoint: {
      max: 464,
      min: 0,
    },
    items: 1,
  },
  tablet: {
    breakpoint: {
      max: 1024,
      min: 464,
    },
    items: 1,
  },
};

const ItemCon = styled.div`
  width: 100%;
  height: auto;
`;
const Item = styled.img`
  width: 100%;
  height: 100%;
`;
