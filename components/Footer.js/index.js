import styled from "styled-components";
import InstagramIcon from "@material-ui/icons/Instagram";
import WhatsAppIcon from "@material-ui/icons/WhatsApp";
import { Grid } from "@material-ui/core";

const Banner = () => (
  <Social>
    <SocialWrapper>
      <SocialNetworks>
        <Subtitle>ما را در شبکه های اجتماعی دنبال کنید:</Subtitle>
        <SocialLinks>
          {/* <Icon>
            <Link href="">
              <WhatsAppIcon />
            </Link>
          </Icon> */}
          <Icon>
            <Link href="https://www.instagram.com/bamadarcom/" target="_blank">
              <InstagramIcon />
            </Link>
          </Icon>
        </SocialLinks>
      </SocialNetworks>
    </SocialWrapper>
  </Social>
);

const Accs = () => (
  <Grid container spacing={3} style={{ width: "99vw", padding: "10px 20px" }}>
    <BottomAccs item sm={12} md={6}>
      <div>
        <Title>آدرس</Title>
        <p>دزفول، بلوار پیام آوران، تقاطع آفرینش، شهر تفریحی مادر</p>
      </div>
      <Vector src="/images/vectors/map.svg" />
    </BottomAccs>
    <BottomAccs item sm={12} md={6}>
      <div>
        <Title>پشتیبانی در ساعات کاری</Title>
        <p>بامادر همه روزه از ساعت 8صبح تا 11:30شب در کنار شماست</p>
        <p>شماره تماس: 42544444</p>
      </div>
      <Vector src="/images/vectors/support.svg" />
    </BottomAccs>
  </Grid>
);

export const Footer = () => {
  return (
    <Container>
      <Banner />
      <Accs />
    </Container>
  );
};

const Container = styled.div`
  @media only screen and (max-width: 767px) {
    display: none;
  }
`;

const Vector = styled.img`
  width: 3rem;
  display: block;
  margin-left: 24px;
  width: 100%;
  height: 100%;
  max-width: 3rem;
`;

const BottomAccs = styled(Grid)`
  display: flex;
  align-items: center;
  text-align: right;
  justify-content: flex-end;
  line-height: 2rem;
`;

const Title = styled.strong`
  color: #0e3368;
`;

const Social = styled.div`
  background-color: #0e3368;
  width: 100%;
  direction: rtl;
  color: white;
`;

const SocialWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  margin-right: auto;
  margin-left: auto;
  width: 100%;
  max-width: 77.5rem;
`;

const SocialNetworks = styled.div`
  display: flex;
  padding: 1.5rem;
  flex-wrap: wrap;
  align-items: center;
  direction: rtl;
`;

const Subtitle = styled.strong`
  margin: 0.5em;
  margin-right: 0;
  color: inherit;
  font-weight: 300;
`;

const SocialLinks = styled.ul`
  display: flex;
  margin: 0;
  padding: 0;
  list-style-position: outside;
  list-style-image: none;
`;

const Icon = styled.li`
  display: block;
  margin: 0;
  margin-top: 10px;
  padding: 0 5px;
  flex-basis: auto;
`;

const Link = styled.a`
  text-decoration: none;
  color: white;
`;
