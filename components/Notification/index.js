import { store } from "react-notifications-component";

export const pushNotification = (title, message, type, pos) => {
  store.addNotification({
    title: title,
    message: message,
    type: type ? type : "default",
    insert: "top",
    container: pos ? pos : "top-right",
    animationIn: ["animated", "fadeIn"],
    animationOut: ["animated", "fadeOut"],
    dismiss: {
      duration: 5000,
      onScreen: true,
    },
  });
};
