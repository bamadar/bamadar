import React, { Component } from "react";
import styled from "styled-components";
import uniqid from "uniqid";

class Textbox extends Component {
  id = uniqid();
  state = {
    labelClass: "email_label",
  };

  textFillness = (event) => {
    const text = event.target.value,
      size = text ? text.length : null;

    if (this.props.changed) this.props.changed(event.target.value);
    if (!size) {
      this.setState({ labelClass: "email_label" });
    } else {
      this.setState({ labelClass: "email_label_inputFilled" });
    }
  };

  componentDidMount() {
    if (this.props.defaultValue || this.props.placeholder) {
      const event = { target: { value: this.props.defaultValue } };
      this.textFillness(event);
    }
  }

  textboxJSX = () => {
    return (
      <Wrapper {...this.props}>
        <div className={"controls"}>
          <input
            className={"txtInput"}
            type={this.props.type}
            id={this.id}
            onChange={this.textFillness}
            defaultValue={this.props.defaultValue}
            placeholder={this.props.placeholder}
            disabled={this.props.disabled ? "disabled" : null}
          />
          <label htmlFor={this.id} className={this.state.labelClass}>
            {this.props.label}
          </label>
        </div>
      </Wrapper>
    );
  };

  render() {
    return this.textboxJSX();
  }
}

export default Textbox;

const Wrapper = styled.div`
  input {
    text-align: right;
    font-family: "IRANSans";
  }

  label {
    position: absolute;
    top: 0;
    background: white;
    right: 5%;
    transition: 200ms;
    font-size: 0.8rem;
  }

  .controls {
    width: 100%;
    position: relative;
    .email_label {
      transform: translateY(25%);
      transition: 200ms;
      color: gray;
      padding-right: 5px;
      right: 5%;
    }
  }

  .txtInput {
    padding: 10px 15px;
    box-sizing: border-box;
    transition: 200ms;
    width: 100%;
    display: block;
    border: 1px solid ${(props) => props.theme.colors.gray};
    margin-top: 10px + 5px;
    border-radius: 5px;

    &:focus,
    &:hover {
      outline: none;
      border-color: ${(props) => props.theme.colors.navyBlue};
    }

    &:focus + .email_label {
      transform: translateY(-10px) translateX(0px);
      font-size: 0.8rem;
      color: ${(props) => props.theme.colors.navyBlue};
      font-weight: bold;
      padding: 0 5px;
    }
  }

  .email_label_inputFilled {
    transform: translateY(-10px) translateX(0px);
    font-size: 0.8rem;
    color: ${(props) => props.theme.colors.navyBlue};
    font-weight: bold;
    padding: 0 5px;
  }
`;
