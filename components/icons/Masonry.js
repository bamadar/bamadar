import * as React from "react";

export default (props) => {
  return (
    <svg
      width="24"
      height="25"
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="2.5"
        y="3"
        width="8"
        height="6"
        rx="2"
        stroke="#262F56"
        strokeWidth="1.8"
      />
      <rect
        x="13.5"
        y="3"
        width="8"
        height="10"
        rx="2"
        stroke="#262F56"
        strokeWidth="1.8"
      />
      <rect
        x="2.5"
        y="12"
        width="8"
        height="10"
        rx="2"
        stroke="#262F56"
        strokeWidth="1.8"
      />
      <rect
        x="13.5"
        y="16"
        width="8"
        height="6"
        rx="2"
        stroke="#262F56"
        strokeWidth="1.8"
      />
    </svg>
  );
};
