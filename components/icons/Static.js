import * as React from "react";

export const Static = (props) => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
    >
      <path
        d="M3 11L8.5 5.5L14 11L23 2M23 2H18M23 2V7M3 18V21M8.5 21V15M14 21V18M19.5 21V15"
        stroke="#262F56"
        strokeWidth="1.8"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <circle cx="14" cy="11" r="2.5" fill="#262F56" />
      <circle cx="8.5" cy="5.5" r="2.5" fill="#262F56" />
      <circle cx="3" cy="11" r="2.5" fill="#262F56" />
    </svg>
  );
};
