import React from "react";
import styled, { css } from "styled-components";
import RemoveOutlinedIcon from "@material-ui/icons/RemoveOutlined";
import DeleteOutlineOutlinedIcon from "@material-ui/icons/DeleteOutlineOutlined";
import Modal from "../Modal";

const DeleteOption = ({ balance, remove, measurement, owner }) => {
  let modal = false;

  switch (measurement) {
    case "عدد":
      modal = balance <= 1 ? true : false;
      break;
    case "گرم":
      modal = balance <= 0.1 ? true : false;
      break;
    case "کیلوگرم":
      modal = balance <= 0.1 ? true : false;
      break;
  }

  return modal ? (
    <Modal
      handleYes={remove}
      title="آیا از حذف این کالا اطمینان دارید؟"
      icon={
        <ButtonWrapper owner={owner}>
          <DeleteOutlineOutlinedIcon />
        </ButtonWrapper>
      }
    />
  ) : (
    <ButtonWrapper onClick={remove} owner={owner}>
      <RemoveOutlinedIcon />
    </ButtonWrapper>
  );
};

export default DeleteOption;

const ButtonWrapper = styled.div`
  height: 100%;
  display: flex;
  align-items: center;

  path {
    ${({ theme, owner }) => {
      if (owner == "cart")
        return css`
          fill: ${(props) => props.theme.palette.secondary};
        `;
      else if (owner == "productPage")
        return css`
          fill: #fff;
        `;
      else
        return css`
          fill: ${(props) => props.theme.palette.primary};
        `;
    }}
  }
`;
