import React from "react";
import styled, { css } from "styled-components";
import { AddToCard } from "../icons/AddToCard";
import DeleteOption from "./delete-option";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import { checkExistance } from "../../utils/main";

const AddProduct = ({ owner, balance, increase, addToCard }) => {
  const mapOwnerToComponent = () => {
    switch (owner) {
      case "productPage":
        return "افزودن به سبد";
      default:
        return <AddToCard />;
    }
  };

  return (
    <NewProduct onClick={balance ? increase : addToCard} owner={owner}>
      {mapOwnerToComponent()}
    </NewProduct>
  );
};

const ProductOptions = ({
  status,
  itemBalance,
  increase,
  addToCard,
  measurement,
  remove,
  number,
  owner,
}) => {
  const balance = itemBalance.length > 0 ? itemBalance[0].balance : 0;

  return (
    checkExistance(status, number) && (
      <Left type={balance} owner={owner}>
        {balance ? (
          <>
            <AddToCardButton
              onClick={balance ? increase : addToCard}
              owner={owner}
            >
              <AddOutlinedIcon />
            </AddToCardButton>

            <Balance measurement={measurement} balance={balance} owner={owner}>
              {measurement !== "عدد" && balance < 1 ? balance * 1000 : balance}
            </Balance>

            <DeleteOption
              balance={balance}
              remove={remove}
              measurement={measurement}
              owner={owner}
            />
          </>
        ) : (
          <AddProduct
            owner={owner}
            balance={balance}
            increase={increase}
            addToCard={addToCard}
          />
        )}
      </Left>
    )
  );
};

export default ProductOptions;

const Balance = styled.div`
  font-size: 15px;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  padding: 0 12px;
  border-radius: 5px;
  
  font-weight: bolder;

  ${({ theme, owner }) => {
    if (owner == "cart")
      return css`
        color: ${theme.palette.secondary};
      `;
    else if (owner == "productPage")
      return css`
        color: #fff;
      `;
    else
      return css`
        color: ${theme.palette.primary};
      `;
  }}

  ::after {
    content: "${({ measurement, balance }) => {
      if (balance < 1 && measurement !== "عدد") return "گرم ";
      else if (measurement === "کیلوگرم" || measurement === "گرم")
        return "کیلوگرم";
    }}"
     ;
    /* position: absolute; */
    top: calc(100% + 5px);
    font-size: 0.8rem;
  }
`;

const Left = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  direction: rtl;
  border: 1px solid
    ${(props) =>
      props.type ? props.theme.colors.gray : props.theme.palette.primary};

  background: ${(props) =>
    props.type ? props.theme.colors.white : props.theme.palette.primary};

  ${({ theme, owner }) => {
    if (owner == "cart")
      return css`
        border-radius: 5px;
        padding: 3px;
        height: auto;
      `;
    else if (owner == "productPage")
      return css`
        border-radius: 30px;
        padding: 0 5px;
        height: 30px;
        width: 135px;
        background: ${(props) => props.theme.palette.primary};
        border-color: ${(props) => props.theme.palette.primary};
      `;
    else
      return css`
        border-bottom-left-radius: 15px;
        border-top-right-radius: 10px;
        padding: 0 5px;
        height: 50px;
      `;
  }}
`;

const AddToCardButton = styled.div`
  height: 100%;
  display: flex;
  align-items: center;

  path {
    ${({ owner }) => {
      if (owner == "cart")
        return css`
          fill: ${(props) => props.theme.palette.secondary};
        `;
      else if (owner == "productPage")
        return css`
          fill: #fff;
        `;
      else
        return css`
          fill: ${(props) => props.theme.palette.primary};
        `;
    }}
  }
`;

const NewProduct = styled(AddToCardButton)`
  ${({ owner }) => {
    if (owner == "productPage")
      return css`
        color: #fff;
        align-items: baseline;
      `;
    else
      return css`
        width: 20px;
        height: 18px;
        transform: translateY(1px);
        path {
          fill: ${(props) => props.theme.colors.white};
        }
      `;
  }}
`;
