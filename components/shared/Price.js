import React from "react";
import styled, { css } from "styled-components";

export const Price = ({ children, cross, ...props }) => {
  return (
    <Wrapper cross={cross} {...props}>
      {numberWithCommas(children)} {!cross && "ریال"}
    </Wrapper>
  );
};

const numberWithCommas = (x) => {
  return parseInt(x)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

const Wrapper = styled.div`
  font-weight: 600;
  padding: 0 5px;

  ${(p) =>
    p.cross &&
    css`
      text-decoration: line-through;
      opacity: 0.5;
      font-size: 12.5px;
    `}
`;
