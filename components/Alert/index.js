import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styled from "styled-components";
import { Alert, AlertTitle } from "@material-ui/lab";

const Wrapper = styled.div`
  width: 100%;
  direction: rtl;

  span,
  div {
    font-family: "IRANSans";
  }

  .MuiAlert-icon {
    margin-left: 10px;
  }
`;

export function DescAlert({ type, title, subTitle, variant, ...props }) {
  return (
    <Wrapper>
      <Alert severity={type} variant={variant} {...props}>
        <AlertTitle>{title}</AlertTitle>
        {subTitle}
      </Alert>
    </Wrapper>
  );
}

export const SuccessAlert = ({ message, ...props }) => {
  return (
    <DescAlert
      type={"success"}
      subTitle={message}
      title={"عملیات موفقیت آمیز بود"}
      variant="filled"
      {...props}
    />
  );
};

export const ErrorAlert = ({ message, ...props }) => {
  return (
    <DescAlert
      type={"error"}
      subTitle={message}
      title={"خطا"}
      variant="filled"
      {...props}
    />
  );
};
