import styled from "styled-components";

export const Paper = ({ children, ...props }) => {
  return <Wrapper {...props}>{children}</Wrapper>;
};

const Wrapper = styled.div`
  box-shadow: 0px 1px 15px rgba(0, 0, 0, 0.08);
  margin: 15px;
  padding: 10px;
  border-radius: 5px;
`;
