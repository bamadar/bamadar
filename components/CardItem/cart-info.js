import React from "react";
import styled from "styled-components";
import { config } from "./../../context/server";
import Router from "next/router";

export const CartInfo = ({ name, finalPrice, image, id }) => {
  return (
    <Right onClick={() => Router.push("/product/[id]", `/product/${id}`)}>
      <Img src={image ? config + image : "images/no image.png"} />
      <ProductName>{name}</ProductName>
    </Right>
  );
};

const Right = styled.div`
  line-height: 2rem;
  display: flex;
  padding-bottom: 15px;
`;

const Img = styled.img`
  width: 65px;
  height: 65px;
  margin-left: 8px;
`;

const ProductName = styled.p`
  display: flex;
  align-items: center;
  font-size: 0.9rem;
  color: #3f3f3f;
`;
