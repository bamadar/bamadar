import React from "react";
import styled from "styled-components";
import ProductOptions from "./../ProductOptions";
import { CartInfo } from "./cart-info";
import { Price } from "./../shared/Price";

export const CardItem = ({
  name,
  balance,
  finalPrice,
  add,
  measurement,
  remove,
  number,
  status,
  image,
  id,
}) => {
  return (
    <ProductItem>
      <CartInfo name={name} finalPrice={finalPrice} image={image} id={id} />
      <Container>
        <ProductOptions
          status={status}
          itemBalance={balance}
          increase={add}
          addToCard={add}
          measurement={measurement}
          remove={remove}
          number={number}
          owner="cart"
        />
        <ThePrice>{finalPrice}</ThePrice>
      </Container>
    </ProductItem>
  );
};

const ThePrice = styled(Price)`
  display: flex;
  align-items: center;
  padding-right: 30px;
`;

const Container = styled.div`
  display: grid;
  grid-template-columns: 135px 1fr;
  padding: 0 15px;
  margin-top: 10px;
`;

const ProductItem = styled.div`
  width: 100%;
  border-bottom: 5px solid #efeef3;
  padding: 15px 10px;
  direction: rtl;

  @media only screen and (min-width: 768px) {
    border: .0625rem solid #e5e5e5;
    box-shadow: 0 0 0 0 hsla(0,0%,90%,.05);
    padding: .5rem 1rem 1rem;
    border-radius: .25rem;
    background: #fff;
  }

  /* :hover {
    background: ${(p) => p.theme.colors.lightGray};
  } */
`;
