import React from "react";
import styled from "styled-components";
import { Price } from "../shared/Price";

export default ({ price, finalPrice, status, number }) => {
  const ShowPrice = (price, finalPrice) => {
    return price != finalPrice ? (
      <>
        <Price cross>{price}</Price>
        <Price>{finalPrice}</Price>
      </>
    ) : (
      <Price>{finalPrice}</Price>
    );
  };

  const checkStatus = (status, price, finalPrice, number) => {
    if (!number) return <Empty>ناموجود</Empty>;

    switch (status) {
      case "available":
        return ShowPrice(price, finalPrice);
      case "soon":
        return <Soon>به زودی</Soon>;
      case "empty":
        return <Empty>ناموجود</Empty>;
    }
  };

  return <>{checkStatus(status, price, finalPrice, number)}</>;
};

const Soon = styled.span`
  color: ${(props) => props.theme.palette.primary};
`;

const Empty = styled.span`
  color: ${(props) => props.theme.palette.secondary};
`;
const Pic = styled.div`
  float: right;
  width: 80px;
  height: 80px;
  border-radius: 10px;
  img {
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 10px;
  }
`;
