import React from "react";
import styled from "styled-components";
import MyPrice from "./MyPrice";

export const ProductInfo = ({
  apiAdd,
  name,
  price,
  finalPrice,
  image,
  status,
  number,
}) => {
  return (
    <>
      <Pic>
        <img
          src={image == "empty" ? "/images/no image.png" : apiAdd + image}
          alt={name}
        />
      </Pic>
      <Right>
        <Name>{name}</Name>
        <Row>
          <MyPrice
            price={price}
            finalPrice={finalPrice}
            status={status}
            number={number}
          />
        </Row>
      </Right>
    </>
  );
};

const Pic = styled.div`
  float: right;
  width: 80px;
  height: 80px;
  border-radius: 10px;
  img {
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 10px;
  }
`;
const Right = styled.div`
  flex-grow: 1;
  padding-right: 20px;
`;

const Name = styled.div`
  padding: 0 5px;
`;

const Row = styled.div`
  margin-top: 10px;
  display: flex;
  text-align: center;

  color: ${(props) => props.theme.colors.black};
`;
