import React from "react";
import styled from "styled-components";
import ProductOptions from "../ProductOptions";
import { ProductInfo } from "./product-info";

export const Product = ({
  apiAdd,
  name,
  measurement,
  price,
  finalPrice,
  image,
  addToCard,
  remove,
  balance,
  increase,
  status,
  number,
}) => {
  return (
    <ProductItem>
      <ProductInfo
        apiAdd={apiAdd}
        name={name}
        price={price}
        finalPrice={finalPrice}
        image={image}
        status={status}
        number={number}
      />

      <ProductOptions
        status={number ? status : "empty"}
        itemBalance={balance}
        increase={increase}
        addToCard={addToCard}
        measurement={measurement}
        remove={remove}
      />
    </ProductItem>
  );
};

const ProductItem = styled.div`
  min-height: 50px;
  border-radius: 15px;

  border: 1px solid ${(props) => props.theme.colors.lightGray};
  box-shadow: 0px 0px 10px ${(props) => props.theme.colors.lightGray};
  margin: 10px 8px;
  padding: 10px 8px;
  border-radius: 15px;

  display: grid;
  grid-template-columns: 1fr 10fr 1fr;
  direction: rtl;
  justify-content: space-between;
  align-items: center;
  color: ${(props) => props.theme.colors.black};

  cursor: pointer;
  :hover {
    background: #eee;
  }
`;
