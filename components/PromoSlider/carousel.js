import React from "react";
// import Carousel from "react-elastic-carousel";
import uniqid from "uniqid";
import styled from "styled-components";
import { ProductCard } from "../ProductCard";
import ElasticCarousel from "react-slick";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";

const NO_LOCAL_STORAGE = typeof window === "undefined" || !localStorage;

const Button = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;

  svg {
    color: gray;
  }
`;

var ksettings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 6,
  slidesToScroll: 1,
  initialSlide: 5,
  rtl: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

const NextArrow = ({ style, onClick, className }) => {
  return (
    <ButtonCon
      onClick={onClick}
      className={className}
      style={{ ...style, color: "#3e3e3e" }}
    >
      <ArrowForwardIosIcon />
    </ButtonCon>
  );
};

const ButtonCon = styled.div`
  ::before {
    display: none;
  }
`;

const PrevArrow = ({ style, onClick, className }) => {
  return (
    <ButtonCon
      onClick={onClick}
      className={className}
      style={{ ...style, color: "#3e3e3e" }}
    >
      <ArrowBackIosIcon />
    </ButtonCon>
  );
};
const DesktopCarousel = ({ children, responsive }) => {
  return (
    children &&
    children.length > 2 && (
      <ElasticCarousel
        {...ksettings}
        {...responsive}
        nextArrow={<NextArrow />}
        prevArrow={<PrevArrow />}
      >
        {children}
      </ElasticCarousel>
    )
  );
};

export const PromoCarousel = ({
  proPros,
  items,
  addToCard,
  increaseCardBalance,
  decreaseCardBalance,
  config,
  logo,
  responsive,
  iniItem,
  lastItem,
}) => {
  const carouselItems = proPros
    ? proPros.map((data, index) => (
        <ProductCard
          key={uniqid()}
          items={items}
          addToCard={addToCard}
          increaseCardBalance={increaseCardBalance}
          decreaseCardBalance={decreaseCardBalance}
          config={config}
          index={index}
          image={data && data.images && data.images.thumb}
          data={data}
          logo={logo}
          status={data.status}
          number={data.number}
        />
      ))
    : Array(16)
        .fill({})
        .map((data, index) => (
          <ProductCard
            key={uniqid()}
            items={items}
            addToCard={addToCard}
            increaseCardBalance={increaseCardBalance}
            decreaseCardBalance={decreaseCardBalance}
            config={config}
            index={index}
            image={data && data.images && data.images.thumb}
            data={data}
            logo={logo}
            status={data.status}
            number={data.number}
          />
        ));

  iniItem &&
    carouselItems &&
    carouselItems.unshift(
      <PromoImage>
        <img
          src={iniItem}
          alt="پیشنهاد ویژه"
          style={{ width: 160, height: 250 }}
        />
      </PromoImage>
    );

  lastItem && carouselItems.push(lastItem);

  carouselItems &&
    !NO_LOCAL_STORAGE &&
    window.innerWidth < 667 &&
    carouselItems.push(
      <FreeSpace>
        <FreeSpace />
      </FreeSpace>
    );

  return !NO_LOCAL_STORAGE && window.innerWidth >= 667 ? (
    <DesktopCarousel responsive={responsive}>{carouselItems}</DesktopCarousel>
  ) : (
    <Carousel>{carouselItems}</Carousel>
  );
};

const PromoImage = styled.div`
  img {
    width: 160px;
    height: 250px;
  }
`;

const FreeSpace = styled.div`
  width: 10px;
`;

const Carousel = styled.div`
  display: flex;
  max-width: 1000px;
  direction: rtl;
  overflow-x: scroll;
  padding-left: 15px;
  padding: 5px;

  -ms-overflow-style: none;
  scrollbar-width: none;
  ::-webkit-scrollbar {
    display: none;
  }

  & > div {
    margin-right: 15px;
    transform: translateX(20px);
  }
`;
