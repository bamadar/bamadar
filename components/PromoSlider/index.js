import React from "react";
import styled from "styled-components";
import { PromoCarousel as Carousel } from "./carousel";
import CircularProgress from "./../Loading";

export const PromoSlider = ({
  proPros,
  items,
  addToCard,
  increaseCardBalance,
  decreaseCardBalance,
  config,
  logo,
  iniItem,
  responsive,
  lastItem,
}) => {
  return (
    <Container>
      {
        <Carousel
          proPros={proPros}
          items={items}
          addToCard={addToCard}
          increaseCardBalance={increaseCardBalance}
          decreaseCardBalance={decreaseCardBalance}
          config={config}
          logo={logo}
          iniItem={iniItem}
          lastItem={lastItem}
          responsive={responsive}
        />
      }
    </Container>
  );
};

const Loading = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Container = styled.div`
  position: relative;

  .rec-carousel-wrapper {
    flex-direction: row !important;
    overflow-x: scroll;

    @media only screen and (min-width: 767px) {
      overflow-x: auto !important;
    }

    .rec-carousel {
      /* width: 160px !important; */
      height: 250px !important;
    }
    & > div {
      margin-right: 15px;
    }
  }
  /* li {
    @media only screen and (max-width: 600px) {
      margin: 25px;
      margin-top: 0;
    }
  } */
`;
