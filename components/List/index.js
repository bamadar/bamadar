import MaterialList from "@material-ui/core/List";
import React from 'react';
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import uniqid from "uniqid";

export const List = ({ items, style }) => {
  return (
    <>
      <Divider />
      {items &&
        items.map((item) => {
          return item.text ? (
            <React.Fragment key={uniqid()}>
              <MaterialList onClick={item.action} style={style}>
                <ListItem>
                  <ListItemAvatar>{item.icon}</ListItemAvatar>
                  <ListItemText
                    primary={item.text}
                    style={{ textAlign: "right" }}
                  />
                  <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="delete">
                      {item.secIcon}
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              </MaterialList>
              <Divider />
            </React.Fragment>
          ) : (
            <></>
          );
        })}
    </>
  );
};
