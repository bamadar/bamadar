import React from "react";
import styled, { css } from "styled-components";
import { Price } from "../shared/Price";
import ProductOptions from "../ProductOptions";
import Skeleton from "@material-ui/lab/Skeleton";

import Router from "next/router";

export const ProductCard = ({
  items,
  addToCard,
  increaseCardBalance,
  decreaseCardBalance,
  config,
  index,
  image,
  data,
  logo,
  status,
  number,
}) => {
  const checkStatus = (status, price, sell_price, discount, number) => {
    const { asPath } = Router;
    if (!number)
      return asPath == "/shop" || asPath == "/" || asPath == "/promotions" ? (
        <Empty style={{ direction: "rtl", display: "block" }}>تمام شد!</Empty>
      ) : (
        <Empty>ناموجود</Empty>
      );

    switch (status) {
      case "available":
        return (
          <PriceCon>
            <SellAndDiscCon>
              <SellPrice>{sell_price}</SellPrice>
              {discount > 0 && <Discount>{discount}%</Discount>}
            </SellAndDiscCon>
            {sell_price != price && <Price cross>{price}</Price>}
          </PriceCon>
        );
      case "soon":
        return <Soon>به زودی</Soon>;
      case "empty":
        return <Empty>ناموجود</Empty>;
    }
  };

  return (
    <ItemCon key={index} disabled={!number || status != "available"}>
      <AddToCart>
        {
          <ProductOptions
            status={data.status}
            itemBalance={items.filter((thisItem) => thisItem.id === data.id)}
            increase={() => increaseCardBalance(data)}
            addToCard={() => addToCard(data)}
            remove={() => decreaseCardBalance(data)}
            measurement={data.measurement}
            number={number}
          />
        }
      </AddToCart>

      <div onClick={() => Router.push("/product/[id]", `/product/${data.id}`)}>
        <ImgAndTxtCon>
          <ImgCon>
            {data.name ? (
              <Item
                src={image ? config + image : "/images/no image.png"}
                alt={data.name}
              />
            ) : (
              <Skeleton variant="rect" width={145} height={124} />
            )}
          </ImgCon>
          <ItemText>
            {data.name ? (
              data.name
            ) : (
              <Skeleton variant="rect" width={145} height={15} />
            )}
          </ItemText>
        </ImgAndTxtCon>

        {data.name ? (
          checkStatus(
            status,
            data.price,
            data.sell_price,
            data.discount,
            number
          )
        ) : (
          <Skeleton variant="rect" width={35} height={25} />
        )}
      </div>
    </ItemCon>
  );
};

const Soon = styled.span`
  color: ${(props) => props.theme.palette.primary};
`;

const Empty = styled.span`
  color: ${(props) => props.theme.palette.secondary};
`;

const ImgAndTxtCon = styled.div`
  width: 100%;
`;

const SellAndDiscCon = styled.div`
  display: flex;
  direction: rtl;
  justify-content: space-between;
`;

const AddToCart = styled.div`
  position: absolute;
  right: -1px;
  top: -1px;
  z-index: 15;
`;

const ItemCon = styled.div`
  border-radius: 5px;
  position: relative;

  background: white;

  text-align: right;
  display: flex;
  flex-direction: column;
  align-items: center;

  border: 1px solid #f6f6f6;
  border-radius: 9px;
  padding: 8px;
  position: relative;
  box-shadow: -5px 10px 20px rgba(0, 0, 0, 0.05);
  background-color: #fff;
  width: 165px;
  min-height: 250px;

  border: 1px solid #dadada;

  cursor: pointer;

  ::before {
    content: " ";
    position: absolute;
    width: 100%;
    height: calc(100% + 5px);
    background: rgba(255, 255, 255, 0.5);
    z-index: 10;
    transform: translateY(-10px);
    display: ${({ disabled }) => (disabled ? "block" : "none")};
    border-radius: 9px;
    pointer-events: none;
  }
`;
const ImgCon = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 124px;
  margin-bottom: 10px;
  z-index: 0;
  position: relative;
  width: 145px;
`;

const Item = styled.img`
  width: 120px;
  height: 100%;
  pointer-events: none;
`;
const ItemText = styled.p`
  font-size: 0.8rem;
  line-height: 22px;
  height: 42px;
  color: #2a2a2a;
  overflow: hidden;
  margin-bottom: 5px;
  text-overflow: ellipsis;
  display: grid;
  align-items: center;
`;

const Discount = styled.div`
  width: 45px;
  height: 22px;
  left: 273px;
  top: 1147px;
  background: ${({ theme }) => theme.palette.secondary};
  border-radius: 8px;
  text-align: center;
  color: #fff;
`;

const PriceCon = styled.div`
  display: grid;
  font-size: 0.857rem;
  line-height: 1.833;
  font-weight: 300;
  flex-wrap: wrap;
  color: #424242;
  width: 100%;
`;

const SellPrice = styled(Price)``;
