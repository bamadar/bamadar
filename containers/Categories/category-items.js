import React from "react";
import styled from "styled-components";
import ShoppingBasketOutlinedIcon from "@material-ui/icons/ShoppingBasketOutlined";
import Router from "next/router";
import Paper from "@material-ui/core/Paper";

export const CategoryItem = ({ apiAdd, index, image, name, subCatApi, id }) => {
  return (
    <Item
      elevation={0}
      key={index}
      onClick={() => Router.push("/category/[id]", subCatApi + id)}
    >
      {image == null ? (
        <ShoppingBasketOutlinedIcon />
      ) : (
        <img src={apiAdd + image} alt={name} />
      )}
      <span>{name}</span>
    </Item>
  );
};

const Item = styled.div`
  cursor: pointer;
  margin: 5px;

  display: grid;
  grid-template-columns: 1fr 1fr;
  justify-content: center;
  align-items: center;
  text-align: center;
  padding: 5px 10px;

  border-radius: 10px;
  border: 1px solid ${(props) => props.theme.colors.lightGray};
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.25) !important;

  transition: 300ms;
  box-shadow: 0 0 10px ${(props) => props.theme.colors.lightGray};

  svg {
    display: block;
    width: 50px;
    height: 50px;
    margin: 5px auto;
  }
  img {
    display: block;
    width: 100%;
    margin: 2px auto;
  }
`;
