import React from "react";
import styled from "styled-components";
import LinearProgress from "../../components/Loading";
import { CategoryItem } from "./category-items";
import uniqid from "uniqid";

export const Categories = ({ apiAdd, cats, subCatApi }) => {
  const categoryItems = () =>
    cats.map((data, index) => (
      <CategoryItem
        key={uniqid()}
        apiAdd={apiAdd}
        index={index}
        image={data.image}
        name={data.name}
        subCatApi={subCatApi}
        id={data.id}
      />
    ));

  return (
    <Wrapper>
      {cats && cats.length != 0 ? (
        <CatsCon>{categoryItems()} </CatsCon>
      ) : (
        <LinearProgress />
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  direction: rtl;
`;
const CatsCon = styled.div`
  width: 100%;
  height: auto;
  overflow: hidden;
  display: grid;
  justify-content: center;
  grid-template-columns: 1fr 1fr;
  @media only screen and (min-width: 600px) {
    grid-template-columns: repeat(5, 1fr);
  }

  transition: all 500ms;
`;
