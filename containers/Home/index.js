import { useContext, useState } from "react";
import styled, { css } from "styled-components";
import { Slider } from "./slider";
import { PromoSlider, TopSells, TopProducts } from "./sliders";
import { Category } from "./categories";
import { Pwa } from "./Pwa";
import { DeviceContext } from "../../context/DeviceContext";
import Container from "@material-ui/core/Container";
import { Banner } from "./banner";
import Router from "next/router";

const ConditionalContainer = ({ children }) => {
  const { isMobile } = useContext(DeviceContext);

  return isMobile ? children : <Container>{children}</Container>;
};

export const Home = ({ config }) => {
  const { isMobile } = useContext(DeviceContext);
  const [deleteWrapper, setdeleteWrapper] = useState(true);

  return (
    <Wrapper deleteWrapper={deleteWrapper}>
      <Slider />
      {isMobile && <Pwa />}
      <PromoSlider config={config} />
      <ConditionalContainer>
        <Category />
        <TopProducts config={config} />
        <TopSells config={config} />
      </ConditionalContainer>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  margin: auto;
`;
