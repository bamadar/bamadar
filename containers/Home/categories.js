import React from "react";
import styled from "styled-components";
import { Categories } from "../Categories";
import useSWR from "swr";
import { subCatApi, config } from "../../context/server";
import { CategoryTab } from "../SubCategories/Item";
import { Carousel } from "./../../components/Carousel";
import ListIcon from "@material-ui/icons/List";
import Router from "next/router";

export const Category = () => {
  const { data } = useSWR("all_categories");

  const cats = data;

  const slice1 = cats ? cats.slice(0, cats.length / 2) : Array(10).fill({});
  const slice2 = cats
    ? cats.slice(cats.length / 2, cats.length)
    : Array(10).fill({});

  const onClickListner = (id) => {
    Router.push("/category?catid=" + id);
  };

  return (
    <CatCon>
      <CatTitle onClick={() => Router.push("/category")}>
        دسته بندی‌ها
        <ListIcon />
      </CatTitle>
      <Carousel style={{ marginLeft: "5px", paddingRight: "20px" }}>
        {slice1.map((data, index) => (
          <CategoriesCon
            data={data}
            config={config}
            onClick={() => onClickListner(data.id)}
          />
        ))}
      </Carousel>
      <Carousel style={{ marginLeft: "5px", paddingRight: "20px" }}>
        {slice2.map((data, index) => (
          <CategorySecTab
            data={data}
            config={config}
            onClick={() => onClickListner(data.id)}
          />
        ))}
      </Carousel>
    </CatCon>
  );
};

const CategoriesCon = styled(CategoryTab)`
  & > div {
    background: rgb(236, 237, 239) none repeat scroll 0% 0%;
    width: 100px;
    height: 100px;
  }

  & > span {
    width: 100px;
    height: 52px;
  }
`;

const CategorySecTab = styled(CategoriesCon)`
  margin-top: 20;
`;

const CatCon = styled.div`
  width: 100%;
  height: auto;

  & > div {
    padding: 0;
  }

  @media only screen and (min-width: 768px) {
    display: none;
    padding: 10px;
  }
`;
const CatTitle = styled.p`
  display: flex;
  align-items: center;
  padding: 10px 5px;
  justify-content: flex-end;
  font-size: 1.2rem;

  svg {
    margin-left: 10px;
  }
`;
