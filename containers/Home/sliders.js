import React, { useContext } from "react";
import { PromoSlider as Prmsldr } from "../../components/PromoSlider";
import styled from "styled-components";
import useSWR from "swr";
import { CardContext } from "../../context/cardContext";
const logo = "../images/logos/logo96.png";
import LoyaltyOutlinedIcon from "@material-ui/icons/LoyaltyOutlined";
import StoreOutlinedIcon from "@material-ui/icons/StoreOutlined";
import ArrowBackOutlinedIcon from "@material-ui/icons/ArrowBackOutlined";
import Router from "next/router";
import Container from "@material-ui/core/Container";

const promoResponsive = [
  { width: 1, itemsToShow: 2 },
  { width: 500, itemsToShow: 3, itemsToScroll: 2 },
  { width: 850, itemsToShow: 5 },
  { width: 1000, itemsToShow: 5 },
  { width: 1200, itemsToShow: 6 },
  { width: 1400, itemsToShow: 7 },
  { width: 1500, itemsToShow: 8 },
];

export const PromoSlider = ({ config }) => {
  const {
    items,
    addToCard,
    increaseCardBalance,
    decreaseCardBalance,
  } = useContext(CardContext);

  const extractPromotions = (products) => {
    if (!products) return [];

    const extracted = products.slice(0, 6);
    return extracted;
  };

  const { data } = useSWR("get_promotions");
  const proPros = extractPromotions(data?.data);

  return (
    <PromoCon id="promoSlider" className="promo" style={{ marginTop: -3 }}>
      <GContainer>
        <Prmsldr
          proPros={proPros}
          items={items}
          addToCard={addToCard}
          increaseCardBalance={increaseCardBalance}
          decreaseCardBalance={decreaseCardBalance}
          config={config}
          logo={logo}
          iniItem="/images/proSlider/promo1.png"
          responsive={promoResponsive}
          lastItem={
            <ShowMore onClick={() => Router.push("/promotions")}>
              <div>
                <ArrowBackOutlinedIcon />
                <p>مشاهده همه</p>
              </div>
            </ShowMore>
          }
        />
      </GContainer>
    </PromoCon>
  );
};

export const TopSells = ({ config }) => {
  const {
    items,
    addToCard,
    increaseCardBalance,
    decreaseCardBalance,
  } = useContext(CardContext);

  const extractPromotions = (products) => {
    if (!products) return [];

    const extracted = products.slice(0, 10);
    return extracted;
  };

  const { data } = useSWR("top_user_sells");
  const proPros = data?.data;

  return proPros ? (
    <SlidersCon>
      <Title>
        مجبوب ترین ها
        <LoyaltyOutlinedIcon />
      </Title>
      <PromoCon style={{ backgroundColor: "transparent" }}>
        <GContainer>
          <Prmsldr
            proPros={proPros}
            items={items}
            addToCard={addToCard}
            increaseCardBalance={increaseCardBalance}
            decreaseCardBalance={decreaseCardBalance}
            config={config}
            logo={logo}
            responsive={promoResponsive}
          />
        </GContainer>
      </PromoCon>
    </SlidersCon>
  ) : (
    <></>
  );
};

export const TopProducts = ({ config }) => {
  const {
    items,
    addToCard,
    increaseCardBalance,
    decreaseCardBalance,
  } = useContext(CardContext);

  const { data } = useSWR("top_products");
  const proPros = data?.data;

  return proPros ? (
    <SlidersCon>
      <Title>
        پرفروش ترین ها
        <StoreOutlinedIcon />
      </Title>
      <PromoCon>
        <GContainer>
          <Prmsldr
            proPros={proPros}
            items={items}
            addToCard={addToCard}
            increaseCardBalance={increaseCardBalance}
            decreaseCardBalance={decreaseCardBalance}
            config={config}
            logo={logo}
          />
        </GContainer>
      </PromoCon>
    </SlidersCon>
  ) : (
    <></>
  );
};

const responsive = {
  slidesToShow: 6,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

const Title = styled.p`
  display: flex;
  justify-content: flex-end;
  padding: 5px;
  padding-top: 15px;
  margin-bottom: 15px;

  svg {
    margin-left: 10px;
  }
`;

const SlidersCon = styled.div`
  width: 100%;
  padding-bottom: 15px;

  @media only screen and (max-width: 768px) {
    div {
      transform: none !important;
      box-shadow: none;
    }
  }
  @media only screen and (min-width: 768px) {
    padding-bottom: 15px;
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
    border-radius: 8px;
    background: #fff;
    margin: 16px 0;
    padding-bottom: 20px;
    /* width: calc(100% - 14px); */
    /* transform: translateX(21px); */
  }
`;

const ShowMore = styled.div`
  width: 165px !important;
  min-height: 250px;
  display: flex !important;
  justify-content: center;
  align-items: center;
  background: #fff;
  border-radius: 9px;
  box-shadow: -5px 10px 20px rgba(0, 0, 0, 0.05);
  cursor: pointer;
  flex-wrap: wrap;

  & > div {
    text-align: center;
    color: #424750;
    width: 165px;
  }

  svg {
    border: 1px solid;
    border-radius: 50%;
    padding: 5px;
    box-sizing: content-box;
    color: ${(props) => props.theme.palette.primary};
    fill: ${(props) => props.theme.palette.primary};
  }
`;

const GContainer = styled(Container)`
  @media only screen and (max-width: 767px) {
    padding: 0 !important;
  }
`;

const PromoCon = styled.div`
  width: 100%;
  padding: 15px 10px;
  margin-top: 5px;
  background-size: contain;
  background-repeat: no-repeat;

  @media only screen and (max-width: 767px) {
    padding: 15px 0;
  }

  &.promo {
    background-image: url("/images/proSlider/promo_bg.svg");
    background: #003058;
  }

  @media only screen and (min-width: 768px) {
    background-position: right;
  }
`;
