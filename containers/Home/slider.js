import { useContext } from "react";
import styled from "styled-components";
import { MySlider } from "./../../components/Slider";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { DeviceContext } from "../../context/DeviceContext";
import Container from "@material-ui/core/Container";
import Router from "next/router";
import useSWR from "swr";
import { config } from "../../context/server";
import { pathToSchema } from "../../utils/main";
import Skeleton from "@material-ui/lab/Skeleton";

const NewsCard = ({ grid, image, title, subTxt, link, ...props }) => {
  return (
    <GridWrapper
      item
      {...grid}
      {...props}
      onClick={() => Router.push(pathToSchema(link), link)}
    >
      <NewsPaper>
        <CartImg src={image} />
      </NewsPaper>
    </GridWrapper>
  );
};
const GridWrapper = styled(Grid)`
  cursor: pointer;
`;
const CartImg = styled.img`
  width: 100%;
  height: 100%;
`;

const NewsPaper = styled(Paper)`
  height: 100%;
  border-radius: 16px;
  overflow: hidden;
`;

const NewsCards = ({ items }) => {
  return (
    items &&
    items.map((item) => (
      <NewsCard
        grid={item.grid}
        image={item.image}
        link={item.link}
        {...item}
      />
    ))
  );
};

const News = () => {
  const { data } = useSWR("slider/desktop/main");

  const imagesData = data?.data
    ? data.data.sort((a, b) => a.priority > b.priority)
    : [];

  const newsItems = imagesData.map((item) => {
    return {
      ...item,
      grid: { sm: 12 },
      image: "https://www.bamadar.com/data" + item.image,
    };
  });

  const leftImage = newsItems.shift();
  return (
    <Container>
      <Wrapper container spacing={2} justify="center">
        <Grid container xs={12} sm={4} spacing={2}>
          <NewsCards items={newsItems} />
        </Grid>
        <LeftCard
          grid={{ sm: 8 }}
          image={leftImage?.image}
          title="دانلود اپلیکیشن"
          subTxt="فروشگاهی در دستان شما، با تنوع بی نظیر بیش از هشت هزار نوع کالا. با دسترسی سریع به کالاها و دریافت فوری با رعایت اصول و نکات بهداشتی"
          style={{ padding: "0 12px", paddingRight: 0 }}
          link={leftImage?.link}
          item
        />
      </Wrapper>
    </Container>
  );
};

export const Slider = () => {
  const { isMobile } = useContext(DeviceContext);
  const { data } = useSWR("slider/mobile/main");
  const imagesData = data?.data
    ? data.data.sort((a, b) => a.priority > b.priority)
    : [];
  const images = imagesData.map((item) => {
    return { ...item, image: "https://www.bamadar.com/data" + item.image };
  });

  return isMobile ? (
    data ? (
      <Sldr>
        <MySlider images={images} />
      </Sldr>
    ) : (
      <Skeleton variant="rect" width="100vw" height={250} />
    )
  ) : (
    <News />
  );
};

const Wrapper = styled(Grid)`
  padding: 30px 10px;
`;

const LeftCard = styled(NewsCard)`
  @media only screen and (max-width: 598px) {
    height: 100%;
    margin-bottom: 16px;
  }
`;

const Sldr = styled.div`
  width: 100%;
  .rec-slider-container {
    margin: 0 !important;
  }

  img {
    width: 100%;
  }
`;
