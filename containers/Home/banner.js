import { useContext } from "react";
import styled from "styled-components";
import { Slider } from "./slider";
import { PromoSlider, TopSells, TopProducts } from "./sliders";
import { Category } from "./categories";
import { Pwa } from "./Pwa";
import { DeviceContext } from "../../context/DeviceContext";
import Container from "@material-ui/core/Container";
import Router from "next/router";

export const Banner = ({ config }) => {
  return (
    <div>
      <Wrapper onClick={() => Router.push("/category?catid=139")}>
        <School src="/images/school/1.png" />
        <Bigcloud src="/images/school/2.png" />
        <Smallcloud src="/images/school/2.png" />
        <Pen src="/images/school/4.png" />
      </Wrapper>
    </div>
  );
};

const Wrapper = styled.div`
  margin: auto;
  position: relative;
  padding-top: 10px;
  background: #0e3368;
  cursor: pointer;
  @media only screen and (min-width: 768px) {
    display: none;
  }
  /* overflow-y: hidden; */
`;

const School = styled.img`
  display: block;
  width: 100%;
  height: auto;
  box-shadow: 2px -14px 20px #001c33;
`;

const Bigcloud = styled.img`
  position: absolute;
  top: -10px;
  right: 0;
  width: 100px;

  @keyframes moveRight {
    0% {
      transform: translateX(-100%);
    }
    50% {
      transform: translateX(0);
    }
    100% {
      transform: translateX(-100%);
    }
  }
  animation: moveRight 10s infinite;
`;
const Smallcloud = styled.img`
  position: absolute;
  top: 10px;
  left: 0;
  width: 80px;

  @keyframes moveLeft {
    0% {
      transform: translateX(80%);
    }
    50% {
      transform: translateX(0);
    }
    100% {
      transform: translateX(80%);
    }
  }
  animation: moveLeft 11s infinite;
`;

const Pen = styled.img`
  width: 90px;
  position: absolute;
  bottom: 0;
  right: 50%;
`;
