import React, { useEffect, useState } from "react";
import styled from "styled-components";

export const Pwa = () => {
  const [deferredPrompt, setdeferredPrompt] = useState(null);

  useEffect(() => {
    if ("serviceWorker" in navigator) {
      navigator.serviceWorker
        .register("/serviceWorker.js")
        .catch((err) =>
          console.error("Service worker registration failed", err)
        );
    } else console.log("Service worker not supported");

    window.addEventListener("beforeinstallprompt", (e) => {
      e.preventDefault();
      setdeferredPrompt(e);
    });
  }, []);

  return (
    deferredPrompt && (
      <Wrapper>
        <Banner
          onClick={(e) => {
            deferredPrompt.prompt();
            deferredPrompt.userChoice.then((choiceResult) => {
              if (choiceResult.outcome === "accepted") {
                console.log("User accepted the install prompt");
              } else {
                console.log("User dismissed the install prompt");
              }
            });
          }}
        >
          <Img src="images/installApp.svg" />
          <TextCon>
            <BannerText>
              نصب سریع برنامه
              <BannerSubText>
                دسترسی آسان و سریع به فروشگاه بزرگ بامادر
              </BannerSubText>
            </BannerText>
          </TextCon>
        </Banner>
      </Wrapper>
    )
  );
};

const Wrapper = styled.div`
  padding: 0 10px;
  margin: 10px 0;
`;

const Banner = styled.div`
  width: 100%;
  background: rgba(23, 155, 191, 0.4);
  border-radius: 10px;
  display: grid;
  direction: rtl;
  grid-template-columns: 150px 1fr;
  padding: 5px 15px;
`;
const Img = styled.img`
  width: 150px;
  height: 150px;
`;

const BannerText = styled.p`
  font-size: 1.2rem;
`;
const BannerSubText = styled.p`
  font-size: 0.8rem;
`;

const TextCon = styled.div`
  display: grid;
  justify-content: center;
  align-items: center;
  text-align: center;
`;
