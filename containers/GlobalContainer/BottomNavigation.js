import React, { useContext, useState, useEffect } from "react";
import BottomNavigation from "../../components/BottomNavigation/BottomNavigation";
import { CardContext } from "../../context/cardContext";
import Router, { useRouter } from "next/router";
import User from "../../components/icons/user";
import Cart from "../../components/icons/Cart";
import Categories from "../../components/icons/Masonry";
import Home from "../../components/icons/Home";
import TimelineOutlinedIcon from "@material-ui/icons/TimelineOutlined";
import { Static } from "../../components/icons/Static";
import { Support } from "../../components/icons/Support";

export default ({ currentNav }) => {
  const { items } = useContext(CardContext);

  const inputs = [
    {
      icon: <Home />,
      onClick: () => Router.push("/"),
      label: "خانه",
      id: "home",
    },
    {
      icon: <Categories />,
      onClick: () => Router.push({ pathname: "/category" }),
      label: "دسته‌بندی",
      id: "categories",
    },
    {
      icon: <Support />,
      label: "پشتیبانی‌آنلاین",
      onClick: () => {},
      id: "history",
    },
    {
      icon: <Cart />,
      onClick: () => Router.push("/cart"),
      label: "سبد خرید",
      badge: items.length,
      id: "cart",
    },
    {
      icon: <User />,
      label: "پروفایل",
      onClick: () => Router.push("/profile"),
      id: "panel",
    },
  ];
  return <BottomNavigation inputs={inputs} currentState={currentNav} />;
};
