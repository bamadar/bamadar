import React, { useContext, useState, useEffect, useRef } from "react";
import styled, { css } from "styled-components";
import { Header } from "../../components/Header/Header";
import BottomNavigation from "../../components/BottomNavigation/BottomNavigation";
import Router, { useRouter } from "next/router";
import User from "../../components/icons/user";
import Cart from "../../components/icons/Cart";
import Categories from "../../components/icons/Masonry";
import Home from "../../components/icons/Home";
import dynamic from "next/dynamic";
import { CardContext } from "../../context/cardContext";

export const ReactTour = ({ withTour }) => {
  let Tour = useRef(undefined);
  if (typeof window !== "undefined")
    Tour.current = dynamic(() => import("reactour"));

  const [isTourOpen, setIsTourOpen] = useState(true);

  const onRequestClose = () => {
    window.localStorage.setItem("guided", "true");
    setIsTourOpen(false);
  };

  const lastStepNextButton = () => {
    const onClickHandler = () => {
      window.localStorage.setItem("guided", "true");
      setIsTourOpen(false);
    };
    return <Dana onClick={onClickHandler}>خروج</Dana>;
  };

  return typeof window !== "undefined" && withTour ? (
    <Tour.current
      onRequestClose={onRequestClose}
      steps={guidedSteps}
      badgeContent={(curr, tot) => <Dana>{curr}</Dana>}
      isOpen={isTourOpen}
      className="helper"
      rounded={5}
      accentColor={"#5cb7b7"}
      lastStepNextButton={lastStepNextButton}
    />
  ) : (
    <></>
  );
};
const guidedSteps = [
  {
    content: () => (
      <CenteredTxt>
        <p>
          .به بامادر خوش آمدید
          <br />
          برای ورود به تور آموزشی بامادر روی ➔ کلیک کنید
        </p>
      </CenteredTxt>
    ),
  },
  {
    selector: "#searchBar",
    content: () => (
      <CenteredTxt>
        محصول موردنظر خود را می‌توانید دراینجا جستجو کنید
      </CenteredTxt>
    ),
  },
  {
    selector: "#promoSlider",
    content: () => (
      <CenteredTxt>
        پیشنهاد ویژه و آخرین تخفیف ها از اینجا در دسترس شماست. با زدن روی ✚
        محصولات را به سبد خرید اضافه کنید
      </CenteredTxt>
    ),
  },
  {
    selector: "#home",
    content: () => (
      <CenteredTxt>
        در هر زمانی میتوانید با فشردن این گزینه به صفحه اصلی بازگردید
      </CenteredTxt>
    ),
  },
  {
    selector: "#categories",
    content: () => (
      <CenteredTxt>
        دسته بندی های محصولات بامادر از اینجا در دسترس شماست
      </CenteredTxt>
    ),
  },
  {
    selector: "#history",
    content: () => (
      <CenteredTxt>
        از این قسمت می‌توانید با ما در ارتباط باشید. پشتیبانی آنلاین در ساعات
        کاری هایپرمارکت مادر
      </CenteredTxt>
    ),
  },
  {
    selector: "#cart",
    content: () => (
      <CenteredTxt>
        میتوانید لیست محصولات خود را از این قسمت مشاهده و ویرایش کنید. هر محصول
        پس از افزوده شدن به سبد خرید در این قسمت قابل مشاهده است
      </CenteredTxt>
    ),
  },
  {
    selector: "#panel",
    content: () => (
      <CenteredTxt>
        تاریخچه خریدها، ورود به سامانه و ویرایش اطلاعات از این قسمت در دسترس است
      </CenteredTxt>
    ),
  },
];

const CenteredTxt = styled.div`
  text-align: center;
`;

const Dana = styled.span`
  font-family: "IRANSans";
`;
