import React, { useContext, useState, useEffect, useRef } from "react";
import styled, { css } from "styled-components";
import { Header } from "../../components/Header/Header";
import BottomNavigation from "./BottomNavigation";
import Router, { useRouter } from "next/router";
import { ReactTour } from "./GuidedTour";
import Loading from "./Loading";
import { DeviceContext } from "../../context/DeviceContext";
import { Footer } from "../../components/Footer.js";
import ReactNotification from "react-notifications-component";

export default ({ children, currentNav, deleteHeader, withTour }) => {
  const { isMobile } = useContext(DeviceContext);
  const { pathname } = useRouter();
  deleteHeader = !isMobile && false;

  const deletePadding =
    isMobile &&
    !(
      pathname === "/products" ||
      pathname === "/category/[id]" ||
      pathname === "/product/[id]" ||
      pathname === "/promotions" ||
      pathname === "/555"
    );

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <GlobalWrapper>
      <ReactNotification />
      <MainWrapper deletePadding={deletePadding}>
        {!deleteHeader && <Header />}
        <ContentWrapper>
          <Loading />
          {children}
        </ContentWrapper>
        <BottomNavigation currentNav={currentNav} />
        <ReactTour withTour={withTour} />
      </MainWrapper>
      {(pathname == "/shop" || pathname == "/") && <Footer />}
    </GlobalWrapper>
  );
};
const GlobalWrapper = styled.div`
  width: 100vw;
  overflow-x: hidden;
`;

const MainWrapper = styled.div`
  margin: 0 auto;
  width: 100%;
  display: flex;
  justify-content: flex-start;
  position: relative;
  flex-direction: column;
  align-items: center;
  font-family: IRANSans;
  padding-top: ${(props) => (props.deletePadding ? "0" : "60px")};

  @media only screen and (min-width: 768px) {
    padding-top: ${(props) => (props.deletePadding ? "0" : "146px")};
  }

  @media only screen and (max-width: 767px) {
    padding-bottom: 53px;
  }

  span,
  p {
    font-family: IRANSans;
  }
`;

const ContentWrapper = styled.div`
  flex-grow: 1;
  width: 100%;
  background: #fff;
  color: ${(props) => props.theme.colors.black};

  @media only screen and (min-width: 768px) {
    background: #f5f5f5;
    min-height: 100vh;
  }

  ${(p) =>
    p.boxed &&
    css`
      padding: 20px;
    `}
`;
