import React, { useState } from "react";
import styled from "styled-components";
import Router from "next/router";
import Progress from "../../components/Loading";

export default ({ props }) => {
  const [loading, setloading] = useState(false);

  Router.events.on("routeChangeStart", () => setloading(true));
  Router.events.on("routeChangeComplete", () => setloading(false));
  Router.events.on("routeChangeError", () => setloading(false));

  return (
    <>
      {loading && (
        <Loading>
          <Progress />
        </Loading>
      )}
    </>
  );
};

const Loading = styled.div`
  width: 100vw;
  height: 100vh;

  z-index: 1600;
  position: fixed;
  left: 0;
  top: 0;
  background: white;

  display: flex;
  justify-content: center;
  align-items: center;

  @media only screen and (min-width: 768px) {
    background: #f5f5f5;
    min-height: 100vh;
  }
`;
