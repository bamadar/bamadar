import React, { useState, useEffect, useContext } from "react";
import styled from "styled-components";
import Router, { useRouter } from "next/router";
import { config, productsAction } from "../../context/server";
import { CategoryTab } from "./Item";
import useSWR from "swr";
import { MySlider } from "../../components/Slider";
import { ScrollableTabs } from "../../components/Tab";
import { DeviceContext } from "../../context/DeviceContext";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Collapse from "@material-ui/core/Collapse";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Progress from "../../components/Loading";
import { Carousel } from "../../components/Carousel";

const TabPanels = ({ catId }) => {
  const { data, error } = useSWR("categories?sub=" + catId);
  const { asPath } = useRouter();

  const createHeader = () => {
    if (data.data.length < 2) return <></>;
    const max = data.data.length,
      min = 0,
      index = Math.floor(Math.random() * (max - min + 1) + min),
      res = data.data[index] || data.data[0];

    return (
      <SubCatsHeader
        onClick={() => {
          Router.push("/category/[id]", "/category/" + res.id);
        }}
      >
        <img src={res.image ? config + res.image : "images/no image.png"} />
        <p>{res.name}</p>
      </SubCatsHeader>
    );
  };

  return (
    <>
      <Panels>
        {data && data.data && createHeader()}
        <CategoriesCon>
          {data &&
            data.data &&
            data.data.map((data) => (
              <SubcatCon
                onClick={() => {
                  Router.push("/category/[id]", "/category/" + data.id);
                }}
              >
                <img
                  src={data.image ? config + data.image : "images/no image.png"}
                />
                <SubCatName>{data.name}</SubCatName>
              </SubcatCon>
            ))}
        </CategoriesCon>
      </Panels>
    </>
  );
};

const SubCatName = styled.div`
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  direction: rtl;
`;

const CategoriesCon = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  min-height: 50vh;
  align-content: baseline;
  justify-content: center;
  padding-bottom: 12px;
`;

const SubCatsHeader = styled.div`
  width: 100%;
  background: #ffffff;
  border-radius: 10px;
  margin: auto;
  height: 137px;
  margin-bottom: 10px;
  padding: 0 16px;
  display: flex;
  align-items: center;
  text-align: center;
  justify-content: space-between;

  img {
    display: block;
    width: 125px;
    height: 125px;
  }
`;

const MobileCategory = ({ mainCats }) => {
  const { query } = useRouter();
  const iniTab = query.catid
    ? mainCats.findIndex((x) => x.id == parseInt(query.catid))
    : mainCats.length - 1;
  const [sliderIndex, setsliderIndex] = useState(iniTab);

  const tabPanel = mainCats.map((data) => <TabPanels catId={data.id} />);
  const tabs = mainCats.map((data, index) => (
    <CategoryTab
      productsAction={productsAction}
      active={index == sliderIndex}
      data={data}
      config={config}
      key={data.id}
      onClick={() => {
        Router.push("/category?catid=" + data.id);
        setsliderIndex(index);
      }}
    />
  ));
  return (
    <Content>
      <SubCatsCon>
        {mainCats.length && mainCats[sliderIndex] && (
          <img
            src={config + mainCats[sliderIndex].banner}
            style={{ width: "100vw", height: "min-intrinsic" }}
          />
        )}
        {/* <Carousel style={{ marginRight: 16 }}>{tabs}</Carousel> */}
        {/* {tabPanel[sliderIndex]} */}
        <ScrollableTabs tabs={tabs} iniTab={sliderIndex} tabPanel={tabPanel} />
      </SubCatsCon>
    </Content>
  );
};

const MobilePage = ({ mainCats }) => {
  if (!mainCats.length)
    return (
      <Center>
        <Progress />
      </Center>
    );
  else return <MobileCategory mainCats={mainCats} />;
};

const Center = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const SeeMore = styled.div`
  display: grid;
  flex-wrap: wrap;
  width: 150px;
  justify-content: center;
  text-align: center;
`;

const More = styled.div`
  color: ${(props) => props.theme.palette.primary};
  font-size: 0.8rem;
  display: flex;
  align-items: center;
  cursor: pointer;

  svg {
    color: ${(props) => props.theme.palette.primary} !important;
  }
`;

const Title = styled.p`
  font-size: 0.8rem;
  font-weight: bold;
`;

const CatItem = ({ item, sub_categories }) => {
  const [catId, setcatId] = useState(null);
  const { query } = useRouter();
  const iniTab = query.catid;

  const [checked, setChecked] = React.useState(false);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };

  return (
    <ListItem
      onClick={() => {
        setcatId(item.id);
      }}
    >
      <SeeMore>
        <img
          src={item.image ? config + item.image : "images/no image.png"}
          alt={item.name}
        />
        <Title>{item.name}</Title>
        {sub_categories && sub_categories.length > 5 && (
          <FormControlLabel
            control={
              <More onClick={handleChange}>
                {checked ? "مشاهده کمتر" : "مشاهده همه"}
                <ExpandMoreIcon checked={checked} />
              </More>
            }
          />
        )}
      </SeeMore>

      <SubItems>
        {sub_categories &&
          sub_categories.map((item, index) => {
            return (
              index < 5 && (
                <p
                  onClick={() => {
                    Router.push("/category/[id]", "/category/" + item.id);
                  }}
                  style={{ cursor: "pointer", display: "block ruby" }}
                >
                  {item.name}
                  <ArrowForwardIosIcon />
                </p>
              )
            );
          })}
        <Collapse in={checked}>
          {sub_categories &&
            sub_categories.map((item, index) => {
              return (
                index > 4 && (
                  <p
                    onClick={() => {
                      Router.push("/category/[id]", "/category/" + item.id);
                    }}
                    style={{ cursor: "pointer" }}
                  >
                    {item.name}
                    <ArrowForwardIosIcon />
                  </p>
                )
              );
            })}
        </Collapse>
      </SubItems>
    </ListItem>
  );
};

const DesktopPage = ({ mainCats }) => {
  return (
    <CardsWrapper>
      <GridList
        xs={1}
        sm={1}
        md={2}
        lg={3}
        xl={4}
        cellHeight={200}
        spacing={10}
        cols={4}
        justify="center"
      >
        {mainCats &&
          mainCats
            .reverse()
            .map((item) => (
              <CatItem
                item={item}
                sub_categories={item && item.sub_categories}
              />
            ))}
      </GridList>
    </CardsWrapper>
  );
};

const Page = ({ mainCats }) => {
  const { isMobile } = useContext(DeviceContext);
  const { data, error } = useSWR("all_categories");

  return isMobile ? (
    <MobilePage mainCats={mainCats} />
  ) : (
    <DesktopPage mainCats={data} />
  );
};

export default Page;

const GridList = styled.div`
  display: grid;
  grid-template-columns: repeat(${({ cols }) => (cols ? cols : 1)}, 1fr);
  grid-gap: ${({ spacing }) => `${spacing}px`};
  justify-content: ${({ justify }) => justify};

  @media only screen and (max-width: 600px) {
    grid-template-columns: repeat(${({ xs }) => xs}, 1fr);
  }

  @media only screen and (min-width: 600px) {
    grid-template-columns: repeat(${({ sm }) => sm}, 1fr);
  }

  @media only screen and (min-width: 768px) {
    grid-template-columns: repeat(${({ md }) => md}, 1fr);
  }

  @media only screen and (min-width: 992px) {
    grid-template-columns: repeat(${({ lg }) => lg}, 1fr);
  }

  @media only screen and (min-width: 1400px) {
    grid-template-columns: repeat(${({ xl }) => xl}, 1fr);
  }
`;

const CardsWrapper = styled.div`
  padding: 30px;
  margin: auto;
`;

const ListItem = styled.div`
  display: flex;
  border-radius: 5px;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 5px;
  justify-content: space-between;
  padding: 5px;
  @media only screen and (min-width: 768px) {
    background: #fff;
  }

  img {
    width: 85px;
    height: 85px;
    justify-self: center;
  }
  .MuiFormControlLabel-root {
    margin: auto;
  }
`;

const SubItems = styled.p`
  text-align: right;
  padding: 5px 10px;
  line-height: 1.8rem;
  font-size: 0.8rem;

  svg {
    font-size: 0.6rem;
    margin-left: 8px;
    color: ${(props) => props.theme.palette.primary};
  }
`;

const Img = styled.img`
  width: 100%;
`;

const Panels = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 8px;
  justify-content: center;
  padding: 4px;
  background: #f1f1f1;
`;

const SubcatCon = styled.div`
  height: 125px;
  border-radius: 5px;
  background: #fff;
  display: grid;
  justify-items: center;
  align-items: center;
  padding: 5px;
  margin: 2.5px;
  color: #5e5e5e;
  font-size: 0.8rem;
  width: calc(33% - 5px);

  overflow: hidden;
  text-align: center;
  font-size: 0.8rem;

  cursor: pointer;

  & > div {
    margin-top: 5px;
  }

  @media only screen and (min-width: 768px) {
    height: auto;
  }

  img {
    height: 70px;
    width: 70px;
  }
`;

const Content = styled.div`
  flex-grow: 1;
  width: 100%;
  background: #f1f1f1;
  border-radius: 16px 16px 0px 0px;
  overflow: auto;
  margin-top: -20px;
  color: ${(props) => props.theme.colors.black};
  svg path {
    fill: ${(props) => props.theme.palette.secondary};
  }
`;

const SubCatsCon = styled.div`
  width: 100%;
  height: auto;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  .MuiTabs-scroller {
    background: #f1f1f1;
  }
  .rec-slider-container {
    margin: 0 !important;
  }
  .PrivateTabIndicator-colorSecondary-8,
  .PrivateTabIndicator-colorSecondary-22,
  .MuiTabs-indicator {
    background: inherit !important;
    display: none;
  }
`;

const ExpandMore = styled(ExpandMoreIcon)`
  color: black;
  font-size: 16px;
  transform: ${({ checked }) => `rotate(${checked ? "-180deg" : "0"})`};
  transition: 250ms;
`;
