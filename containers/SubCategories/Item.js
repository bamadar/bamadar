import React, { useContext } from "react";
import styled, { css } from "styled-components";
import Router, { useRouter } from "next/router";
import { SmallBasketIcon } from "../../components/icons/Icons";
import uniqid from "uniqid";
import { DeviceContext } from "./../../context/DeviceContext";
import Skeleton from "@material-ui/lab/Skeleton";

export const CategoryTab = ({
  productsAction,
  data,
  config,
  active,
  ...props
}) => {
  const { asPath } = useRouter();

  return (
    <Item
      {...props}
      active={active}
      style={{ background: asPath == "/shop" && "rgb(236,237,239)" }}
    >
      <ImgCon>
        {data && data.name ? (
          data.image ? (
            <Img src={config + data.image} alt={data.name} />
          ) : (
            <Img src="images/no image.png" style={{ width: 75, height: 79 }} />
          )
        ) : (
          <Skeleton variant="circle" width={80} height={80} />
        )}
      </ImgCon>

      {data && data.name ? (
        <Name>{data.name}</Name>
      ) : (
        <Skeleton
          variant="rect"
          width={80}
          height={15}
          style={{ margin: "auto" }}
        />
      )}
    </Item>
  );
};

const ImgCon = styled.div`
  border-radius: 6px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;

  @media only screen and (min-width: 768px) {
    background: #fff;
  }
`;

const Img = styled.img`
  width: 75px;
  height: 100px;
`;

const Name = styled.span`
  display: block;
  font-size: 13px;
  color: #5e5e5e;
  text-align: center;
  padding: 5px 0;
`;

const Item = styled.div`
  @media only screen and (min-width: 768px) {
    min-width: 200px;
    padding: 0 15px;
    margin-right: 15px;
    box-shadow: rgba(0, 0, 0, 0.2) 0px 0px 5px;
    min-height: 66px;
    background-color: rgb(255, 255, 255);
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    border-radius: 5px;
    text-decoration: none;
    margin: 5px 10px;
  }
  height: auto;
  width: auto;
  min-width: 100px;
  min-height: 150px;
  background: #fff;
  margin-bottom: 10px;
  border-radius: 5px;
  display: grid;
  justify-content: center;
  align-items: center;
  ${({ active }) =>
    active &&
    css`
      box-shadow: 0px 0px 7px rgba(0, 0, 0, 0.25);
    `}

  @media only screen and (max-width: 767px) {
    width: 100px;
    flex-wrap: nowrap;
  }

  svg {
    display: block;
    width: 100%;
    height: 50px;
    margin: 5px auto;
    path {
      text-align: center;
      fill: ${(props) => props.theme.colors.purple};
    }
  }
`;
