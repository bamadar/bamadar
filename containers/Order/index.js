import { useState, useContext, useEffect, useRef } from "react";
import styled from "styled-components";
import { OrderForm } from "./form";
import { SuccessAlert, ErrorAlert } from "../../components/Alert";
import Router, { useRouter } from "next/router";
import { loginCheck, phoneValidation } from "../../utils/main";
import { postUserInfo, getUserInfo } from "../../context/server";
import { setInfo } from "../../context/localStorage";
import { pushNotification } from "../../components/Notification";
import SupervisedUserCircleIcon from "@material-ui/icons/SupervisedUserCircle";
import PhoneRoundedIcon from "@material-ui/icons/PhoneRounded";
import LocationOnRoundedIcon from "@material-ui/icons/LocationOnRounded";
import PersonRoundedIcon from "@material-ui/icons/PersonRounded";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import { FixedSubmit, SubmitCart } from "../../components/FixedSubmit";
import Select from "@material-ui/core/Select";
import {
  just_persian,
  justDigit,
  chackPelak,
  persianToStandard,
} from "../../utils/input-validator";
import HistoryIcon from "@material-ui/icons/History";
import GroupAddIcon from "@material-ui/icons/GroupAdd";
import { geolocated } from "react-geolocated";
import LocationOnOutlinedIcon from "@material-ui/icons/LocationOnOutlined";

const InfoContainer = ({
  title,
  icon,
  inputs,
  extraInput,
  refs,
  values,
  ...props
}) => {
  return (
    <InfoCon {...props}>
      <InfoWrapper>
        <InfoTitle>{title}</InfoTitle>
        <InfoIcon>{icon}</InfoIcon>
      </InfoWrapper>
      <InputCon>
        {inputs?.map((i, index) => (
          <input
            placeholder={i}
            defaultValue={values && values[index]}
            ref={refs && refs[index]}
          />
        ))}
        {extraInput}
      </InputCon>
    </InfoCon>
  );
};
const AddressArea = ({ refs, userInfo }) => {
  const [closeMap, setcloseMap] = useState(true);
  const [location, setlocation] = useState();

  // useEffect(() => {
  //   var map = L.map("mapid", { doubleClickZoom: false }).locate({
  //     setView: true,
  //     maxZoom: 16,
  //   });

  //   L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  //     maxZoom: 16,
  //     attribution:
  //       '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  //   }).addTo(map);

  //   let curMark, marker;
  //   map.on("locationfound", (e) => {
  //     marker = new L.Marker(e.latlng, { draggable: true });
  //     curMark = new L.Marker(e.latlng, { draggable: true });
  //     setlocation(e.latlng);
  //     map.addLayer(marker);
  //   });
  //   map.on("click", onMapClick);

  //   function onMapClick(e) {
  //     if (marker) map.removeLayer(marker);
  //     marker = new L.Marker(e.latlng, { draggable: true });
  //     map.addLayer(marker);
  //     setlocation(e.latlng);
  //     map.panTo(e.latlng, { animate: true, duration: 1.0 });
  //     //marker.bindPopup("😃سفارش من رو اینجا ارسال کن").openPopup();÷
  //   }

  //   document
  //     .getElementById("pure-button")
  //     .addEventListener("click", function () {
  //       if (marker) {
  //         map.removeLayer(marker);
  //         map.removeLayer(curMark);
  //       }
  //       map.locate({ setView: true, maxZoom: 16 });
  //       setlocation(curMark);
  //     });

  //   map.on("click", onMapClick);
  // }, []);

  const addr = userInfo?.address?.split("دزفول ").join("").trim();

  return (
    <>
      <textarea
        name="message"
        rows="3"
        placeholder="آدرس"
        defaultValue={addr?.split("&dezful").join("")?.trim()}
        ref={refs.address}
      />

      <AddrCon>
        <input
          placeholder="پلاک"
          defaultValue={userInfo?.pelak}
          ref={refs.pelak}
          style={{
            width: 67,
            textAlign: "center",
          }}
        />
        <Select
          native
          value={10}
          style={{
            width: 67,
          }}
        >
          <option value={10}>دزفول</option>
          {/* <option value={20}>اندیمشک</option> */}
        </Select>
      </AddrCon>
      {/* <MapCon close={closeMap}>
        <div
          id="mapid"
          style={{
            zIndex: closeMap ? 1 : 99999999999999,
            pointerEvents: closeMap ? "none" : "fill",
          }}
        ></div>
      </MapCon> */}
      {/* <AddrCon
        style={{
          display: "flex",
          justifyContent: "center",
          marginTop: 8,
        }}
      >
        <SubmitCart
          onClick={() => setcloseMap(false)}
          style={{
            backgroundColor: "#179bbf",
            fontSize: "0.9rem",
            width: "100%",
            color: "#fff",
            display: "none",
          }}
        >
          انتخاب از روی نقشه
        </SubmitCart>
      </AddrCon>

      <MapAccsCon
        style={{
          zIndex: closeMap ? -10 : 99999999999999,
          pointerEvents: closeMap ? "none" : "fill",
          visibility: closeMap ? "hidden" : "visible",
        }}
      >
        <LocationOnOutlinedIcon
          id="pure-button"
          style={{
            width: 30,
            height: 30,
            marginRight: 14,
          }}
        />
        <AccLocation
          onClick={() => {
            refs.location = location;
            setcloseMap(true);
          }}
        >
          تایید مقصد
        </AccLocation>
      </MapAccsCon> */}
    </>
  );
};

const MapAccsCon = styled.div`
  width: 100%;
  height: 90px;
  background: #fff;
  position: fixed;
  bottom: -9px;
  right: 0px;
  display: grid;
  grid-template-columns: auto 1fr;
  justify-content: center;
  align-items: center;
`;

const CurLocationBtn = styled(LocationOnOutlinedIcon)``;

const UserInfo = ({ refs, setLoading, prevUserInfo }) => {
  const [userInfo, setuserInfo] = useState(prevUserInfo);

  useEffect(() => {
    setLoading(true);
    getUserInfo().then((res) => {
      if (res && res.data && res.data.status === "ok") {
        const data = res.data.data;
        const {
          address,
          last_name,
          mobile,
          first_name,
          no,
          phone,
          referral,
        } = data;
        const city = "dezful";

        const updatedInfo = {
          address: address,
          family: last_name,
          mobile: mobile,
          name: first_name,
          pelak: no,
          tel: phone,
          // payment: refs.payment == "0" ? "cash" : "onine",
          payment: "cash",
          city: "dezful",
        };
        setInfo(updatedInfo);

        setuserInfo({
          address: address ? address.replace(`&${city}`, "") : "",
          city: "dezful",
          family: last_name,
          mobile: mobile,
          name: first_name,
          pelak: no,
          tel: phone,
          payment: "cash",
          referral: referral,
        });
      } else
        pushNotification("دریافت اطلاعات شما با خطا مواجه شد", " ", "danger");

      setLoading(false);
    });
  }, []);

  return (
    <>
      <InfoContainer
        title="اطلاعات فردی"
        icon={<PersonRoundedIcon />}
        inputs={["نام", "نام خانوادگی"]}
        values={[userInfo?.name, userInfo?.family]}
        refs={[refs.name, refs.family]}
      />
      <InfoContainer
        title="تلفن"
        icon={<PhoneRoundedIcon />}
        inputs={["تلفن ثابت"]}
        refs={[refs.tel]}
        values={[userInfo?.tel]}
        extraInput={
          <input disabled value={userInfo?.mobile} ref={refs.mobile} />
        }
      />
      <InfoContainer
        title="آدرس"
        icon={<LocationOnRoundedIcon />}
        extraInput={<AddressArea refs={refs} userInfo={userInfo} />}
      />
      <InfoContainer
        title="شماره موبایل معرف"
        style={{ marginTop: 15 }}
        icon={<GroupAddIcon />}
        extraInput={
          <input
            disabled={userInfo?.referral}
            value={userInfo?.referral}
            placeholder="_________09"
            ref={refs.referral}
          />
        }
      />
    </>
  );
};

const Payment = ({ refs }) => {
  return (
    <Form
      component="fieldset"
      ref={refs.payment}
      onChange={(e) => (refs.payment = e.target.value)}
    >
      <FormTitle component="legend">شیوه پرداخت</FormTitle>
      <RadioGroup>
        <FormInput
          value="0"
          control={<Radio color="primary" checked={true} />}
          label="پرداخت درب منزل"
          labelPlacement="start"
        />
        {/* <FormInput
          value="1"
          control={<Radio color="primary" />}
          label="پرداخت اینترنتی"
          labelPlacement="start"
        /> */}
      </RadioGroup>
    </Form>
  );
};

const Order = ({ userInfo }) => {
  const { query } = useRouter();
  const justShowInfo = query && query.userInfo === "true";
  const [loading, setLoading] = useState(false);
  const refs = {
    address: useRef(null),
    city: useRef(null),
    family: useRef(null),
    mobile: useRef(null),
    name: useRef(null),
    pelak: useRef(null),
    tel: useRef(null),
    referral: useRef(null),
    payment: undefined,
    location: {},
  };

  const validateInputs = (values) => {
    const nameStatus = values.name && just_persian(values.name.trim());
    const familyStatus = values.family && just_persian(values.family.trim());
    const addressStatus =
      values.address &&
      (!values.address.trim() ||
        !(values.address.replace(`&${values.city}`, "") == ""));

    const telStatus =
      values.tel && justDigit(persianToStandard(values.tel.trim()));
    const pelakStatus =
      values.pelak && chackPelak(persianToStandard(values.pelak.trim()));

    !nameStatus &&
      pushNotification("نام خود را به فارسی وارد کنید", " ", "danger");

    !familyStatus &&
      pushNotification("نام خانوادگی خود را به فارسی وارد کنید", " ", "danger");

    !addressStatus && pushNotification("آدرس را وارد کنید", " ", "danger");

    !telStatus &&
      pushNotification("شماره تلفن را به عدد وارد کنید", " ", "danger");

    !pelakStatus &&
      pushNotification("پلاک خود را به عدد وارد کنید", " ", "danger");

    // if (!justShowInfo && !values.payment)
    // pushNotification("نحوه پرداخت را انتخاب کنید", " ", "danger");

    if (
      !nameStatus ||
      !familyStatus ||
      !addressStatus ||
      !telStatus ||
      !pelakStatus
      // || (!justShowInfo && !values.payment)
    )
      return false;

    if (!values?.referral?.trim()) return true;

    if (
      persianToStandard(values?.referral) == persianToStandard(values?.mobile)
    ) {
      pushNotification("شماره معرف با شماره همراه یکسان است.", " ", "danger");
      return false;
    }

    if (
      values?.referral &&
      !phoneValidation(persianToStandard(values?.referral))
    ) {
      pushNotification("شماره معرف وارد شده نامعتبر است", " ", "danger");
      return false;
    }

    return true;
  };

  const sendPack = async () => {
    const updatedValues = {
      address: refs.address.current.value,
      family: refs.family.current.value,
      mobile: refs.mobile.current.value,
      name: refs.name.current.value,
      pelak: refs.pelak.current.value,
      tel: refs.tel.current.value,
      // payment: refs.payment == "0" ? "cash" : "onine",
      payment: "cash",
      city: "dezful",
      referral: refs.referral.current.value,
      location: refs.location,
    };
    if (!validateInputs({ ...updatedValues, payment: refs.payment })) return;

    setLoading(true);
    postUserInfo(updatedValues)
      .then((res) => {
        setLoading(false);
        if (res && res.data && res.data.message == "updated") {
          setInfo(updatedValues);
          if (justShowInfo)
            pushNotification(
              "اطلاعات شما با موفقیت در سیستم ثبت گردید",
              " ",
              "success"
            );
          else Router.push("/finalizeOrder");
        } else pushNotification("درخواست شما با خطا مواجه شد", " ", "danger");
      })
      .catch((er) =>
        pushNotification("خطایی در سرور رخ داده است", " ", "danger")
      );
  };
  const buttonText = justShowInfo ? "ثبت اطلاعات" : "ثبت اطلاعات و ادامه";

  return (
    <Content>
      <Title> ویرایش اطلاعات</Title>
      <UserInfo refs={refs} setLoading={setLoading} prevUserInfo={userInfo} />
      {!justShowInfo && <Payment refs={refs} />}
      <Button onClick={sendPack}>{buttonText}</Button>
      <HideOnDesktop>
        <FixedSubmit
          buttonTxt={buttonText}
          onClick={sendPack}
          loading={loading}
        />
      </HideOnDesktop>
    </Content>
  );
};

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(Order);

const AccLocation = styled(SubmitCart)`
  width: calc(100% - 64px);
  height: 46px;
  border-color: transparent;
  font-size: 0.9rem;
  margin: auto;
`;

const Title = styled.p`
  padding: 10px;
  border-bottom: 2px solid #e7e7e7;
  text-align: right;
`;

const Content = styled.div`
  overflow: auto;
  padding-bottom: 80px;
  max-width: 600px;
  margin: 0 auto;
  overflow: hidden;
  background: #fff;
  display: grid;

  @media only screen and (min-width: 768px) {
    position: relative;
    top: 25px;
    padding-bottom: 15px;
    border-radius: 5px;
  }
`;

const InfoCon = styled.div`
  display: grid;
  grid-template-columns: 1fr 2fr;
  direction: rtl;
`;
const InfoTitle = styled.p`
  color: #474747;
  font-weight: bold;
  font-size: 0.9rem;
  padding: 10px 0;
`;
const InfoIcon = styled.div``;

const InfoWrapper = styled.div`
  color: #818181;
  text-align: center;
  svg {
    width: 30px;
    height: 30px;
  }
`;

const InputCon = styled.div`
  padding: 5px;
  padding-top: 15px;
  input,
  textarea {
    padding: 10px 5px;
    margin-bottom: 5px;
    width: 90%;
    border: 0px;
    border-bottom: 1px solid #bfbfbf;
    font-family: IRANSans;
    font-size: 0.8rem;
  }
`;

const FormTitle = styled.div`
  width: 100%;
  text-align: right;
  padding: 10px;
`;

const FormInput = styled(FormControlLabel)`
  margin: 5px;
  padding: 0 25px;
  color: #474747;
  font-size: 0.9rem;

  span.MuiRadio-root {
    color: #179bbf;
    margin-right: 25px;
  }

  .MuiFormControlLabel-label {
    padding-right: 45px;
  }
`;

const Form = styled(FormControl)`
  width: 100%;
  padding-bottom: 15px;
  border-top: 1px solid #e7e7e7 !important;
  margin-top: 15px;
`;

const AddrCon = styled.div`
  display: flex;
  justify-content: space-between;
  width: 90%;

  .MuiInput-input {
    color: #818181;
    font-size: 0.8rem;
  }

  .MuiInput-underline {
    ::before {
      border-color: #bfbfbf;
      bottom: 5px;
    }

    ::after {
      border-color: ${(props) => props.theme.palette.primary};
      bottom: 5px;
    }
  }
`;

const Button = styled.div`
  display: inline-block;
  align-self: center;
  justify-self: center;
  background: #3bb44a;
  color: #fff;
  padding: 7.5px 15px;
  border-radius: 5px;
  margin-top: 15px;
  cursor: pointer;

  @media only screen and (max-width: 768px) {
    display: none;
  }
`;

const HideOnDesktop = styled.div`
  @media only screen and (min-width: 768px) {
    display: none;
  }
`;

const MapCon = styled(AddrCon)`
  margin-top: 8px;
  display: none;
  #mapid {
    width: ${({ close }) => (close ? "100%" : "100vw")};
    height: ${({ close }) => (close ? "150px" : "calc(100vh - 65px)")};
    position: ${({ close }) => (close ? "relative" : "fixed")};
    top: 0;
    right: 0;
    z-index: 99999999999999;
  }
  .leaflet-marker-pane > img {
    pointer-events: none;
  }
`;
