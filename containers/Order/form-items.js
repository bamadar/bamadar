import styled from "styled-components";
import Button from "@material-ui/core/Button";
import Textbox from "../../components/Textbox";

const PersonalOnfo = ({ name, family, values }) => (
  <NameInfo>
    <Textbox
      label="نام"
      defaultValue={name}
      changed={(val) => (values.name = val)}
    />
    <Textbox
      label="نام خانوادگی"
      defaultValue={family}
      changed={(val) => (values.family = val)}
    />
  </NameInfo>
);

const AddressInfo = ({ addr, pelak, city, values }) => (
  <Address>
    <div>
      <select name="city">
        <option value="dezful">دزفول</option>
      </select>
      <Textbox
        label="پلاک"
        defaultValue={pelak}
        changed={(val) => (values.pelak = val)}
      />
    </div>
    <AddrInput>
      <Textbox
        label="آدرس"
        defaultValue={addr ? addr.replace(`&${city}`, "") : ""}
        changed={(val) => (values.address = val)}
      />
    </AddrInput>
  </Address>
);

const PaymentType = ({}) => (
  <Payment>
    <div className="info">نحوه پرداخت</div>
    <select name="payment">
      <option value="cash">پرداخت در محل</option>
    </select>
  </Payment>
);

const Tel = ({ tel, values }) => (
  <Textbox
    label="تلفن ثابت"
    defaultValue={tel}
    changed={(val) => (values.tel = val)}
  />
);

const Mob = ({ mobile, values }) => (
  <Textbox
    disabled={true}
    defaultValue={mobile}
    changed={(val) => (values.mobile = val)}
  />
);
const Btn = ({ onClickHandler, values }) => {
  <Button
    variant="contained"
    color="primary"
    onClick={() => onClickHandler(values)}
  >
    ثبت اطلاعات
  </Button>;
};

export { PersonalOnfo, AddressInfo, Tel, Mob, Btn, PaymentType };

const AddrInput = styled.div`
  input {
    height: 85px;
  }
`;

const NameInfo = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  direction: rtl;
  grid-column-gap: 8px;
`;

const Address = styled.div`
  display: grid;
  grid-template-columns: 1fr 3fr;
  direction: rtl;
  grid-column-gap: 8px;
`;

const Payment = styled.div`
  display: flex;
  font-size: 0.8rem;

  select {
    margin: 0 !important;
  }

  .info {
    width: 100px;
    display: flex;
    align-items: center;
  }
`;
