import styled from "styled-components";
import Router from "next/router";
import Button from "@material-ui/core/Button";
import { PersonalOnfo, AddressInfo, Tel, Mob, PaymentType } from "./form-items";
import {
  just_persian,
  justDigit,
  chackPelak,
  persianToStandard,
} from "../../utils/input-validator";
import { CircularProgress } from "@material-ui/core";
import { pushNotification } from "../../components/Notification";

const OrderForm = ({ info, setUserInfo, setInformation, loading }) => {
  const { name, family, tel, address, pelak, city, mobile, payment } = info;

  const values = {
    address: address,
    city: city,
    family: family,
    mobile: mobile,
    name: name,
    payment: payment,
    pelak: pelak,
    tel: tel,
  };
  const { query } = Router;
  const selectPayment = !query || (query && !query.userInfo);

  const onClickHandler = (values) => {
    setInformation(values);
    const nameStatus = values.name && just_persian(values.name.trim());
    const familyStatus = values.family && just_persian(values.family.trim());
    const addressStatus =
      values.address &&
      (!values.address.trim() ||
        !(values.address.replace(`&${values.city}`, "") == ""));

    const telStatus =
      values.tel && justDigit(persianToStandard(values.tel.trim()));
    const pelakStatus =
      values.pelak && chackPelak(persianToStandard(values.pelak.trim()));

    !nameStatus &&
      pushNotification("نام خود را به فارسی وارد کنید", " ", "danger");

    !familyStatus &&
      pushNotification("نام خانوادگی خود را به فارسی وارد کنید", " ", "danger");

    !addressStatus && pushNotification("آدرس را وارد کنید", " ", "danger");

    !telStatus &&
      pushNotification("شماره تلفن را به عدد وارد کنید", " ", "danger");

    !pelakStatus &&
      pushNotification("پلاک خود را به عدد وارد کنید", " ", "danger");

    if (
      !nameStatus ||
      !familyStatus ||
      !addressStatus ||
      !telStatus ||
      !pelakStatus
    )
      return;

    setUserInfo(values);
  };

  return (
    <Wrapper>
      <PersonalOnfo name={name} family={family} values={values} />
      <Tel tel={tel} values={values} />
      <AddressInfo
        addr={address && address.replace("null", "")}
        pelak={pelak}
        city={city}
        values={values}
      />
      <Mob mobile={mobile} values={values} />
      {selectPayment && <PaymentType />}
      <Button onClick={() => onClickHandler(values)}>
        {loading ? (
          <CircularProgress style={{ width: 25, height: 25 }} />
        ) : (
          "ثبت اطلاعات"
        )}
      </Button>
    </Wrapper>
  );
};

export { OrderForm };

const Wrapper = styled.form`
  display: grid;
  grid-row-gap: 12px;
  margin-top: 20px;
  direction: rtl;

  select {
    width: 100%;
    margin-bottom: 10px;
    padding: 5px 10px;
  }
  button {
    background: ${(props) => props.theme.colors.navyBlue};
    color: ${(props) => props.theme.colors.white};

    svg {
      color: white;
    }

    &:hover {
      background: ${(props) => props.theme.colors.navyBlue};
    }
  }

  * {
    font-family: IRANSans;
    outline: none;
  }
`;
