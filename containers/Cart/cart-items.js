import React, { useContext } from "react";
import { CardItem } from "../../components/CardItem";
import styled from "styled-components";
import { CardContext } from "../../context/cardContext";
import SimpleModal from "./../../components/Modal/index";
import Router from "next/router";
import DeleteSweepOutlinedIcon from "@material-ui/icons/DeleteSweepOutlined";

export default ({ items, increaseCardBalance, decreaseCardBalance }) => {
  const { clearCart } = useContext(CardContext);

  return (
    <>
      <Wrapper>
        {items && items.length > 0 && (
          <>
            {items.map((item) => (
              <div style={{ cursor: "pointer" }}>
                <CardItem
                  key={item.id}
                  id={item.id}
                  name={item.name}
                  image={item.images && item.images.thumb}
                  balance={[{ balance: item.balance }]}
                  finalPrice={item.sell_price * item.balance}
                  measurement={item.measurement}
                  add={() => increaseCardBalance(item)}
                  remove={() => decreaseCardBalance(item)}
                  number={item.number}
                  status={item.status}
                />
              </div>
            ))}
          </>
        )}
      </Wrapper>
      {items && items.length > 0 && (
        <UserDescription>
          <Bold style={{ textAlign: "right", padding: "10px 18px" }}>
            توضیحات سفارش
          </Bold>
          <DescriptionInput
            rows="3"
            placeholder="توضیحات سفارش خود را وارد کنید"
            onInput={(e) => {
              const textarea = e.target;
              textarea.style.height = "";
              textarea.style.height =
                Math.min(textarea.scrollHeight, 320) + "px";
              window.scrollTo(0, document.body.scrollHeight);
            }}
            defaultValue={window.localStorage.getItem("orderDescription")}
            onChange={(e) =>
              window.localStorage.setItem("orderDescription", e.target.value)
            }
          />
        </UserDescription>
      )}
      {items && items.length > 0 && (
        <SimpleModal
          icon={
            <Clear>
              حذف همه
              <DeleteSweepOutlinedIcon />
            </Clear>
          }
          handleYes={clearCart}
          title={
            <p>
              آیا از حذف <Bold> همه سفارشها</Bold>اطمینان دارید؟
            </p>
          }
        />
      )}
    </>
  );
};

const Wrapper = styled.div`
  @media only screen and (min-width: 768px) {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: 8px;
    padding: 8px;
    direction: rtl;
  }
`;

const Clear = styled.div`
  display: flex;
  align-items: center;
  color: ${(p) => p.theme.palette.secondary};
  justify-content: flex-end;
  padding: 18px;
  cursor: pointer;

  svg,
  path {
    color: ${(p) => p.theme.palette.secondary};
    fill: ${(p) => p.theme.palette.secondary};
    margin-left: 10px;
  }
`;

const Bold = styled.span`
  font-weight: bold;
  margin-left: 5px;
`;

const UserDescription = styled.div`
  text-align: right;
  span {
    display: block;
    padding-right: 6px;
    padding-top: 6px;
    font-size: 0.9rem;
  }
`;
const DescriptionInput = styled.textarea`
  width: 100%;
  text-align: right;
  padding: 5px 18px;
  padding-top: 0;
  font-size: 0.8rem;
  border: none;
  border-bottom: 5px solid #efeef3;
`;
