import React from "react";
import Router from "next/router";
import Modal from "../../components/Modal";
import styled from "styled-components";

export default ({ setShowModal, showModal }) => {
  const Alert = () => (
    <>
      <Text>سفارش شما در ساعت کاری برای شما ارسال خواهد شد</Text>
      <Desc>ساعات کاری از ساعت 10صبح تا 23:30</Desc>
    </>
  );
  return (
    <>
      {showModal && (
        <Modal
          handleYes={() => Router.push("/order")}
          handleNo={() => setShowModal(false)}
          title={<Alert />}
          initState={true}
          icon=" "
          yesTxt="تایید"
          noBtn={false}
        />
      )}
    </>
  );
};

const Text = styled.p``;
const Desc = styled.p`
  font-size: 0.9rem;
`;
