import React from "react";
import styled from "styled-components";

export default ({ items }) => {
  return (
    <Wrapper>
      <EmptyCart>
        <img src="/EmptyCart.svg" alt="سبد خرید شما خالی است" />
        <Title>سبد خرید شما خالی است</Title>
      </EmptyCart>
    </Wrapper>
  );
};

const Title = styled.p`
  height: 50px;
  margin: 10px auto;
  text-align: center;
`;

const Wrapper = styled.div`
  position: relative;
`;

const EmptyCart = styled.div`
  text-align: center;
  width: 100%;

  img {
    position: relative;
    right: -15px;
    width: 20rem;
  }
`;
