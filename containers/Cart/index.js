import React from "react";
import styled from "styled-components";
import Submit from "./submit";
import EmptyCart from "./empty-cart";
import CartItems from "./cart-items";
import LocationOnOutlinedIcon from "@material-ui/icons/LocationOnOutlined";
import { getUserInfo } from "../../context/localStorage";
import { makeAddr } from "../../utils/finalize-order";
import { DescAlert } from "../../components/Alert";

const Cart = ({
  cartDetails,
  setShowModal,
  items,
  increaseCardBalance,
  decreaseCardBalance,
}) => {
  const { city, pelak, address } = getUserInfo();
  const fullAddr = makeAddr(city, address, pelak);
  let allPrice = 0;
  items?.map((item) => (allPrice += item.balance * item.sell_price));
  const submitDisable = allPrice > 250000 ? false : true;

  return (
    <Container>
      <Title>سبد خرید</Title>
      {address && (
        <AddressWrapper>
          <Address>
            <SendTo>ارسال به</SendTo>
            <UserAddr>{fullAddr}</UserAddr>
          </Address>
          <AddrIconWrapper>
            <LocationOnOutlinedIcon />
          </AddrIconWrapper>
        </AddressWrapper>
      )}
      <DescAlert
        dir="rtl"
        type="info"
        variant="standard"
        title="زمان دریافت سفارش"
        subTitle="مدت زمان  ثبت سفارش تا دریافت سفارش 2ساعت خواهد بود"
      />
      <CartItems
        items={items}
        increaseCardBalance={increaseCardBalance}
        decreaseCardBalance={decreaseCardBalance}
      />
      {items && !items.length && <EmptyCart />}
      <Submit
        cartDetails={cartDetails}
        setShowModal={setShowModal}
        disable={submitDisable}
      />
    </Container>
  );
};

export default React.memo(Cart);

const Container = styled.div`
  padding-bottom: 60px;

  svg {
    color: ${(p) => p.theme.palette.primary};
  }
`;

const Title = styled.p`
  padding: 10px;
  padding-right: 18px;
  text-align: right;
  border-bottom: 4px solid #efeef3;

  @media only screen and (min-width: 768px) {
    display: none;
  }
`;

const AddressWrapper = styled.div`
  display: flex;
  text-align: right;
  padding: 6px 16px;
  border-bottom: 6px solid #efeef3;
  justify-content: flex-end;
  max-height: 350px;

  @media only screen and (min-width: 768px) {
    display: none;
  }
`;
const AddrIconWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 0 10px;

  svg {
    font-size: 1.2rem;
    width: 22px;
    height: 22px;
  }
`;
const Address = styled.div``;
const SendTo = styled.p`
  font-size: 0.8rem;
  color: #969696;
`;
const UserAddr = styled.p`
  font-size: 0.9rem;
`;
