import React from "react";
import Submit from "../../components/FixedSubmit";
import { Price } from "../../components/shared/Price";
import Router from "next/router";
import { pushNotification } from "../../components/Notification";

export default ({ cartDetails, disable }) => {
  return (
    <>
      {parseInt(cartDetails) > 0 && (
        <Submit
          mainSubject={<Price>{cartDetails}</Price>}
          subTitle="مجموع"
          buttonTxt="ادامه فرایند خرید"
          onClick={() =>
            disable
              ? pushNotification("حداقل سفارش", "حداقل سفارش 25هزار تومان است")
              : Router.push("/order")
          }
          disable={disable}
        />
      )}
    </>
  );
};
