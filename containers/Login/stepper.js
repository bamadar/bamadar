import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepConnector from "@material-ui/core/StepConnector";
import styled from "styled-components";
import TelegramIcon from "@material-ui/icons/Telegram";
import VerifiedUserOutlinedIcon from "@material-ui/icons/VerifiedUserOutlined";

const QontoConnector = withStyles({
  alternativeLabel: {
    top: 10,
    left: "calc(-50% + 16px)",
    right: "calc(50% + 16px)",
  },
  active: {
    "& $line": {
      borderColor: "#F55C6B",
    },
  },
  completed: {
    "& $line": {
      borderColor: "#F55C6B",
    },
  },
  line: {
    borderColor: "#eaeaf0",
    borderTopWidth: 3,
    borderRadius: 1,
  },
})(StepConnector);

function getSteps() {
  return [
    { title: "ورود اطلاعات", icon: <TelegramIcon /> },
    { title: "اعتبارسنجی", icon: <VerifiedUserOutlinedIcon /> },
  ];
}

export function Steppers({ activeStep }) {
  const steps = getSteps();

  return (
    <Wrapper>
      <Stepper
        style={{
          background: "transparent",
          width: "100%",
          paddingTop: 30,
          paddingBottom: 0,
        }}
        alternativeLabel
        activeStep={activeStep}
        connector={<QontoConnector />}
      >
        {steps.map((label) => (
          <Step key={label.title}>
            <StepLabel icon={label.icon}>{label.title}</StepLabel>
          </Step>
        ))}
      </Stepper>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  width: 100%;
  .Mui-disabled {
    svg,
    path {
      color: #94a5b0;
      fill: #94a5b0;
    }
  }
`;
