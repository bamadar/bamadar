import React, { useState, useContext, useCallback } from "react";
import styled from "styled-components";
import { persianToStandard, chackPhoneNo } from "../../utils/input-validator";
import { ErrorAlert } from "../../components/Alert";
import {
  Titles,
  MobileInput,
  VerifyInput,
  Submit,
  HeadImage,
  ChangeNumber,
} from "./login-items";
import { loginUser, verifyUser } from "../../context/server";
import { Paper } from "../../components/Paper";
import { Steppers } from "./stepper";
import { pushNotification } from "../../components/Notification";
export const Login = ({ setLogged }) => {
  const [mobile, setMobile] = useState({ mobile: undefined });
  const [otp, setOtp] = useState({ otp: undefined });
  const [step, setStep] = useState("mobile");
  const [loader, setLoader] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const login = useCallback(async () => {
    setShowAlert(false);
    if (
      !mobile.mobile ||
      (mobile.mobile && mobile.mobile.length < 11) ||
      !chackPhoneNo(mobile.mobile)
    ) {
      setShowAlert(true);
      return;
    }

    setLoader(true);

    loginUser(mobile.mobile).then((response) => {
      setLoader(false);
      if (response.data.message == "EnterOTP") {
        mobile.mobile = persianToStandard(mobile.mobile);
        setStep("verify");
      } else setShowAlert(true);
    });
  }, [mobile.mobile, loginUser]);

  const checkOtp = useCallback(async () => {
    setShowAlert(false);
    if (!otp.otp || (otp.otp && otp.otp.length < 4)) {
      setShowAlert(true);
      return;
    }

    setLoader(true);
    verifyUser(mobile.mobile, otp.otp).then((res) => {
      if (res && res.data && res.data.status === "ok") {
        const token = res.data.data.access_token;
        window.localStorage.setItem("MadarLogged", 1);
        window.localStorage.setItem("access_token", token);
        window.localStorage.setItem("MadarCell", mobile.mobile);
        setLogged("1");
      } else {
        setShowAlert(true);
        setLoader(false);
      }
    });
  }, [mobile.mobile, otp.otp, verifyUser, setLogged]);

  return (
    <Wrapper>
      <Steppers activeStep={step == "mobile" ? 0 : 1} />
      <Container>
        <HeadImage step={step} />
        <MobileInput step={step} setMobile={mobile} />
        <VerifyInput step={step} setOtp={otp} />
        <Titles step={step} />
        <Center>
          <Submit
            step={step}
            login={login}
            checkOtp={checkOtp}
            loader={loader}
          />
        </Center>
        <ChangeNumber
          step={step}
          setStep={setStep}
          setShowAlert={setShowAlert}
        />
        {showAlert && pushNotification("ورودی نامعتبر است", " ", "danger")}
      </Container>
    </Wrapper>
  );
};

const Container = styled(Paper)`
  background: #fff;
  height: 410px;
`;

const Center = styled.div`
  display: flex;
  justify-content: center;
`;

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  background: #f7f8fb;
  height: calc(100vh - 60px);

  @media only screen and (min-width: 768px) {
    background: #fff;
    height: 100%;
  }

  span,
  p {
    font-family: "IRANSans";
  }
`;
