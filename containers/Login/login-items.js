import React from "react";
import styled from "styled-components";
import { persianToStandard } from "../../utils/input-validator";
import CircularProgress from "@material-ui/core/CircularProgress";

export const HeadImage = ({ step }) => (
  <TextTitle>
    {step == "mobile" ? (
      <img
        src="/images/vectors/rocket.svg"
        style={{
          transform: "rotate(-45deg)",
        }}
      />
    ) : (
      <img src="/images/vectors/shield.svg" />
    )}
  </TextTitle>
);

export const Titles = ({ step }) => (
  <Caption>
    {step == "mobile"
      ? `لطفا شماره موبایل خود را وارد کرده و دکمه دریافت کد تایید را بزنید تا
         کد تایید از طریق پیامک برای شما ارسال شود
        `
      : "پس از دریافت پیامک حاوی کد تایید، کد را وارد کرده و سپس دکمه تاییدرا بزنید"}
  </Caption>
);

export const MobileInput = ({ step, setMobile }) =>
  step == "mobile" && (
    <InputWrapper>
      <InputLabel>:شماره موبایل</InputLabel>
      <input
        type="text"
        maxLength="11"
        onChange={(event) => {
          const val = event.currentTarget.value;
          setMobile.mobile = persianToStandard(val ? val : " ");
        }}
      />
    </InputWrapper>
  );

export const VerifyInput = ({ step, setOtp }) =>
  step == "verify" && (
    <InputWrapper>
      <InputLabel>:کد دریافت شده</InputLabel>
      <input
        maxLength="4"
        type="text"
        onChange={(event) => {
          const val = event.currentTarget.value;
          setOtp.otp = persianToStandard(val ? val : " ");
        }}
      />
    </InputWrapper>
  );

export const Submit = ({ step, login, checkOtp, loader }) => (
  <Button onClick={() => (step == "mobile" ? login() : checkOtp())}>
    {!loader && (step == "mobile" ? "دریافت کد تایید" : "تایید")}
    {loader && <CircularProgress style={{ width: 25, height: 25 }} />}
  </Button>
);

export const ChangeNumber = ({ setStep, step, setShowAlert }) => {
  return step == "verify" ? (
    <ChangeNo
      onClick={() => {
        setStep("mobile");
        setShowAlert(false);
      }}
    >
      تغییر شماره
    </ChangeNo>
  ) : (
    <></>
  );
};

const TextTitle = styled.span`
  display: block;
  color: ${(props) => props.theme.colors.black};
  font-weight: bold;
  font-size: 1.5rem;
  margin: 30px auto;
  text-align: center;

  img {
    width: 100px;
  }
`;

const Button = styled.div`
  display: inline-flex;
  width: 150px;
  padding: 10px 5px;
  margin: 10px 0;
  border-radius: 8px;
  background: #179bbf;
  display: flex;
  justify-content: center;
  font-size: 1rem;
  color: ${(props) => props.theme.colors.white};
  cursor: pointer;

  svg,
  path {
    color: white;
    fill: white;
  }
`;

const InputLabel = styled.label`
  position: absolute;
  right: 0;
  top: 50%;
  right: 10px;
  transform: translateY(calc(-50% + 3px));
  color: #56768b;
  pointer-events: none;
`;

const InputWrapper = styled.div`
  position: relative;
  width: 100%;

  input {
    height: 40px;
    direction: ltr;
    width: 100%;
    border-width: 0;
    margin-top: 10px;
    font-weight: 500;
    letter-spacing: 2px;
    font-family: IRANSans !important;
    box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.08);
    border-radius: 8px;
    padding: 5px 8px;
    padding-left: 30px;
    font-size: 1rem;
    color: #101922;
  }
`;

const Caption = styled.p`
  font-size: 0.8rem;
  text-align: right;
  margin: 15px 0;
  line-height: 1.5rem;
`;

const ChangeNo = styled.span`
  color: #f55163;
  border-bottom: 1px solid #f55163;
  font-size: 0.8rem;
`;

const Center = styled.div`
  display: flex;
  justify-content: center;
`;
