import React from "react";
import { CardItem } from "./../../components/CardItem";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Router from "next/router";
import ShoppingBasketOutlinedIcon from "@material-ui/icons/ShoppingBasketOutlined";

export default ({ cart, add, remove, isEmpty }) => {
  return (
    <ExpansionPanel
      onClick={() => (isEmpty ? Router.push("/cart") : null)}
      defaultExpanded={isEmpty ? false : true}
    >
      <ExpansionPanelSummary
        expandIcon={
          isEmpty ? <ShoppingBasketOutlinedIcon /> : <ExpandMoreIcon />
        }
      >
        <Typography>سبد خرید</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <Typography>
          {cart &&
            cart.map((item) => (
              <CardItem
                key={item.id}
                name={item.name}
                balance={[{ balance: item.balance }]}
                measurement={item.measurement}
                finalPrice={item.sell_price * item.balance}
                add={() => add(item)}
                remove={() => remove(item)}
                number={item.number}
                image={item.images ? item.images.thumb : "/images/no image.png"}
              />
            ))}
        </Typography>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};
