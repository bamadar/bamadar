import React from "react";
import Router from "next/router";
import styled from "styled-components";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import CreateIcon from "@material-ui/icons/Create";
import UserOrders from "./user-orders";
import Paper from "@material-ui/core/Paper";

export default ({ cart, add, remove, isEmpty }) => {
  return (
    <>
      <EditBtnCon onClick={() => Router.push("/order")}>
        <ExpansionPanelSummary expandIcon={<CreateIcon />}>
          <Typography>ویرایش اطلاعات</Typography>
        </ExpansionPanelSummary>
      </EditBtnCon>

      <UserOrders cart={cart} add={add} remove={remove} isEmpty={isEmpty} />
    </>
  );
};

const EditBtnCon = styled(Paper)`
  margin-bottom: 10px;
`;
