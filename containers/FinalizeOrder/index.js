import React from "react";
import styled from "styled-components";
import { loginCheck } from "../../utils/main";
import { Price } from "../../components/shared/Price";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import FixedSubmit from "./../../components/FixedSubmit";
import SimpleModal from "./../../components/Modal";
import { makeAddr } from "./../../utils/finalize-order";
import { config } from "./../../context/server";
import UserAccs from "./user-accessibilities";

import Router from "next/router";

import InfoOutlinedIcon from "@material-ui/icons/InfoOutlined";
import AirportShuttleOutlinedIcon from "@material-ui/icons/AirportShuttleOutlined";
import AssignmentTurnedInOutlinedIcon from "@material-ui/icons/AssignmentTurnedInOutlined";
import NavigateBeforeOutlinedIcon from "@material-ui/icons/NavigateBeforeOutlined";
import NavigateNextOutlinedIcon from "@material-ui/icons/NavigateNextOutlined";
import LocalMallOutlinedIcon from "@material-ui/icons/LocalMallOutlined";
import AccessAlarmsOutlinedIcon from "@material-ui/icons/AccessAlarmsOutlined";
import { Grid } from "@material-ui/core";

const Tarrifs = () => {
  const FreeDel = () => {
    return (
      <>
        <p>
          <Red>برای مسیرهای 5 تا 10 تومانی:</Red>
          <span>180هزارتومان خرید به بالا رایگان است</span>
        </p>
        <p style={{ paddingBottom: 25 }}>
          <Red>برای مسیرهای 15 تومانی:</Red>
          <span>300هزارتومان خرید به بالا رایگان است</span>
        </p>
      </>
    );
  };
  const TableOfTarris = () => {
    const tarrifs = [
      {
        title: "مسیر های ۵ تومانی",
        desc: [
          "کوی پیام، کوی اندیشه، فرهنگ شهر، کوی رسالت، روستا قاضی، پیام آوران و بهاران",
        ],
      },
      {
        title: "مسیرهای۶ تومانی ",
        desc: ["خیابان 45 متری فتح المبین تا خیابان شریعتی "],
      },
      {
        title: "مسیرهای ۷ تومانی",
        desc: [
          "خیابان شریعتی تا طالقانی ",
          "منازل سازمانی گنجویان و بیمارستان گنجویان",
          "کوی مقاومت و آتش نشانی میدان مثلث تا حاج مرادی",
          "کوی حافظ و کوی بهارستان",
        ],
      },
      {
        title: "مسیرهای ۸ تومانی",
        desc: [
          "خیابان طالقانی تا خیابان آیت الله طباطبایی، بین کوی مقاومت تا جاده ساحلی",
          "خیابان طالقانی تا کوی بوستان، بین خیابان امام خمینی و جاده ساحلی",
          " کوی آزادگان و پایگاه چهارم شکاری و کوی سوم شعبان",
        ],
      },
      {
        title: "مسیرهای ۹ تومانی",
        desc: [" کوی بهمن، کوی انقلاب و کوی سعدی"],
      },
      {
        title: "مسیرهای ۱۰ تومانی",
        desc: ["بلوار غدیر تا کوی مدرس", "مهرشهر"],
      },
      { title: " مسیرهای ۱۵ تومانی ", desc: ["سیاه منصور و شمس آباد"] },
      {
        title: "حداقل سفارشات با ارسال رایگان ",
        desc: [<FreeDel />],
      },
    ];
    return tarrifs.map(({ title, desc }, index) => (
      <>
        <p>
          <TarrifTitle>{title}</TarrifTitle>
          {desc.map((item) => (
            <>
              <TarrifDesc>{item}</TarrifDesc>
            </>
          ))}
        </p>
        {index != tarrifs.length - 1 && <hr />}
      </>
    ));
  };
  return (
    <SimpleModal
      icon={<TarrifBtn>مشاهده تعرفه ها</TarrifBtn>}
      title={
        <ModalWrapper>
          <TableOfTarris />
        </ModalWrapper>
      }
      noBtn=""
      yesTxt="تایید"
    />
  );
};

const TarrifTitle = styled.p`
  font-weight: bold;
  text-align: right;
  line-height: 1;
  color: ${(props) => props.theme.palette.primary};
  margin-bottom: 10px;
`;
const TarrifDesc = styled.p`
  text-align: right;
  padding-right: 10px;
  line-height: 1.8;
`;

const ModalWrapper = styled.div`
  max-height: 70vh;
  overflow: scroll;
  padding: 10px 0;
  hr {
    margin: 10px 0;
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
  }
`;

const TarrifBtn = styled.p`
  color: ${(props) => props.theme.palette.primary};
  border-bottom: 1px dashed ${(props) => props.theme.palette.primary};
  cursor: pointer;
`;
const Red = styled.span`
  color: ${(props) => props.theme.palette.secondary};
`;

export default ({
  cart,
  cartDetails,
  userOrder,
  snackBar,
  loader,
  setLoader,
  FinalizeMyOrder,
  add,
  remove,
}) => {
  const { sum, delivery, finalSum } = cartDetails ? cartDetails : {},
    { user } = userOrder ? userOrder : {},
    { pelak, address, city, tel, name, family } = user ? user : {},
    UserAddr = makeAddr(city, address, pelak);

  const cardItems = [
    { title: "مجموع سبد:", detail: <Price>{sum}</Price> },
    { title: "هزینه حمل:", detail: <Price>{delivery}</Price>, widthLine: true },
    { title: "نام و نام خانوادگی:", detail: name + " " + family },
    { title: "شماره تماس:", detail: tel, widthLine: true },
    { title: "آدرس:", detail: UserAddr },
    { extra: <UserAccs add={add} remove={remove} cart={cart} /> },
  ];

  const submitOnClickHandler = () => {
    if (!loader && loginCheck() != "0") {
      setLoader(true);
      FinalizeMyOrder();
    }
  };

  return (
    <>
      <Wrapper isEmpty={cart.length == 0 || !user}>
        {cart.length != 0 && user ? (
          <>
            <Title>تکمیل فرایند خرید</Title>
            <Address>
              {UserAddr}
              <ChangeAddrCon onClick={() => Router.push("/order")}>
                <NavigateBeforeOutlinedIcon />
                <ChangeAddr> ویرایش آدرس و اطلاعات کاربری </ChangeAddr>
              </ChangeAddrCon>
            </Address>

            <Grid container>
              <Grid item xs={12} md={4}>
                <CartBrief>
                  <BriefHead>
                    جزئیات ارسال
                    <InfoOutlinedIcon />
                  </BriefHead>
                  <BriefDesc>
                    ارسال رایگان برای سبد خرید بیش از 180هزار تومان
                  </BriefDesc>

                  <BriefHead>
                    تحویل سریع
                    <AirportShuttleOutlinedIcon />
                  </BriefHead>
                  <BriefDesc>
                    ارسال هر مرسوله با خودرو و با رعایت نکات بهداشتی
                  </BriefDesc>

                  <BriefHead>
                    مدت زمان ارسال
                    <AccessAlarmsOutlinedIcon />
                  </BriefHead>
                  <BriefDesc>
                    زمان تقریبی ارسال سفارش شما 2ساعت خواهد بود
                  </BriefDesc>

                  <BriefHead>
                    سفارشات شما
                    <LocalMallOutlinedIcon />
                  </BriefHead>
                  <Products>
                    {cart.map((item) => (
                      <Product
                        src={
                          item.images && item.images.thumb
                            ? config + item.images.thumb
                            : "/images/no image.png"
                        }
                      />
                    ))}
                  </Products>
                  <SeeOrders onClick={() => Router.push("/cart")}>
                    مشاهده سبد خرید
                    <NavigateNextOutlinedIcon />
                  </SeeOrders>
                </CartBrief>
              </Grid>

              <Grid item xs={12} md={8}>
                <SentProducts>
                  <BriefHead>
                    زمان ارسال سبد خرید
                    <AssignmentTurnedInOutlinedIcon />
                  </BriefHead>
                  <BriefDesc>
                    سفارش شما در ساعات کاری از ساعت
                    <Bold> 10 صبح تا 23:30 شب </Bold>
                    ارسال خواهد شد
                  </BriefDesc>
                </SentProducts>

                <BillCon>
                  <Bill>
                    <BriefHead
                      style={{
                        direction: "ltr",
                      }}
                    >
                      :جزئیات فاکتور
                    </BriefHead>
                    <BriefHead>{cart.length} کالا در سبد خرید شما</BriefHead>
                  </Bill>
                  <Bill>
                    <BriefHead
                      style={{
                        direction: "ltr",
                      }}
                    >
                      :مبلغ سفارش
                    </BriefHead>
                    <BriefHead>
                      <Price>{sum}</Price>
                    </BriefHead>
                  </Bill>
                  <Bill>
                    <BriefHead
                      style={{
                        direction: "ltr",
                      }}
                    >
                      :هزینه ارسال
                    </BriefHead>
                    <BriefHead>
                      <Tarrifs />
                    </BriefHead>
                  </Bill>
                  {/* <hr />
                  <Bill>
                    <BriefHead
                      style={{
                        direction: "ltr",
                      }}
                    >
                      :جمع کل
                    </BriefHead>
                    <BriefHead>
                      <Price>{finalSum}</Price>
                    </BriefHead>
                  </Bill> */}
                </BillCon>
              </Grid>
            </Grid>

            <FixedSubmit
              mainSubject={<Price>{sum}</Price>}
              subTitle="مجموع فاکتور"
              buttonTxt="تکمیل فرایند خرید"
              onClick={submitOnClickHandler}
              loading={loader}
            />
          </>
        ) : (
          <AccsWrapper>
            <Title color="black">
              {user
                ? "سبد خرید شما خالی است"
                : "اطلاعات کاربری خود را تکمیل کنید"}
            </Title>
            <UserAccs
              add={add}
              remove={remove}
              cart={cart}
              isEmpty={cart.length ? false : true}
            />
          </AccsWrapper>
        )}
      </Wrapper>
    </>
  );
};

const AccsWrapper = styled.div`
  background: ${(props) => props.theme.colors.white};
  z-index: 10;
  padding: 20px;
  box-shadow: 5px 5px 15px ${(props) => props.theme.colors.gray};
  border-radius: 10px;
  text-align: center;
  margin: 0 20px;
  max-height: 80vh;
  overflow: scroll;
`;

const Wrapper = styled.div`
  width: 100%;
  color: ${(props) => props.theme.colors.black};
  display: flex;
  flex-direction: column;
  text-align: right;
  padding-bottom: 80px;

  p,
  span {
    font-family: IRANSans;
  }
`;

const Title = styled.p`
  color: #545454;
  text-align: right;
  border-bottom: 2px solid #e8e7ec;
  padding: 10px;
  padding-right: 18px;
`;

const Address = styled.div`
  padding: 24px 18px;
  font-size: 0.9rem;
  border-bottom: 5px solid #e8e7ec;
`;

const ChangeAddr = styled.span`
  padding: 0 5px 0 10px;
`;

const ChangeAddrCon = styled.div`
  text-align: left;
  margin-top: 16px;
  text-align: left;
  font-size: 0.8rem;
  color: ${(props) => props.theme.palette.primary};
  display: flex;
  align-items: center;
  justify-content: flex-start;

  svg {
    width: 15px;
    height: 15px;
    transform: translateX(10px);
  }
`;

const CartBrief = styled.div`
  padding: 5px 18px;
`;
const BriefHead = styled.p`
  color: #545454;
  margin-top: 16px;
  display: flex;
  align-items: center;
  justify-content: flex-end;

  svg {
    margin-left: 5px;
  }
`;
const BriefDesc = styled.p`
  color: #b7b7b7;
  font-size: 0.9rem;
`;

const Bold = styled.span`
  font-weight: bold;
  color: #a6a6a6;
`;

const Bill = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`;

const BillCon = styled.div`
  padding: 18px;
  direction: rtl;

  @media only screen and (min-width: 768px) {
    background: #fff;
    border-radius: 16px;
  }

  hr {
    margin-top: 15px;
  }
`;

const SentProducts = styled.div`
  background-color: #f2f2f2;
  padding: 16px 18px;
  padding-top: 0;
`;
const Products = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const Product = styled.img`
  width: 45px;
  height: 45px;
  margin-top: 10px;
  margin-left: 10px;
`;

const SeeOrders = styled.div`
  margin-top: 10px;
  margin-top: 16px;
  font-size: 0.9rem;
  color: ${(props) => props.theme.palette.secondary};
  display: flex;
  align-items: center;
  justify-content: flex-end;

  svg {
    width: 15px;
    height: 15px;
  }
`;
