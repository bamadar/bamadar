import React, { useEffect, useState } from "react";
import AssignmentOutlinedIcon from "@material-ui/icons/AssignmentOutlined";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import LocalShippingOutlinedIcon from "@material-ui/icons/LocalShippingOutlined";
import DoneOutlinedIcon from "@material-ui/icons/DoneOutlined";
import styled from "styled-components";
import { Progress } from "../../components/Loading";
import ImgMediaCard from "./imageCard";
import { config } from "../../context/server";
import { CStepper } from "../../components/Stepper";
import { mapOrderStatus } from "../../utils/history";
import { DescAlert } from "../../components/Alert";
import { Price } from "../../components/shared/Price";
import { Grid as MuiGrid } from "@material-ui/core";

export const Stepper = ({ activeStep }) => {
  const steps = ["ثبت سفارش", "درحال پردازش", "درحال ارسال", "تحویل"];

  const icons = {
    1: <AssignmentOutlinedIcon />,
    2: <AddShoppingCartIcon />,
    3: <LocalShippingOutlinedIcon />,
    4: <DoneOutlinedIcon />,
  };

  return (
    <StepperCon>
      <CStepper activeStep={activeStep} icons={icons} steps={steps} />
    </StepperCon>
  );
};

export default ({ getUserOrders, trackingCode }) => {
  const [orders, setOrders] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);

    getUserOrders(null, trackingCode).then((res) => {
      setLoading(false);
      const data = res && res.data && res.data.data;

      if (data && data.items && data.items.data) {
        const products = data.items.data;
        const status = data.status;

        const userOrders = [
          <Grid xs={1} md={2} xl={3}>
            {products.map((item) => (
              <ImgMediaCard
                title={item.name}
                image={item.image ? config + item.image : "images/no image.png"}
                number={item.number}
                price={item.price}
                measurement={item.measurement}
                status={item.status}
                id={item.id}
              />
            ))}
          </Grid>,
        ];

        userOrders.unshift(
          <Card>
            <Title>جمع پرداختی:</Title>
            <Price>{data.amount}</Price>
            <Title>کد رهگیری:</Title>
            <Title>{trackingCode}</Title>
            <Title>تاریخ خرید:</Title>
            <Title>{data.date}</Title>
          </Card>
        );

        let headStatus;
        if (status != "cancelled")
          headStatus = <Stepper activeStep={mapOrderStatus(status)} />;
        else
          headStatus = (
            <StatusCon>
              <DescAlert
                type="error"
                title="این سفارش لغو شده است"
                variant="standard"
              />
            </StatusCon>
          );
        userOrders.unshift(headStatus);

        setOrders(userOrders);
      } else setOrders("خطایی در ارسال درخواست شما رخ داده است");
    });
  }, [getUserOrders]);

  return (
    <>
      {loading ? (
        <Center>
          <Progress />
        </Center>
      ) : (
        orders
      )}
    </>
  );
};

const StepperCon = styled.div`
  @media only screen and (max-width: 768px) {
    padding-top: 50px;
  }
`;

const StatusCon = styled.div`
  @media only screen and (max-width: 768px) {
    padding-top: 65px;
  }
`;

const Card = styled(MuiGrid)`
  display: grid;
  grid-template-columns: 1fr 1fr;
  direction: rtl;
  padding: 15px;
  margin-top: 30px;
  line-height: 2rem;
  border: 2px solid ${(p) => p.theme.colors.lightGray};
  border-radius: 15px;
  margin: 10px;
  background: #fff;
  margin: auto;
  margin-top: 10px;

  @media only screen and (min-width: 768px) {
    width: 50%;
    margin-top: auto;
  }
`;

const Title = styled.p``;

const Grid = styled.div`
  display: grid;
  direction: rtl;

  @media only screen and (max-width: 600px) {
    grid-template-columns: repeat(${({ xs }) => xs}, 1fr);
  }

  @media only screen and (min-width: 600px) {
    grid-template-columns: repeat(${({ sm }) => sm}, 1fr);
  }

  @media only screen and (min-width: 768px) {
    grid-template-columns: repeat(${({ md }) => md}, 1fr);
  }

  @media only screen and (min-width: 992px) {
    grid-template-columns: repeat(${({ lg }) => lg}, 1fr);
  }

  @media only screen and (min-width: 1200px) {
    grid-template-columns: repeat(${({ xl }) => xl}, 1fr);
  }
`;

const Center = styled.div`
  height: calc(100vh - 200px);
  display: flex;
  align-items: center;
  justify-content: center;
`;
