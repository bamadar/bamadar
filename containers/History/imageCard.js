import React from "react";
import styled from "styled-components";
import { Price } from "../../components/shared/Price";
import Router from "next/router";

export default function ImgMediaCard({
  image,
  title,
  price,
  number,
  measurement,
  id,
}) {
  return (
    <Wrapper onClick={() => Router.push("/product/[id]", `/product/${id}`)}>
      <Container>
        <Container>
          <Img src={image} alt={title} />
        </Container>
      </Container>
      <Container>
        <Title>{title}</Title>
        <Price>{price}</Price>
        <Bought>
          {number} {measurement}
        </Bought>
      </Container>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 85px 1fr;
  direction: rtl;
  line-height: 2rem;
  grid-column-gap: 15px;
  border: 1px solid ${(p) => p.theme.colors.lightGray};
  box-shadow: 0px 0px 10px ${(props) => props.theme.colors.lightGray};
  margin: 10px 8px;
  padding: 10px 8px;
  border-radius: 15px;
  background: #fff;
  cursor: pointer;
`;

const Img = styled.img`
  width: 100%;
  max-height: 85px;
`;
const Container = styled.div``;
const Title = styled.p``;
const Bought = styled.p``;
