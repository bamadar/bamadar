import React, { useEffect, useState } from "react";
import uniqid from "uniqid";
import styled from "styled-components";
import Router, { useRouter } from "next/router";
import { config } from "../../context/server";
import { Price } from "../../components/shared/Price";
import { Chip, CircularProgress } from "@material-ui/core";
import {
  statusToPersian,
  statusToColor,
  mapOrderStatus,
} from "../../utils/history";
import ScrollableTabs from "../../components/Tab";
import { Stepper } from "./products";
import { SimpleModal } from "../../components/Modal";

const Orders = ({ price, trackingCode, items, date, status }) => {
  return (
    <OrderStyle
      key={uniqid()}
      onClick={() => Router.push(`/history?trackingCode=${trackingCode}`)}
    >
      <TitleInfo>
        <Cost>
          <Price>{price}</Price>
        </Cost>
        <TrackingCode>{trackingCode}</TrackingCode>
      </TitleInfo>
      <TitleInfo>
        <OrderStatus
          label={statusToPersian(status)}
          color={statusToColor(status)}
        />
        <Date>{date}</Date>
      </TitleInfo>
      <Images>
        {items.map((item) => (
          <Img
            src={item ? config + item : "images/no image.png"}
            key={uniqid()}
            alt=""
          />
        ))}
      </Images>
      <Accs>
        <Acc>لیست خرید</Acc>
        {status == "cancelled" ? (
          <Acc style={{ color: "red" }}>سفارش لغو شده</Acc>
        ) : (
          <SimpleModal
            icon={<Acc> وضعیت سفارش</Acc>}
            title={<Stepper activeStep={mapOrderStatus(status)} />}
            yesTxt="تایید"
            noBtn=""
            style={{ padding: 0 }}
          />
        )}
      </Accs>
    </OrderStyle>
  );
};

const Accs = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  padding: 7.5px 5px;
  margin-top: 7.5px;
  font-size: 0.9rem;
  border-top: 1px solid ${(p) => p.theme.colors.lightGray};
`;

const Acc = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  :nth-child(1n) {
    border-right: 1px solid ${(p) => p.theme.colors.lightGray};
  }

  svg {
    margin-left: 5px;
  }
`;

const mapArrayToComponent = (data, loading) => {
  if (loading) return "";
  if (data.length < 1) return <TxtCenter>سفارشی یافت نشد</TxtCenter>;

  return data.map((item) => {
    const [date] = item.date.split(" ");
    return (
      <Orders
        key={uniqid()}
        price={item.price}
        trackingCode={item.trackingCode}
        items={item.items}
        date={date}
        status={item.status}
      />
    );
  });
};

export default ({ getUserOrders, filter }) => {
  const [orders, setOrders] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);

    getUserOrders().then((res) => {
      setLoading(false);
      const data = res && res.data && res.data.data;
      if (data) setOrders(data);
      else setOrders("خطایی در ارسال درخواست شما رخ داده است");
    });
  }, [getUserOrders]);

  const tabs = [
      "همه سفارشات",
      "جدید",
      "درحال پردازش",
      "ارسال شده",
      "تحویل شده",
      "لغو شده",
    ],
    tabPanel = [
      mapArrayToComponent(orders, loading),
      mapArrayToComponent(
        orders.filter(
          (item) => item.status != "completed" && item.status != "cancelled"
        ),
        loading
      ),
      mapArrayToComponent(
        orders.filter((item) => item.status == "processing"),
        loading
      ),
      mapArrayToComponent(
        orders.filter((item) => item.status == "sent"),
        loading
      ),
      mapArrayToComponent(
        orders.filter((item) => item.status == "completed"),
        loading
      ),
      mapArrayToComponent(
        orders.filter((item) => item.status == "cancelled"),
        loading
      ),
    ];

  return (
    <Wrapper>
      {
        <ScrollableTabs
          tabs={tabs.reverse()} //for rtl
          tabPanel={tabPanel.reverse()} //for rtl
          iniTab={filter ? tabs.length - 2 : tabs.length - 1}
          indicatorColor="secondary"
        />
      }
      {loading && <Progress disableShrink />}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  .MuiAppBar-positionStatic {
    position: fixed;
    top: 55px;
  }

  .MuiTypography-body1 {
    padding-top: 110px;
  }

  @media only screen and (min-width: 768px) {
    .MuiTabs-flexContainer {
      display: flex;
      justify-content: center;
    }
    .MuiTypography-body1 {
      display: grid;
      grid-template-columns: 1fr 1fr;
      padding: 0 16px;
      padding-top: 0;
    }
  }
`;

const Progress = styled(CircularProgress)`
  position: absolute;
  top: calc(50vh - 15px);
  right: calc(50vw - 15px);
`;

const OrderStyle = styled.div`
  display: grid;
  grid-row-gap: 5px;
  border: 1px solid ${(p) => p.theme.colors.lightGray};
  box-shadow: 0px 0px 10px ${(props) => props.theme.colors.lightGray};
  margin: 0 10px;
  margin-top: 10px;
  padding: 10px 8px;
  border-radius: 15px;
`;
const Date = styled.p`
  text-align: right;
  font-size: 0.8rem;
  color: ${(p) => p.theme.colors.darkGray};
`;

const TrackingCode = styled.p`
  text-align: right;
  font-size: 0.9rem;

  font-weight: bold;
`;
const Cost = styled.p`
  display: flex;

  div {
    direction: rtl;
  }
`;

const TitleInfo = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`;

const Images = styled.div`
  display: flex;
  justify-content: flex-end;
  max-width: 100%;
  overflow: hidden;
  position: relative;
  cursor: pointer;
`;

const Img = styled.img`
  width: 40px;
  height: 40px;
  margin-left: 5px;

  background-image: url("/images/Logo.png");
`;

const OrderStatus = styled(Chip)`
  width: 105px;
  height: 25px !important;
  background: ${({ color }) => color} !important;
  color: white !important;
  span {
    font-size: 0.8rem;
  }
`;

const TxtCenter = styled.p`
  text-align: center;
  margin-top: 15px;

  @media only screen and (min-width: 768px) {
    transform: translateX(50%);
  }
`;
