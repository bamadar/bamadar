import React, { useEffect, useState, useContext } from "react";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import EventNoteOutlinedIcon from "@material-ui/icons/EventNoteOutlined";
import Router, { useRouter } from "next/router";
import styled from "styled-components";
import OrdersPage from "./orders";
import { List } from "../../components/List";
import Products from "./products";
import { DeviceContext } from "../../context/DeviceContext";

const header = [
  {
    action: () => Router.back(),
    icon: <ArrowBackIosIcon />,
    text: "تاریخچه خرید",
    secIcon: <EventNoteOutlinedIcon />,
  },
];

export const History = ({ getUserOrders }) => {
  const { query } = useRouter();
  const trackingCode = query.trackingCode;
  const filter = query.filter;
  const { isMobile } = useContext(DeviceContext);

  return (
    <Wrapper>
      <List items={isMobile && header} style={{ position: "fixed" }} />
      {!trackingCode && (
        <OrdersPage getUserOrders={getUserOrders} filter={filter} />
      )}
      {trackingCode && (
        <Products getUserOrders={getUserOrders} trackingCode={trackingCode} />
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  .MuiList-root {
    position: fixed;
    width: 100%;
    background-color: #fff;
    z-index: 15;
  }
  @media only screen and (min-width: 768px) {
    .MuiStepper-root {
      width: 50%;
      background: #f5f5f5;
      margin: auto;
    }
  }
  hr {
    height: 0;
  }
`;
