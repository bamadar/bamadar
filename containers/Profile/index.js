import React from "react";
import styled from "styled-components";
import UserInfo from "./user-info";
import QuickAccs from "./quick-accs";
import Accs from "./accs";
export default () => {
  return (
    <Wrapper>
      <Header>پروفایل کاربری</Header>
      <UserCon>
        <QuickAccs />
        <UserInfo />
      </UserCon>
      <Accs />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  max-width: 1000px;
  margin: auto;
`;

const Header = styled.div`
  text-align: right;
  padding: 12px 24px;
  color: #818181;
  font-weight: bold;
  font-size: 1rem;

  @media only screen and (min-width: 768px) {
    display: none;
  }
`;

const UserCon = styled.div`
  display: flex;

  @media only screen and (max-width: 767px) {
    display: grid;
  }
`;
