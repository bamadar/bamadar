import React from "react";
import styled from "styled-components";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Router from "next/router";

export default () => {
  return (
    <EditBtnCon onClick={() => Router.push("/order?userInfo=true")}>
      <ExpansionPanelSummary expandIcon={<ExitToAppIcon />}>
        <Typography> ورود به سامانه و ثبت اطلاعات </Typography>
      </ExpansionPanelSummary>
    </EditBtnCon>
  );
};
const EditBtnCon = styled(Paper)`
  margin: 10px 0;

  p {
    font-family: IRANSans;
  }
`;
