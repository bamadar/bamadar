import React from "react";
import styled from "styled-components";
import AndroidIcon from "@material-ui/icons/Android";
import GroupIcon from "@material-ui/icons/Group";
import Router from "next/router";
import SimpleModal from "../../components/Modal";
import CallIcon from "@material-ui/icons/Call";
import ContactSupportIcon from "@material-ui/icons/ContactSupport";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";

export default () => {
  return (
    <Buttons>
      <Intro onClick={() => Router.push("/history?filter=true")}>
        <Basket>
          <ShoppingBasketIcon />
        </Basket>
        <Title>آخرین سفارشات</Title>
      </Intro>

      <Intro
        onClick={() => {
          window.localStorage.setItem("guided", "false");
          Router.push("/");
        }}
      >
        <Basket>
          <ContactSupportIcon />
        </Basket>
        <Title>آموزش کار با بامادر</Title>
      </Intro>

      <SimpleModal
        icon={
          <Intro>
            <Support>
              <CallIcon />
            </Support>
            <Title>تماس با پشتیبانی</Title>
          </Intro>
        }
        title="جهت ارتباط با پشتیبانی با شماره 42544444 تماس بگیرید"
        noBtn=""
        yesTxt="تایید"
      />
    </Buttons>
  );
};

const Intro = styled.div`
  cursor: pointer;
`;

const Buttons = styled.div`
  display: grid;
  justify-content: space-between;
  grid-column-gap: 10px;
  margin: 15px;
  position: relative;
  box-shadow: 0px 1px 15px rgba(0, 0, 0, 0.08);
  border-radius: 10px;
  padding: 10px 0;

  @media only screen and (min-width: 768px) {
    padding: 8px 16px;
    width: 250px;
    justify-content: center;
    background: #fff;
  }

  @media only screen and (max-width: 767px) {
    grid-template-columns: repeat(3, 1fr);
  }
`;
const Title = styled.p`
  text-align: center;
  font-size: 0.8rem;
  margin-top: 5px;
`;

const Button = styled.div`
  width: 100%;
  height: 90px;
  border-radius: 25px;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  grid-gap: 10px;

  svg {
    width: 50%;
    height: 50%;
    color: #179bbf;
  }
`;

const Basket = styled(Button)``;

const Support = styled(Button)``;
