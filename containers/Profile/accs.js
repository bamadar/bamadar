import React, { useState } from "react";
import styled from "styled-components";
import Router from "next/router";
import { List } from "../../components/List";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import CreateOutlinedIcon from "@material-ui/icons/CreateOutlined";
import EventNoteOutlinedIcon from "@material-ui/icons/EventNoteOutlined";
import ContactSupportOutlinedIcon from "@material-ui/icons/ContactSupportOutlined";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import SimpleModal from "../../components/Modal";
import ChatOutlinedIcon from "@material-ui/icons/ChatOutlined";
import PhoneAndroidIcon from "@material-ui/icons/PhoneAndroid";

export default () => {
  const exitUserHandler = () => {
    const cart = JSON.parse(window.localStorage.getItem("card"));
    const guided = JSON.parse(window.localStorage.getItem("guided"));
    window.localStorage.clear();
    window.localStorage.setItem("card", JSON.stringify(cart));
    window.localStorage.setItem("guided", guided);
    location.reload();
  };

  const NO_LOCAL_STORAGE = typeof window === "undefined" || !localStorage;
  const showExit =
    !NO_LOCAL_STORAGE && window.localStorage.getItem("MadarLogged") == "1";

  const items = [
    {
      text: <p onClick={() => Router.push("/app")}>نصب اپلیکیشن</p>,
      secIcon: <PhoneAndroidIcon />,
    },
    {
      text: <p onClick={() => Router.push("/terms")}>قوانین و مقررات</p>,
      secIcon: <ChatOutlinedIcon />,
    },

    {
      text: showExit && (
        <SimpleModal
          icon="خروج"
          handleYes={exitUserHandler}
          title="قصد خروج از سیستم را دارید؟"
        />
      ),
      secIcon: showExit && <ExitToAppIcon />,
    },
  ];
  return (
    <Wrapper>
      <List items={items} style={{ cursor: "pointer" }} />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: relative;
  z-index: 1;
  span {
    font-family: IRANSans;
  }

  color: #304351;

  svg {
    color: #304351;
  }
`;
