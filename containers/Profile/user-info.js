import React, { useState, useCallback, useEffect, useContext } from "react";
import { makeAddr } from "../../utils/finalize-order";
import Login from "./login";
import { getUserInfo } from "../../context/localStorage";
import styled from "styled-components";
import Divider from "@material-ui/core/Divider";
import Router from "next/router";

export default () => {
  const [userInfo, setUserInfo] = useState([]);

  const { address, city, family, mobile, name, pelak } = userInfo
    ? userInfo
    : {};

  useEffect(() => {
    setUserInfo(getUserInfo());
  }, [getUserInfo]);

  return (
    <>
      <UserInfo>
        <Center>
          <img src="/images/vectors/man.svg" />
        </Center>

        {mobile ? (
          <>
            <InfoCon>
              <UserData>{mobile}</UserData>
              <InfoTitle>:شماره موبایل</InfoTitle>
            </InfoCon>
            <Divider />

            <InfoCon>
              <UserData>{name + " " + family}</UserData>
              <InfoTitle>:نام و نام‌خانوادگی</InfoTitle>
            </InfoCon>
            <Divider />

            <InfoCon style={{ gridTemplateColumns: "1fr" }}>
              <InfoTitle>:آدرس</InfoTitle>
              <UserData style={{ textAlign: "right" }}>
                {makeAddr(city, address, pelak)}
              </UserData>
            </InfoCon>
            <Divider />

            <EditInfo onClick={() => Router.push("/order?userInfo=true")}>
              <span>ویرایش اطلاعات</span>
            </EditInfo>
          </>
        ) : (
          <Login />
        )}
      </UserInfo>
    </>
  );
};

const UserInfo = styled.div`
  line-height: 2rem;
  box-shadow: 0px 1px 15px rgba(0, 0, 0, 0.08);
  border-radius: 10px;
  padding: 10px;
  margin: 15px;

  @media only screen and (min-width: 768px) {
    width: 100%;
    padding: 16px;
    background: #fff;
  }
`;
const InfoTitle = styled.div`
  color: #54748a;
  text-align: right;
`;
const UserData = styled.div`
  color: #043353;
  font-weight: bold;
  padding-right: 8px;
  text-align: left;
`;
const InfoCon = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  justify-content: space-between;
  padding: 15px 0;
`;

const EditInfo = styled.div`
  padding-top: 10px;
  display: flex;

  span {
    color: #f55163;
    border-bottom: 1px solid #f55163;
    font-size: 0.8rem;
  }
`;

const Center = styled.div`
  display: flex;
  justify-content: center;
`;
