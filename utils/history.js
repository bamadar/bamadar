import themeColors from "../theme/theme";

export const mapOrderStatus = (status) => {
  switch (status) {
    case "new":
      return 0;
    case "processing":
      return 1;
    case "sent":
      return 2;
    case "completed":
      return 3;
  }
};

export const statusToPersian = (status) => {
  switch (status) {
    case "cancelled":
      return "لغو شد";
    case "processing":
      return "درحال پردازش";
    case "sent":
      return "ارسال شد";
    case "completed":
      return "تحویل شد";
    case "new":
      return "ثبت سفارش";
  }
};

export const statusToColor = (status) => {
  switch (status) {
    case "cancelled":
      return themeColors.palette.secondary;
    case "processing":
      return `rgba(${themeColors.rgb.green}, 0.9)`;
    case "sent":
      return `rgba(${themeColors.rgb.green}, 0.8)`;
    case "completed":
      return `rgba(${themeColors.rgb.green}, 1)`;
    case "new":
      return `rgba(${themeColors.rgb.green}, 0.7)`;
  }
};
