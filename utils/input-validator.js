import persianJs from "persianjs";

export function just_persian(str) {
  var p = /^[\u0600-\u06FF\s]+$/;
  return p.test(str);
}
export function justDigit(str) {
  var p = /^[/^\d*\.?\d*$/]+$/;
  return p.test(str);
}
export function chackPelak(str) {
  var p = /^[/^\d*\/?\d*$/]+$/;
  return p.test(str);
}

export function chackPhoneNo(str) {
  var p = /(\+98|0)?9\d{9}/;
  return p.test(str);
}

export function persianToStandard(str) {
  return persianJs(str)
    .persianNumber()
    .toString();
}
