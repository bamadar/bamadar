export const NO_LOCAL_STORAGE = typeof window === "undefined" || !localStorage,
  LOCAL_STORAGE = !NO_LOCAL_STORAGE;

export const loginCheck = () => {
  if (NO_LOCAL_STORAGE) return;

  if (window.localStorage.getItem("MadarLogged")) {
    return window.localStorage.getItem("MadarLogged");
  }
};

export const getQueryVariable = (variable) => {
  if (typeof window != "object") return undefined;
  let query = window.location.search.slice(1);
  let vars = query.split("&");
  for (const element of vars) {
    let pair = element.split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  }
  return undefined; //not found
};

export const pathToSchema = (path) => {
  const pathSections = path.split("/");
  if (pathSections.length < 2) return path;

  pathSections[2] = "[id]";

  switch (pathSections[1]) {
    case "product":
      return pathSections.join("/");

    case "category":
      return pathSections.join("/");
    default:
      return path;
  }
};

export const checkExistance = (status, number) =>
  status === "available" && number > 0;

export const imageBasedOnSize = () => {
  //100 300 600 768
  if (NO_LOCAL_STORAGE) return;

  const w = window.innerWidth;

  if (w <= 412) return 100;
  else if (w <= 768) return 300;
  else if (w <= 1024) return 600;
  else return 768;
};

export const phoneValidation = (input) => {
  let regex = /(\+98|0)?9\d{9}/;
  return regex.test(input);
};
