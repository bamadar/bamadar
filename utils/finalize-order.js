export const toId = (city) => {
  switch (city) {
    case "dezful":
      return 190;
    case "andimeshk":
      return 187;
  }
};

export const cityToPersian = (city) => {
  switch (city) {
    case "dezful":
      return " دزفول ";
    case "andimeshk":
      return " اندیمشک ";
  }
};

export const makeAddr = (city, address, pelak) => {
  return `${cityToPersian(city)} - ${`${address}`.replace(`&${city}`, "")} -
پلاک ${pelak}`;
};

export const calculateShippingCost = (userOrder, cartDetails, sum) => {
  if (userOrder && userOrder.user && userOrder.user.city == "dezful") {
    if (cartDetails.sum >= 1800000) {
      return {
        sum,
        delivery: 0,
        finalSum: sum + 0,
      };
    } else if (cartDetails.sum < 1800000) {
      return {
        sum,
        delivery: 50000,
        finalSum: sum + 50000,
      };
    }
  } else {
    if (cartDetails.sum >= 3000000) {
      return {
        sum,
        delivery: 0,
        finalSum: sum + 0,
      };
    } else if (cartDetails.sum < 3000000) {
      return {
        sum,
        delivery: 100000,
        finalSum: sum + 100000,
      };
    }
  }
};
