import { css } from 'styled-components'

export const mobile = inner => css`
  @media (max-width: 720px) {
    ${inner};
  }
`

export const tablet = inner => css`
  @media (max-width: 1170px) {
    ${inner};
  }
`
